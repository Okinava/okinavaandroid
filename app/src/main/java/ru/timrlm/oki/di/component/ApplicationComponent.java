package ru.timrlm.oki.di.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.local.RealmHelper;
import ru.timrlm.oki.data.local.PreferencesHelper;
import ru.timrlm.oki.data.remote.ApiHelper;
import ru.timrlm.oki.data.remote.SberApi;
import ru.timrlm.oki.data.remote.SmartApi;
import ru.timrlm.oki.data.remote.SmsHelper;
import ru.timrlm.oki.di.ApplicationContext;
import ru.timrlm.oki.di.module.ApplicationModule;
import ru.timrlm.oki.util.RxEventBus;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext
    Context context();
    Application application();
    ApiHelper apiHelper();
    SmsHelper smsHelper();
    SmartApi smartHelper();
    SberApi sberHelper();
    PreferencesHelper preferencesHelper();
    RealmHelper realmHelper();
    DataManager dataManager();
    RxEventBus eventBus();

}
