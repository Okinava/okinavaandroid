package ru.timrlm.oki.di.component;

import dagger.Subcomponent;
import ru.timrlm.oki.di.PerActivity;
import ru.timrlm.oki.di.module.ActivityModule;
import ru.timrlm.oki.ui.auto.pass.PassActivity;
import ru.timrlm.oki.ui.chat.ChatActivity;
import ru.timrlm.oki.ui.chat.mark.MarkActivity;
import ru.timrlm.oki.ui.contact.ContactFragment;
import ru.timrlm.oki.ui.devilary.DevilaryActivity;
import ru.timrlm.oki.ui.auto.AutoActivity;
import ru.timrlm.oki.ui.devilary.pay.PayActivity;
import ru.timrlm.oki.ui.devilary.pay.sber.SberActivity;
import ru.timrlm.oki.ui.foodgroup.FoodGroupFragment;
import ru.timrlm.oki.ui.foodgroup.search.SearchFoodActivity;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.ui.devilary.newadress.NewAdressActivity;
import ru.timrlm.oki.ui.order.OrderActivity;
import ru.timrlm.oki.ui.product.ProductActivity;
import ru.timrlm.oki.ui.product.fragment.ProductFragment;
import ru.timrlm.oki.ui.productlist.ProductListFragment;
import ru.timrlm.oki.ui.prof.ProfFragment;
import ru.timrlm.oki.ui.prof.repeat.RepeatActivity;
import ru.timrlm.oki.ui.prof.set.SetProfActivity;
import ru.timrlm.oki.ui.sale.SaleFragment;
import ru.timrlm.oki.ui.sale.salelist.SaleListActivity;
import ru.timrlm.oki.ui.splash.SplashActivity;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);
    void inject(SetProfActivity activity);
    void inject(SplashActivity activity);
    void inject(PassActivity activity);
    void inject(PayActivity activity);
    void inject(ProductListFragment activity);
    void inject(DevilaryActivity activity);
    void inject(OrderActivity fragment);
    void inject(ContactFragment fragment);
    void inject(NewAdressActivity fragment);
    void inject(AutoActivity activity);
    void inject(ProductActivity activity);
    void inject(FoodGroupFragment fragment);
    void inject(ProfFragment fragment);
    void inject(ProductFragment fragment);
    void inject(SaleFragment saleFragment);
    void inject(SaleListActivity saleListActivity);

    void inject(SearchFoodActivity searchFoodActivity);

    void inject(SberActivity sberActivity);

    void inject(ChatActivity chatActivity);

    void inject(MarkActivity markActivity);

    void inject(RepeatActivity repeatActivity);
}
