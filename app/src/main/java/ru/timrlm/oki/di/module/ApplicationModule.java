package ru.timrlm.oki.di.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.timrlm.oki.data.remote.ApiHelper;
import ru.timrlm.oki.data.remote.SberApi;
import ru.timrlm.oki.data.remote.SmartApi;
import ru.timrlm.oki.data.remote.SmsHelper;
import ru.timrlm.oki.di.ApplicationContext;

/**
 * Provide application-level dependencies.
 */
@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    ApiHelper provideApi() {
        return ApiHelper.Creator.newApi();
    }

    @Provides
    @Singleton
    SmartApi smartApi() {
        return SmartApi.Creator.newApi();
    }

    @Provides
    @Singleton
    SmsHelper smsHelper() {
        return SmsHelper.Creator.newApi();
    }

    @Provides
    @Singleton
    SberApi sberApi() {
        return SberApi.Creator.newApi();
    }
}
