package ru.timrlm.oki.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v7.widget.AppCompatEditText;
import android.text.BoringLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.instacart.library.truetime.TrueTime;

import java.io.InputStream;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public final class ViewUtil {
    public static String R = "\u20BD";

    public static Float pxToDp(float px) {
        float densityDpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return px / (densityDpi / 160f);
    }

    public static int dpToPx(int dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
    }

    public static void showKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager)   activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static void vibrate(Context context){
        Vibrator vibrator = ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE));
        vibrator.vibrate(20);
    }

    public static DisplayMetrics getDisplayMetrics(){
        return Resources.getSystem().getDisplayMetrics();
    }

    public static Bitmap getImage(Context context, String name) {
        Bitmap image = null;
        try {
            AssetManager am = context.getAssets();
            InputStream is = am.open(name);
            image = BitmapFactory.decodeStream(is);
            is.close();
        } catch (Exception e) {
            Log.d("AssertUtil", e.getMessage());
        }
        return image;
    }

    public static void shareText(Context context, String subject,String body) {
        Intent txtIntent = new Intent(android.content.Intent.ACTION_SEND);
        txtIntent .setType("text/plain");
        txtIntent .putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        txtIntent .putExtra(android.content.Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(txtIntent ,"Share"));
    }

    public static Calendar time(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis( TrueTime.now().getTime());
        Log.v("qwe","" + calendar.getTimeInMillis());
        return calendar;
    }

    public static String time(String date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date do1 = formatter.parse(date);
        Long t = do1.getTime() - TrueTime.now().getTime();
        if (t <= 0) return "00:00";
        int h = (int) TimeUnit.MILLISECONDS.toHours(t);
        int m = (int) TimeUnit.MILLISECONDS.toMinutes(t) - ( h * 60 );
        return h + ":" + String.format("%02d",  m);
    }
}

