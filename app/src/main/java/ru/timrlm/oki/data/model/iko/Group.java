package ru.timrlm.oki.data.model.iko;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Group {

    @SerializedName("additionalInfo")
    @Expose
    private Object additionalInfo;
    @SerializedName("code")
    @Expose
    private Object code;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("seoDescription")
    @Expose
    private Object seoDescription;
    @SerializedName("seoKeywords")
    @Expose
    private Object seoKeywords;
    @SerializedName("seoText")
    @Expose
    private Object seoText;
    @SerializedName("seoTitle")
    @Expose
    private Object seoTitle;
    @SerializedName("tags")
    @Expose
    private Object tags;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("isIncludedInMenu")
    @Expose
    private Boolean isIncludedInMenu;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("parentGroup")
    @Expose
    private String parentGroup;

    public Object getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(Object additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getSeoDescription() {
        return seoDescription;
    }

    public void setSeoDescription(Object seoDescription) {
        this.seoDescription = seoDescription;
    }

    public Object getSeoKeywords() {
        return seoKeywords;
    }

    public void setSeoKeywords(Object seoKeywords) {
        this.seoKeywords = seoKeywords;
    }

    public Object getSeoText() {
        return seoText;
    }

    public void setSeoText(Object seoText) {
        this.seoText = seoText;
    }

    public Object getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(Object seoTitle) {
        this.seoTitle = seoTitle;
    }

    public Object getTags() {
        return tags;
    }

    public void setTags(Object tags) {
        this.tags = tags;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Boolean getIsIncludedInMenu() {
        return isIncludedInMenu;
    }

    public void setIsIncludedInMenu(Boolean isIncludedInMenu) {
        this.isIncludedInMenu = isIncludedInMenu;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(String parentGroup) {
        this.parentGroup = parentGroup;
    }

}