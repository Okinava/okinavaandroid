package ru.timrlm.oki.data.model.iko;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class StreetResult {

    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("streets")
    @Expose
    private List<Street> streets = null;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<Street> getStreets() {
        return streets;
    }

    public void setStreets(List<Street> streets) {
        this.streets = streets;
    }

    public class City {

        @SerializedName("additionalInfo")
        @Expose
        private Object additionalInfo;
        @SerializedName("classifierId")
        @Expose
        private String classifierId;
        @SerializedName("deleted")
        @Expose
        private Boolean deleted;
        @SerializedName("externalRevision")
        @Expose
        private Object externalRevision;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        public Object getAdditionalInfo() {
            return additionalInfo;
        }

        public void setAdditionalInfo(Object additionalInfo) {
            this.additionalInfo = additionalInfo;
        }

        public String getClassifierId() {
            return classifierId;
        }

        public void setClassifierId(String classifierId) {
            this.classifierId = classifierId;
        }

        public Boolean getDeleted() {
            return deleted;
        }

        public void setDeleted(Boolean deleted) {
            this.deleted = deleted;
        }

        public Object getExternalRevision() {
            return externalRevision;
        }

        public void setExternalRevision(Object externalRevision) {
            this.externalRevision = externalRevision;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Street {

        @SerializedName("cityId")
        @Expose
        private String cityId;
        @SerializedName("classifierId")
        @Expose
        private String classifierId;
        @SerializedName("deleted")
        @Expose
        private Boolean deleted;
        @SerializedName("externalRevision")
        @Expose
        private Object externalRevision;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getClassifierId() {
            return classifierId;
        }

        public void setClassifierId(String classifierId) {
            this.classifierId = classifierId;
        }

        public Boolean getDeleted() {
            return deleted;
        }

        public void setDeleted(Boolean deleted) {
            this.deleted = deleted;
        }

        public Object getExternalRevision() {
            return externalRevision;
        }

        public void setExternalRevision(Object externalRevision) {
            this.externalRevision = externalRevision;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

}