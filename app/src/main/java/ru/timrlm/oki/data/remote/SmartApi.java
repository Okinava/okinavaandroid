package ru.timrlm.oki.data.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.timrlm.oki.BuildConfig;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.iko.Product;
import ru.timrlm.oki.data.model.iko.Restorans;
import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.data.model.smart.SmartRestoran;

public interface SmartApi {
    String ENDPOINT = "http://smart-resto.ru/";

    @GET("okinava_actions.php?token=98HQq2no")
    Observable<List<Action>> getActions();

    @GET("okinava_address.php?token=m00KijeE2")
    Observable<List<SmartRestoran>> getRestorans();

    @FormUrlEncoded
    @POST("okinava_mp/script.php?key=tujrt8kgdgdgdgndyr8k")
    Observable<Object> addFCM(@Field("name") String name, @Field("phone") String phone,
                              @Field("udid") String udid, @Field("date") String date);

    @POST("okinava_mp/orders.php?key=v33GeIu&type=app&platform=android&build=" + BuildConfig.VERSION_NAME)
    Observable<SmartResponse> addOrder(@Body CusOrder order);

    @GET("/okinava_mp/json.php?type=getActionGoods")
    Observable<List<Product>> getSales();

    @GET("/okinava_mp/json.php?type=actionStatus")
    Observable<iSales> isSales();

    @GET("/okinava_mp/json.php?type=getPushHistory")
    Observable<SmartPushs> getPushs( @Query(value = "phone", encoded = true) String phone);

    @GET("/okinava_mp/json.php?type=getVersion&os=android")
    Observable<SmartResponse> getVersion();

    class Creator {
        public static SmartApi newApi() {
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .readTimeout(15, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(SmartApi.ENDPOINT)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            return retrofit.create(SmartApi.class);
        }
    }

    class iSales {
        @SerializedName("data")
        @Expose
        public
        Boolean data;
    }

    class SmartResponse {
        @SerializedName("status")
        @Expose
        private String status;

        @SerializedName("data")
        @Expose
        private String data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }
    }

    class SmartPushs {
        @SerializedName("status")
        @Expose
        public String status;

        @SerializedName("data")
        @Expose
        public List<SmartPush> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<SmartPush> getData() {
            return data;
        }

        public void setData(List<SmartPush> data) {
            this.data = data;
        }
    }

    class SmartPush{
        @SerializedName("datetime")
        @Expose
        public String datetime;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("orders")
        @Expose
        public String orders;
        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("type")
        @Expose
        public String type;

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrders() {
            return orders;
        }

        public void setOrders(String orders) {
            this.orders = orders;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    class SmartTextPush{
        @SerializedName("comment")
        @Expose
        public String comment;
        @SerializedName("deliveryStatus")
        @Expose
        public String deliveryStatus;
        @SerializedName("orderId")
        @Expose
        public String orderId;
        @SerializedName("orderType")
        @Expose
        public String orderType;
        @SerializedName("receiver")
        @Expose
        public String receiver;
        @SerializedName("sum")
        @Expose
        public Integer sum;
        @SerializedName("text")
        @Expose
        public String text;

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getDeliveryStatus() {
            return deliveryStatus;
        }

        public void setDeliveryStatus(String deliveryStatus) {
            this.deliveryStatus = deliveryStatus;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getReceiver() {
            return receiver;
        }

        public void setReceiver(String receiver) {
            this.receiver = receiver;
        }

        public Integer getSum() {
            return sum;
        }

        public void setSum(Integer sum) {
            this.sum = sum;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
