package ru.timrlm.oki.data.model.realm;

import android.widget.EditText;

import butterknife.BindView;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ru.timrlm.oki.R;

public class lAdres extends RealmObject {

    @PrimaryKey private int id;
    private String street;
    private String home;
    private String entrance;
    private String apartment;
    private String floor;
    private String doorphone;
    private String comment;

    public void setAll(int id, String street, String home, String entrance, String apartment, String floor, String doorphone, String comment) {
        this.id = id;
        this.street = street;
        this.home = home;
        this.entrance = entrance;
        this.apartment = apartment;
        this.floor = floor;
        this.doorphone = doorphone;
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getEntrance() {
        return entrance;
    }

    public void setEntrance(String entrance) {
        this.entrance = entrance;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getDoorphone() {
        return doorphone;
    }

    public void setDoorphone(String doorphone) {
        this.doorphone = doorphone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
