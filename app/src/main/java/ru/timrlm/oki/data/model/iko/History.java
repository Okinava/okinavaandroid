package ru.timrlm.oki.data.model.iko;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class History {

    @SerializedName("customersDeliveryHistory")
    @Expose
    private List<CustomersDeliveryHistory> customersDeliveryHistory = new ArrayList<>();

    public List<CustomersDeliveryHistory> getCustomersDeliveryHistory() {
        return customersDeliveryHistory;
    }

    public void setCustomersDeliveryHistory(List<CustomersDeliveryHistory> customersDeliveryHistory) {
        this.customersDeliveryHistory = customersDeliveryHistory;
    }

    public class CustomersDeliveryHistory {

        @SerializedName("customer")
        @Expose
        private Customer customer;
        @SerializedName("deliveryHistory")
        @Expose
        private List<DeliveryHistory> deliveryHistory = null;

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }

        public List<DeliveryHistory> getDeliveryHistory() {
            return deliveryHistory;
        }

        public void setDeliveryHistory(List<DeliveryHistory> deliveryHistory) {
            this.deliveryHistory = deliveryHistory;
        }

    }

    public class DeliveryHistory {

        @SerializedName("orderId")
        @Expose
        private String orderId;
        @SerializedName("customerId")
        @Expose
        private String customerId;
        @SerializedName("organizationId")
        @Expose
        private String organizationId;
        @SerializedName("sum")
        @Expose
        private Double sum;
        @SerializedName("discount")
        @Expose
        private Double discount;
        @SerializedName("number")
        @Expose
        private String number;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("isSelfService")
        @Expose
        private Boolean isSelfService;
        @SerializedName("address")
        @Expose
        private CusOrder.Address address;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("conception")
        @Expose
        private String conception;
        @SerializedName("comment")
        @Expose
        private String comment;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("items")
        @Expose
        private List<CusOrder.Item> items = null;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(String organizationId) {
            this.organizationId = organizationId;
        }

        public Double getSum() {
            return sum;
        }

        public void setSum(Double sum) {
            this.sum = sum;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public String getNumber() {
            if (number == null) return "0";
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Boolean getIsSelfService() {
            return isSelfService;
        }

        public void setIsSelfService(Boolean isSelfService) {
            this.isSelfService = isSelfService;
        }

        public CusOrder.Address getAddress() {
            return address;
        }

        public void setAddress(CusOrder.Address address) {
            this.address = address;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getConception() {
            return conception;
        }

        public void setConception(String conception) {
            this.conception = conception;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<CusOrder.Item> getItems() {
            return items;
        }

        public void setItems(List<CusOrder.Item> items) {
            this.items = items;
        }
    }
}