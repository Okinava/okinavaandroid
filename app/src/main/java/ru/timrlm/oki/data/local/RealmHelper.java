package ru.timrlm.oki.data.local;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;
import ru.timrlm.oki.data.model.iko.ChildModifier;
import ru.timrlm.oki.data.model.iko.Customer;
import ru.timrlm.oki.data.model.iko.Foods;
import ru.timrlm.oki.data.model.iko.Group;
import ru.timrlm.oki.data.model.iko.Product;
import ru.timrlm.oki.data.model.realm.lAdres;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.data.model.realm.lGroup;
import ru.timrlm.oki.data.model.realm.lMark;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.di.ApplicationContext;

@Singleton
public class RealmHelper {
    public final int VERSION = 46;
    private RealmConfiguration mRealmConfiguration;
    private Realm mRealm;

    @Inject
    public RealmHelper(@ApplicationContext Context context){
        Realm.init(context);
        mRealmConfiguration = new RealmConfiguration.Builder()
                .name("realm.oki")
                .schemaVersion(VERSION)
                .deleteRealmIfMigrationNeeded()
                .build();
        mRealm = Realm.getInstance(mRealmConfiguration);
    }

    public void upd(Foods foods){
        mRealm.executeTransaction((realm) -> {
            for (lGroup g: realm.where(lGroup.class).findAll()) {
                g.setDeleted(true);
            }
            for (lProduct p: realm.where(lProduct.class).findAll()) {
                p.setDeleted(true);
            }
        });
        for (Group group : foods.getGroups()){
            lGroup lg = new lGroup();
            String img = "";
            try{ img = group.getImages().get(0).getImageUrl(); } catch (Exception e) {}
            lg.setAll(group.getId(),group.getIsDeleted(),group.getName(),group.getIsIncludedInMenu(),group.getOrder(),group.getParentGroup(), img);
            mRealm.executeTransaction((realm) -> {
                realm.copyToRealmOrUpdate(lg);
            });
        }
        for(Product product : foods.getProducts()){
            lProduct lp = new lProduct();
            String img = "";
            String modifaers = "";
            try { img = product.getImages().get(0).getImageUrl(); } catch (Exception e) {}
            String modId = "";
            if ( product.getGroupModifiers().size() > 0){
                modId = product.getGroupModifiers().get(0).getModifierId();
                for (ChildModifier child : product.getGroupModifiers().get(0).getChildModifiers()) modifaers += child.getModifierId() + ";;;";
            }
            lp.setAll(product.getId(),product.getCode(),product.getDescription(),product.getIsDeleted(),product.getName(),
                    product.getCarbohydrateAmount(),product.getCarbohydrateFullAmount(),product.getDoNotPrintInCheque(),product.getEnergyAmount(),
                    product.getEnergyFullAmount(),product.getFatAmount(),product.getFatFullAmount(),product.getFiberAmount(),product.getFiberFullAmount(),
                    product.getMeasureUnit(),product.getPrice(),product.getType(),product.getUseBalanceForSell(),product.getWeight(),product.getIsIncludedInMenu(),
                    product.getOrder(),product.getParentGroup(), img,modifaers, modId);
            mRealm.executeTransaction((realm) -> {
                realm.copyToRealmOrUpdate(lp);
            });
        }
    }

    public int createAdres(String street, String home, String entrance, String apartment, String floor, String doorphone, String comment){
        Number id = mRealm.where(lAdres.class).max("id");
        lAdres lA = new lAdres();
        lA.setAll(id == null ? 0 : id.intValue()+ 1, street,home,entrance,apartment,floor,doorphone,comment);
        mRealm.executeTransaction((realm -> {
            realm.copyToRealmOrUpdate(lA);
        }));
        return (int) mRealm.where(lAdres.class).count() - 1;
    }

    public void delete(int id){
        mRealm.executeTransaction((realm -> {
            realm.where(lAdres.class).equalTo("id",id).findAll().deleteAllFromRealm();
        }));
    }

    public List<lAdres> getAdres() { return mRealm.where(lAdres.class).sort("id").findAll(); }

    public lCustomer addUser(Customer customer){
        Crashlytics.setUserIdentifier(customer.getPhone());
        Crashlytics.setUserName(customer.getName());
        lCustomer lCustomer = new lCustomer();
        lCustomer.setAll(customer);
        mRealm.executeTransaction((realm) -> { realm.copyToRealmOrUpdate(lCustomer); });
        return lCustomer;
    }

    public lCustomer updUser(String phone, String name, int sex, String date){
        lCustomer lCustomer = mRealm.where(lCustomer.class).equalTo("phone",phone).findFirst();
        mRealm.executeTransaction((realm) -> {
            lCustomer.setName(name);
            lCustomer.setSex(sex);
            if (date != null) lCustomer.setBirthday(date);
        });
        return lCustomer;
    }

    public @Nullable lCustomer getUser(String phone) {
        return mRealm.where(lCustomer.class).equalTo("phone", phone).findFirst();
    }

    public List<lGroup> getGroups(){
        return mRealm.where(lGroup.class).isNull("parentGroup")
                .equalTo("isIncludedInMenu",true)
                .equalTo("isDeleted",false)
                .sort("order").findAll();
    }

    public lGroup getGroup(String id){
        return mRealm.where(lGroup.class).equalTo("id",id).findFirst();
    }

    public List<lProduct> getProducts(String id) {
        return mRealm.where(lProduct.class).equalTo("parentGroup",id)
                .equalTo("isDeleted",false)
                .equalTo("isIncludedInMenu",true).sort("order").findAll();
    }

    public List<lProduct> getProductsByName(String name) {
        String nameT = name.substring(0,1).toUpperCase() + name.substring(1);
        return mRealm.where(lProduct.class).contains("name",name).or().contains("name",nameT)
                .equalTo("isIncludedInMenu",true)
                .equalTo("isDeleted",false)
                .sort("parentGroup", Sort.ASCENDING,"order",Sort.ASCENDING)
                .findAll();
    }

    public List<lProduct> getProducts() {
        return mRealm.where(lProduct.class)
                .equalTo("isIncludedInMenu",true)
                .equalTo("isDeleted",false)
                .sort("parentGroup", Sort.ASCENDING,"order",Sort.ASCENDING)
                .findAll();
    }

    public Boolean isHavePodGroup(String id){
        return mRealm.where(lGroup.class).equalTo("parentGroup",id).count() != 0;
    }

    public List<lGroup> getPodGroup(String id){
        return mRealm.where(lGroup.class).equalTo("parentGroup",id).sort("order").findAll();
    }

    public OrderInfo getOrderInfo(){
        OrderInfo o = new OrderInfo();
        o.count = mRealm.where(lOrder.class).sum("count").intValue();
        o.sum = mRealm.where(lOrder.class).sum("sum").intValue();
        return  o;
    }

    public void addOrder(int count, lProduct product, String modificator, String nameM, int priceM) {
        lOrder old = mRealm.where(lOrder.class).equalTo("modificator",modificator).equalTo("id",product.getId()).findFirst();
        if (old == null) {
            lOrder ne = new lOrder();
            ne.setAll(product,count,modificator, nameM, priceM);
            mRealm.executeTransaction((realm -> {
                realm.copyToRealm(ne);
            }));
        }else {
            mRealm.executeTransaction((realm -> {
                old.setCount(old.getCount()+count);
                old.setSum(old.getCount()*(product.getPrice()+priceM));
            }));
        }
    }

    public void minusOrder(int count, lProduct product, String modificator, int priceM) {
        lOrder old = mRealm.where(lOrder.class).equalTo("modificator",modificator).equalTo("id",product.getId()).findFirst();
        if (old.getCount() - count < 1) {
            mRealm.executeTransaction((realm -> {
                mRealm.where(lOrder.class).equalTo("id",product.getId()).findAll().deleteAllFromRealm();
            }));
        }else {
            mRealm.executeTransaction((realm -> {
                old.setCount(old.getCount()-count);
                old.setSum(old.getCount()*(product.getPrice() + priceM));
            }));
        }
    }

    public void deleteOrder(lProduct product, String modificator) {
        lOrder old = mRealm.where(lOrder.class).equalTo("modificator", modificator).equalTo("id", product.getId()).findFirst();
        mRealm.executeTransaction((realm -> {
            mRealm.where(lOrder.class).equalTo("id", product.getId()).findAll().deleteAllFromRealm();
        }));
    }

    public List<lOrder> getOrder(){
        return mRealm.copyFromRealm(mRealm.where(lOrder.class).findAll());
    }

    public void clearOrder(){
        mRealm.executeTransaction(realm -> {
            mRealm.where(lOrder.class).findAll().deleteAllFromRealm();
        });
    }

    public List<lProduct> getProduct(List<String> guids) { return mRealm.where(lProduct.class)
            .in("id", guids.toArray(new String[]{}))
            .sort("price").findAll(); }

    public  lProduct getProductById(String id) { return mRealm.where(lProduct.class)
            .equalTo("id", id)
            .equalTo("isIncludedInMenu",true)
            .equalTo("isDeleted",false)
            .findFirst();
    }

    public void addMark(String id, String text, String time){
        mRealm.executeTransaction((realm -> {
            realm.copyToRealmOrUpdate(new lMark().setAll(id,text,time));
        }));
    }

    public lMark getMark(String id){
        return mRealm.where(lMark.class).equalTo("id",id).findFirst();
    }

    public class OrderInfo{
        public int count = 0;
        public int sum = 0;
    }
}
