package ru.timrlm.oki.data.model.iko;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderResponse {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("customerId")
    @Expose
    private String customerId;
    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("address")
    @Expose
    private CusOrder.Address address;
    @SerializedName("restaurantId")
    @Expose
    private String restaurantId;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("sum")
    @Expose
    private Integer sum;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("deliveryCancelCause")
    @Expose
    private Object deliveryCancelCause;
    @SerializedName("deliveryCancelComment")
    @Expose
    private Object deliveryCancelComment;
    @SerializedName("courierInfo")
    @Expose
    private Object courierInfo;
    @SerializedName("orderLocationInfo")
    @Expose
    private OrderLocationInfo orderLocationInfo;
    @SerializedName("deliveryDate")
    @Expose
    private String deliveryDate;
    @SerializedName("actualTime")
    @Expose
    private Object actualTime;
    @SerializedName("billTime")
    @Expose
    private Object billTime;
    @SerializedName("cancelTime")
    @Expose
    private Object cancelTime;
    @SerializedName("closeTime")
    @Expose
    private Object closeTime;
    @SerializedName("confirmTime")
    @Expose
    private String confirmTime;
    @SerializedName("createdTime")
    @Expose
    private String createdTime;
    @SerializedName("printTime")
    @Expose
    private Object printTime;
    @SerializedName("sendTime")
    @Expose
    private Object sendTime;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("problem")
    @Expose
    private Object problem;
    @SerializedName("operator")
    @Expose
    private Object operator;
    @SerializedName("conception")
    @Expose
    private Object conception;
    @SerializedName("marketingSource")
    @Expose
    private Object marketingSource;
    @SerializedName("durationInMinutes")
    @Expose
    private Integer durationInMinutes;
    @SerializedName("personsCount")
    @Expose
    private Integer personsCount;
    @SerializedName("splitBetweenPersons")
    @Expose
    private Boolean splitBetweenPersons;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("guests")
    @Expose
    private List<Guest> guests = null;
    @SerializedName("payments")
    @Expose
    private List<Payment> payments = null;
    @SerializedName("orderType")
    @Expose
    private OrderType orderType;
    @SerializedName("deliveryTerminal")
    @Expose
    private Object deliveryTerminal;
    @SerializedName("discounts")
    @Expose
    private List<Object> discounts = null;
    @SerializedName("iikoCard5Coupon")
    @Expose
    private Object iikoCard5Coupon;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public CusOrder.Address getAddress() {
        return address;
    }

    public void setAddress(CusOrder.Address address) {
        this.address = address;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getDeliveryCancelCause() {
        return deliveryCancelCause;
    }

    public void setDeliveryCancelCause(Object deliveryCancelCause) {
        this.deliveryCancelCause = deliveryCancelCause;
    }

    public Object getDeliveryCancelComment() {
        return deliveryCancelComment;
    }

    public void setDeliveryCancelComment(Object deliveryCancelComment) {
        this.deliveryCancelComment = deliveryCancelComment;
    }

    public Object getCourierInfo() {
        return courierInfo;
    }

    public void setCourierInfo(Object courierInfo) {
        this.courierInfo = courierInfo;
    }

    public OrderLocationInfo getOrderLocationInfo() {
        return orderLocationInfo;
    }

    public void setOrderLocationInfo(OrderLocationInfo orderLocationInfo) {
        this.orderLocationInfo = orderLocationInfo;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Object getActualTime() {
        return actualTime;
    }

    public void setActualTime(Object actualTime) {
        this.actualTime = actualTime;
    }

    public Object getBillTime() {
        return billTime;
    }

    public void setBillTime(Object billTime) {
        this.billTime = billTime;
    }

    public Object getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Object cancelTime) {
        this.cancelTime = cancelTime;
    }

    public Object getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Object closeTime) {
        this.closeTime = closeTime;
    }

    public String getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(String confirmTime) {
        this.confirmTime = confirmTime;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public Object getPrintTime() {
        return printTime;
    }

    public void setPrintTime(Object printTime) {
        this.printTime = printTime;
    }

    public Object getSendTime() {
        return sendTime;
    }

    public void setSendTime(Object sendTime) {
        this.sendTime = sendTime;
    }

    public Object getComment() {
        return comment;
    }

    public void setComment(Object comment) {
        this.comment = comment;
    }

    public Object getProblem() {
        return problem;
    }

    public void setProblem(Object problem) {
        this.problem = problem;
    }

    public Object getOperator() {
        return operator;
    }

    public void setOperator(Object operator) {
        this.operator = operator;
    }

    public Object getConception() {
        return conception;
    }

    public void setConception(Object conception) {
        this.conception = conception;
    }

    public Object getMarketingSource() {
        return marketingSource;
    }

    public void setMarketingSource(Object marketingSource) {
        this.marketingSource = marketingSource;
    }

    public Integer getDurationInMinutes() {
        return durationInMinutes;
    }

    public void setDurationInMinutes(Integer durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    public Integer getPersonsCount() {
        return personsCount;
    }

    public void setPersonsCount(Integer personsCount) {
        this.personsCount = personsCount;
    }

    public Boolean getSplitBetweenPersons() {
        return splitBetweenPersons;
    }

    public void setSplitBetweenPersons(Boolean splitBetweenPersons) {
        this.splitBetweenPersons = splitBetweenPersons;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public Object getDeliveryTerminal() {
        return deliveryTerminal;
    }

    public void setDeliveryTerminal(Object deliveryTerminal) {
        this.deliveryTerminal = deliveryTerminal;
    }

    public List<Object> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<Object> discounts) {
        this.discounts = discounts;
    }

    public Object getIikoCard5Coupon() {
        return iikoCard5Coupon;
    }

    public void setIikoCard5Coupon(Object iikoCard5Coupon) {
        this.iikoCard5Coupon = iikoCard5Coupon;
    }


    public class AdditionalPhone {

        @SerializedName("phone")
        @Expose
        private String phone;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

    }

    public class Customer {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("externalId")
        @Expose
        private Object externalId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("surName")
        @Expose
        private Object surName;
        @SerializedName("nick")
        @Expose
        private Object nick;
        @SerializedName("comment")
        @Expose
        private Object comment;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("additionalPhones")
        @Expose
        private List<AdditionalPhone> additionalPhones = null;
        @SerializedName("addresses")
        @Expose
        private List<Object> addresses = null;
        @SerializedName("cultureName")
        @Expose
        private Object cultureName;
        @SerializedName("birthday")
        @Expose
        private Object birthday;
        @SerializedName("email")
        @Expose
        private Object email;
        @SerializedName("middleName")
        @Expose
        private Object middleName;
        @SerializedName("shouldReceivePromoActionsInfo")
        @Expose
        private Boolean shouldReceivePromoActionsInfo;
        @SerializedName("sex")
        @Expose
        private Integer sex;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Object getExternalId() {
            return externalId;
        }

        public void setExternalId(Object externalId) {
            this.externalId = externalId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getSurName() {
            return surName;
        }

        public void setSurName(Object surName) {
            this.surName = surName;
        }

        public Object getNick() {
            return nick;
        }

        public void setNick(Object nick) {
            this.nick = nick;
        }

        public Object getComment() {
            return comment;
        }

        public void setComment(Object comment) {
            this.comment = comment;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public List<AdditionalPhone> getAdditionalPhones() {
            return additionalPhones;
        }

        public void setAdditionalPhones(List<AdditionalPhone> additionalPhones) {
            this.additionalPhones = additionalPhones;
        }

        public List<Object> getAddresses() {
            return addresses;
        }

        public void setAddresses(List<Object> addresses) {
            this.addresses = addresses;
        }

        public Object getCultureName() {
            return cultureName;
        }

        public void setCultureName(Object cultureName) {
            this.cultureName = cultureName;
        }

        public Object getBirthday() {
            return birthday;
        }

        public void setBirthday(Object birthday) {
            this.birthday = birthday;
        }

        public Object getEmail() {
            return email;
        }

        public void setEmail(Object email) {
            this.email = email;
        }

        public Object getMiddleName() {
            return middleName;
        }

        public void setMiddleName(Object middleName) {
            this.middleName = middleName;
        }

        public Boolean getShouldReceivePromoActionsInfo() {
            return shouldReceivePromoActionsInfo;
        }

        public void setShouldReceivePromoActionsInfo(Boolean shouldReceivePromoActionsInfo) {
            this.shouldReceivePromoActionsInfo = shouldReceivePromoActionsInfo;
        }

        public Integer getSex() {
            return sex;
        }

        public void setSex(Integer sex) {
            this.sex = sex;
        }

    }

    public class Guest {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Item {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("category")
        @Expose
        private Object category;
        @SerializedName("amount")
        @Expose
        private Integer amount;
        @SerializedName("modifiers")
        @Expose
        private List<Object> modifiers = null;
        @SerializedName("sum")
        @Expose
        private Integer sum;
        @SerializedName("guestId")
        @Expose
        private String guestId;
        @SerializedName("comboInformation")
        @Expose
        private Object comboInformation;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getCategory() {
            return category;
        }

        public void setCategory(Object category) {
            this.category = category;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public List<Object> getModifiers() {
            return modifiers;
        }

        public void setModifiers(List<Object> modifiers) {
            this.modifiers = modifiers;
        }

        public Integer getSum() {
            return sum;
        }

        public void setSum(Integer sum) {
            this.sum = sum;
        }

        public String getGuestId() {
            return guestId;
        }

        public void setGuestId(String guestId) {
            this.guestId = guestId;
        }

        public Object getComboInformation() {
            return comboInformation;
        }

        public void setComboInformation(Object comboInformation) {
            this.comboInformation = comboInformation;
        }

    }

    public class OrderLocationInfo {

        @SerializedName("latitude")
        @Expose
        private Double latitude;
        @SerializedName("longitude")
        @Expose
        private Double longitude;

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

    }

    public class OrderType {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("orderServiceType")
        @Expose
        private String orderServiceType;
        @SerializedName("externalRevision")
        @Expose
        private Integer externalRevision;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrderServiceType() {
            return orderServiceType;
        }

        public void setOrderServiceType(String orderServiceType) {
            this.orderServiceType = orderServiceType;
        }

        public Integer getExternalRevision() {
            return externalRevision;
        }

        public void setExternalRevision(Integer externalRevision) {
            this.externalRevision = externalRevision;
        }

    }

    public class Payment {

        @SerializedName("sum")
        @Expose
        private Integer sum;
        @SerializedName("paymentType")
        @Expose
        private PaymentType paymentType;
        @SerializedName("additionalData")
        @Expose
        private Object additionalData;
        @SerializedName("isProcessedExternally")
        @Expose
        private Boolean isProcessedExternally;
        @SerializedName("isPreliminary")
        @Expose
        private Boolean isPreliminary;
        @SerializedName("isExternal")
        @Expose
        private Boolean isExternal;

        public Integer getSum() {
            return sum;
        }

        public void setSum(Integer sum) {
            this.sum = sum;
        }

        public PaymentType getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(PaymentType paymentType) {
            this.paymentType = paymentType;
        }

        public Object getAdditionalData() {
            return additionalData;
        }

        public void setAdditionalData(Object additionalData) {
            this.additionalData = additionalData;
        }

        public Boolean getIsProcessedExternally() {
            return isProcessedExternally;
        }

        public void setIsProcessedExternally(Boolean isProcessedExternally) {
            this.isProcessedExternally = isProcessedExternally;
        }

        public Boolean getIsPreliminary() {
            return isPreliminary;
        }

        public void setIsPreliminary(Boolean isPreliminary) {
            this.isPreliminary = isPreliminary;
        }

        public Boolean getIsExternal() {
            return isExternal;
        }

        public void setIsExternal(Boolean isExternal) {
            this.isExternal = isExternal;
        }

    }

    public class PaymentType {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("comment")
        @Expose
        private Object comment;
        @SerializedName("combinable")
        @Expose
        private Boolean combinable;
        @SerializedName("externalRevision")
        @Expose
        private Integer externalRevision;
        @SerializedName("applicableMarketingCampaigns")
        @Expose
        private Object applicableMarketingCampaigns;
        @SerializedName("deleted")
        @Expose
        private Boolean deleted;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getComment() {
            return comment;
        }

        public void setComment(Object comment) {
            this.comment = comment;
        }

        public Boolean getCombinable() {
            return combinable;
        }

        public void setCombinable(Boolean combinable) {
            this.combinable = combinable;
        }

        public Integer getExternalRevision() {
            return externalRevision;
        }

        public void setExternalRevision(Integer externalRevision) {
            this.externalRevision = externalRevision;
        }

        public Object getApplicableMarketingCampaigns() {
            return applicableMarketingCampaigns;
        }

        public void setApplicableMarketingCampaigns(Object applicableMarketingCampaigns) {
            this.applicableMarketingCampaigns = applicableMarketingCampaigns;
        }

        public Boolean getDeleted() {
            return deleted;
        }

        public void setDeleted(Boolean deleted) {
            this.deleted = deleted;
        }

    }
}