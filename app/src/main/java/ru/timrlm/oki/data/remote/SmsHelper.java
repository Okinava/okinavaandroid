package ru.timrlm.oki.data.remote;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.timrlm.oki.data.model.iko.CreateCustomer;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.iko.Customer;
import ru.timrlm.oki.data.model.iko.Foods;
import ru.timrlm.oki.data.model.iko.History;
import ru.timrlm.oki.data.model.iko.OrderResponse;
import ru.timrlm.oki.data.model.iko.Restorans;
import ru.timrlm.oki.data.model.iko.StreetResult;

public interface SmsHelper {
    String ENDPOINT = "https://sms.ru/";
    String id = "455B17A0-F94E-F153-BE04-E89CF4792C66";
    @GET("sms/send?api_id="+id+"&json=1")
    Observable<Object> send(@Query("to") String phone, @Query("msg") String msg);

    /******** Helper class that sets up a new services *******/
    class Creator {
        public static SmsHelper newApi() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(SmsHelper.ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            return retrofit.create(SmsHelper.class);
        }
    }
}
