package ru.timrlm.oki.data.model.iko;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Locale;

public class Customer {


    @SerializedName("additionalPhones")
    @Expose
    private List<Object> additionalPhones = null;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("cards")
    @Expose
    private List<Cards> cards = null;
    @SerializedName("categories")
    @Expose
    private List<Categories> categories = null;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("cultureName")
    @Expose
    private String cultureName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("iikoCardOrdersSum")
    @Expose
    private Integer iikoCardOrdersSum;
    @SerializedName("isBlocked")
    @Expose
    private Boolean isBlocked;
    @SerializedName("shouldReceivePromoActionsInfo")
    @Expose
    private Boolean shouldReceivePromoActionsInfo;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("rank")
    @Expose
    private String rank;
    @SerializedName("sex")
    @Expose
    private Integer sex;
    @SerializedName("surname")
    @Expose
    private String surname;
    @SerializedName("walletBalances")
    @Expose
    private List<WalletBalance> walletBalances = null;

    public List<Object> getAdditionalPhones() {
        return additionalPhones;
    }

    public void setAdditionalPhones(List<Object> additionalPhones) {
        this.additionalPhones = additionalPhones;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public List<Cards> getCards() {
        return cards;
    }

    public void setCards(List<Cards> cards) {
        this.cards = cards;
    }

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCultureName() {
        return cultureName;
    }

    public void setCultureName(String cultureName) {
        this.cultureName = cultureName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIikoCardOrdersSum() {
        return iikoCardOrdersSum;
    }

    public void setIikoCardOrdersSum(Integer iikoCardOrdersSum) {
        this.iikoCardOrdersSum = iikoCardOrdersSum;
    }

    public Boolean getBlocked() {
        return isBlocked;
    }

    public void setBlocked(Boolean blocked) {
        isBlocked = blocked;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<WalletBalance> getWalletBalances() {
        return walletBalances;
    }

    public void setWalletBalances(List<WalletBalance> walletBalances) {
        this.walletBalances = walletBalances;
    }

    public Customer(String phone) { this.phone = phone; }

    public Customer(String phone, String name) { this.phone = phone; this.name = name;}

    public Customer(String phone, String name, int sex, String birth) {
        this.phone = phone;
        this.name = name;
        this.sex = sex;
        if (birth != null) this.birthday = birth;
    }

    public Customer(){}

    public class WalletBalance {

        @SerializedName("balance")
        @Expose
        private Double balance;
        @SerializedName("wallet")
        @Expose
        private Wallet wallet;

        public Double getBalance() {
            return balance;
        }

        public void setBalance(Double balance) {
            this.balance = balance;
        }

        public Wallet getWallet() {
            return wallet;
        }

        public void setWallet(Wallet wallet) {
            this.wallet = wallet;
        }

    }


    public class Wallet {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("programType")
        @Expose
        private String programType;
        @SerializedName("type")
        @Expose
        private String type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProgramType() {
            return programType;
        }

        public void setProgramType(String programType) {
            this.programType = programType;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }

    public class Cards {

        @SerializedName("Id")
        @Expose
        private String id;
        @SerializedName("IsActivated")
        @Expose
        private Boolean isActivated;
        @SerializedName("NetworkId")
        @Expose
        private String networkId;
        @SerializedName("Number")
        @Expose
        private String number;
        @SerializedName("OrganizationId")
        @Expose
        private String organizationId;
        @SerializedName("OrganizationName")
        @Expose
        private String organizationName;
        @SerializedName("Track")
        @Expose
        private String track;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Boolean getIsActivated() {
            return isActivated;
        }

        public void setIsActivated(Boolean isActivated) {
            this.isActivated = isActivated;
        }

        public String getNetworkId() {
            return networkId;
        }

        public void setNetworkId(String networkId) {
            this.networkId = networkId;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(String organizationId) {
            this.organizationId = organizationId;
        }

        public String getOrganizationName() {
            return organizationName;
        }

        public void setOrganizationName(String organizationName) {
            this.organizationName = organizationName;
        }

        public String getTrack() {
            return track;
        }

        public void setTrack(String track) {
            this.track = track;
        }
    }

    public class Categories {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("isActive")
        @Expose
        private Boolean isActive;
        @SerializedName("isDefaultForNewGuests")
        @Expose
        private Boolean isDefaultForNewGuests;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Boolean getIsActive() {
            return isActive;
        }

        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

        public Boolean getIsDefaultForNewGuests() {
            return isDefaultForNewGuests;
        }

        public void setIsDefaultForNewGuests(Boolean isDefaultForNewGuests) {
            this.isDefaultForNewGuests = isDefaultForNewGuests;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getActive() {
            return isActive;
        }

        public void setActive(Boolean active) {
            isActive = active;
        }

        public Boolean getDefaultForNewGuests() {
            return isDefaultForNewGuests;
        }

        public void setDefaultForNewGuests(Boolean defaultForNewGuests) {
            isDefaultForNewGuests = defaultForNewGuests;
        }
    }
}