package ru.timrlm.oki.data;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.timrlm.oki.BuildConfig;
import ru.timrlm.oki.data.local.RealmHelper;
import ru.timrlm.oki.data.local.PreferencesHelper;
import ru.timrlm.oki.data.model.iko.CheckAdress;
import ru.timrlm.oki.data.model.iko.CreateCustomer;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.iko.Customer;
import ru.timrlm.oki.data.model.iko.Foods;
import ru.timrlm.oki.data.model.iko.Group;
import ru.timrlm.oki.data.model.iko.History;
import ru.timrlm.oki.data.model.iko.OrderResponse;
import ru.timrlm.oki.data.model.iko.Product;
import ru.timrlm.oki.data.model.iko.Restorans;
import ru.timrlm.oki.data.model.iko.StreetResult;
import ru.timrlm.oki.data.model.realm.lAdres;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.data.model.realm.lGroup;
import ru.timrlm.oki.data.model.realm.lMark;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.data.model.smart.SmartRestoran;
import ru.timrlm.oki.data.remote.ApiHelper;
import ru.timrlm.oki.data.remote.SberApi;
import ru.timrlm.oki.data.remote.SmartApi;
import ru.timrlm.oki.data.remote.SmsHelper;
import ru.timrlm.oki.util.RxEventBus;
import ru.timrlm.oki.util.ViewUtil;

@Singleton
public class DataManager {
    public static String SALES_ID = "1e1e6a7c-5b91-2fc2-0146-1d7a94731d2e";
    public static String pp = "";

    private final ApiHelper mApiHelper;
    private final RealmHelper mRealmHelper;
    private final SmartApi mSmartApi;
    private final PreferencesHelper mPreferencesHelper;
    private final RxEventBus mRxEventBus;
    private final String login = "SRmobile";
    private final String pass = "PytEdmefkiwoyf9";
    private final SmsHelper mSmsHelper;
    private final SberApi mSberApi;

    @Inject
    public DataManager(ApiHelper apiHelper, PreferencesHelper preferencesHelper,
                       RealmHelper realmHelper, RxEventBus rxEventBus, SmsHelper smsHelper, SmartApi smartApi, SberApi sberApi) {
        mApiHelper = apiHelper;
        mPreferencesHelper = preferencesHelper;
        mRealmHelper = realmHelper;
        mRxEventBus = rxEventBus;
        mSmsHelper = smsHelper;
        mSmartApi = smartApi;
        mSberApi = sberApi;
        load();
    }

    public boolean isFirstLaunch(){ return mPreferencesHelper.isFirstLaunch();
    }

    public static class NOCONNECT{}
    public void load(){
        Observable<Foods> fir = mApiHelper.getToken(login,pass)
                .subscribeOn(Schedulers.io())
                .flatMap(s -> mApiHelper.getFoods(s))
                .observeOn(AndroidSchedulers.mainThread());
        Observable<List<Product>> sec = mSmartApi.getSales()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observable.mergeArray(fir,sec).toList()
                .subscribe((o1)->{
                    Foods foods = null;
                    List<Product> products = null;
                    if ((o1.get(0) instanceof Foods) && (o1.get(1) instanceof List)){
                        foods = (Foods) o1.get(0);
                        products = (List<Product>) o1.get(1);
                    }else if ((o1.get(1) instanceof Foods) && (o1.get(0) instanceof List)){
                        foods = (Foods) o1.get(1);
                        products = (List<Product>) o1.get(0);
                    }
                    if ((products == null) || (foods == null)) { return; }
                    for (int i = 0; i < products.size(); i++){
                        products.get(i).setParentGroup(SALES_ID);
                        products.get(i).setIsIncludedInMenu(true);
                        products.get(i).setIsDeleted(false);
                    }
                    if (products.size() > 0 ) {
                        Group g = new Group();
                        g.setName("Акция");
                        g.setOrder(-1);
                        g.setIsIncludedInMenu(true);
                        g.setIsDeleted(false);
                        g.setId(SALES_ID);
                        foods.getGroups().add(g);
                    }
                    foods.getProducts().addAll(products);
                    mRealmHelper.upd(foods); mRxEventBus.post(foods);
                },throwable -> {
                    mRxEventBus.post(new NOCONNECT());
                });
        Observable.just(mPreferencesHelper.getPhone())
                .subscribeOn(Schedulers.io())
                .filter(s->s.length()>9)
                .flatMap(s->mApiHelper.getToken(login,pass))
                .subscribeOn(Schedulers.io())
                .flatMap(t->mApiHelper.getCustomer(t,mPreferencesHelper.getPhone()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(cus -> mRxEventBus.post(mRealmHelper.addUser(cus)))
                .subscribe(r->{},t->{});
    }

    public Observable<Object> sendSms(String phone, String code){
        return mSmsHelper.send(phone,code)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()); }

    public Observable<Boolean> checkAdress(CusOrder.Address address){
        return mApiHelper.getToken(login,pass)
                .subscribeOn(Schedulers.io())
                .flatMap(s -> mApiHelper.checkAdress(s, address))
                .map(r->r.getAddressInZone())
                .onErrorReturn(throwable -> true)
                .observeOn(AndroidSchedulers.mainThread()); }

    private Observable<Customer> createUser(String number, Boolean needPush){
        return mApiHelper.getToken(login,pass)
                .subscribeOn(Schedulers.io())
                .flatMap(s -> mApiHelper.createCustomer(s, new CreateCustomer(new Customer("7" + number))))
                .flatMap(s-> checkUser("7"+number, needPush))
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Customer> checkUser(String number, Boolean needPush){
        return mApiHelper.getToken(login,pass)
                .subscribeOn(Schedulers.io())
                .flatMap(s -> mApiHelper.getCustomer(s,"7"+number))
                .onErrorReturnItem(new Customer())
                .flatMap(c-> {
                    if (c.getPhone().equals("xxx")) return createUser("7" + number, needPush);
                    else return Observable.just(c);
                })
                .onErrorReturnItem(new Customer(number.length()==10 ? "7"+number : number))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(cus -> {
                    clearHistory();
                    if (needPush) {
                        mPreferencesHelper.setPhone(cus.getPhone());
                        mRxEventBus.post(mRealmHelper.addUser(cus));
                        Crashlytics.setUserIdentifier(cus.getPhone());
                        mSmartApi.addFCM(cus.getName(), cus.getPhone(), FirebaseInstanceId.getInstance().getToken(),cus.getBirthday())
                        .subscribeOn(Schedulers.io()).subscribe(c->{},Throwable::printStackTrace);
                    }
                });
    }

    public Observable<Customer> reloadUser(String number){
        return mApiHelper.getToken(login,pass)
                .flatMap(s -> mApiHelper.getCustomer(s,mPreferencesHelper.getPhone()))
                .onErrorReturnItem(new Customer("xxx"))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(cus -> {
                    if (cus.getName().equals("xxx")){
                        mRxEventBus.post(mRealmHelper.getUser(mPreferencesHelper.getPhone()));
                    }else {
                        mPreferencesHelper.setPhone(cus.getPhone());
                        mRxEventBus.post(mRealmHelper.addUser(cus));
                    }
                });
    }

    public Observable<String> updUser(String number, String name, int sex, String birth){
        CreateCustomer customer = new CreateCustomer(new Customer(number.substring(1),name,sex,birth));
        return mApiHelper.getToken(login,pass)
                .subscribeOn(Schedulers.io())
                .flatMap(s -> mApiHelper.createCustomer(s, customer))
                .onErrorReturnItem("")
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(cus -> mRxEventBus.post(mRealmHelper.updUser(number,name,sex,birth)));
    }

    public @Nullable lCustomer findUser(String phone){ return mRealmHelper.getUser(phone); }

    public PreferencesHelper getPreferencesHelper() { return mPreferencesHelper; }

    public RxEventBus getRxEventBus() { return mRxEventBus; }

    public List<lGroup> getGroups(){
        lGroup start = new lGroup();
        start.setAll("0",false,"Избранное",true,0,"","");
        if (mPreferencesHelper.getStars().length > 0) {
            List<lGroup> groups = new ArrayList<>();
            if (mRealmHelper.getGroups().size() > 0) {
                groups.add(start);
                for (lGroup group : mRealmHelper.getGroups()) groups.add(group);
            }
            return groups;
        }
        return mRealmHelper.getGroups();
    }

    public lGroup getGroup(String id){ return mRealmHelper.getGroup(id); }

    public List<lProduct> getProducts(String id){
        if ( id.equals("0") ){
            List<lProduct> products = new ArrayList<>();
            for (String star : mPreferencesHelper.getStars()) {
                lProduct p = mRealmHelper.getProductById(star);
                if (p != null) products.add(p);
            }
            return products;
        }
        return mRealmHelper.getProducts(id);
    }

    public List<lProduct> getProducts(List<String> guids){ return mRealmHelper.getProduct(guids); }

    public Boolean isGroupHavePodGroup(String idGroup){ return mRealmHelper.isHavePodGroup(idGroup);}

    public List<lProduct> getProductsWhithPodGroups(String idGroup){
        List<lProduct> result = new ArrayList<>();
        for (lGroup group : mRealmHelper.getPodGroup(idGroup)){
            lProduct p = new lProduct();
            p.setName(group.getName());
            result.add(p);
            result.addAll(getProducts(group.getId()));
        }
        return result;
    }

    public List<lProduct> getProductsFromProduct(lProduct product){
        List<lProduct> result = new ArrayList<>();
        lGroup g = mRealmHelper.getGroup(product.getParentGroup());
        if (g.getParentGroup() != null) {
            for (lGroup group : mRealmHelper.getPodGroup(g.getParentGroup())) {
                result.addAll(getProducts(group.getId()));
            }
        }else result.addAll(getProducts(g.getId()));
        return result;
    }

    public List<lProduct> getProductsByName(String name){
        List<lProduct> result = new ArrayList<>();
        List<String> id = new ArrayList<>();
        for(lProduct prod : mRealmHelper.getProductsByName(name)){
            lGroup g = mRealmHelper.getGroup(prod.getParentGroup());
            if (!id.contains(prod.getParentGroup())) {
                id.add(prod.getParentGroup());
                lProduct p = new lProduct();
                if (g != null){ p.setName(g.getName()); result.add(p); }
            }
            if (g != null){ result.add(prod); }
        }
        return result;
    }

    public List<lProduct> getProducts(){
        List<lProduct> result = new ArrayList<>();
        List<String> id = new ArrayList<>();
        for(lProduct prod : mRealmHelper.getProducts()){
            if (!id.contains(prod.getParentGroup())) {
                id.add(prod.getParentGroup());
                lProduct p = new lProduct();
                p.setName(mRealmHelper.getGroup(prod.getParentGroup()).getName());
                result.add(p);
            }
            result.add(prod);
        }
        return result;
    }

    public RealmHelper.OrderInfo getOrderInfo(){
        return mRealmHelper.getOrderInfo();
    }

    public String addHis(List<CusOrder.Item> items){
        List<lOrder> re = new ArrayList<>();
        Boolean no = false;
        for( CusOrder.Item item : items){
            lProduct p = mRealmHelper.getProductById(item.getId());
            if (p!=null) {
                try{
                    String id = item.getModifiers().get(0).getId();
                    List<String> ids = new ArrayList<>();
                    ids.add(id);
                    lProduct m = mRealmHelper.getProduct(ids).get(0);
                    re.add(new lOrder().setAll(p, item.getAmount(), m.getId(), m.getName(), (int) m.getPrice()));
                } catch (Exception e) {
                    re.add(new lOrder().setAll(p, item.getAmount(), "", "", 0));
                }
            }  else no = true;
        }
        if ( re.size() == 0 ) return "Невозможно повторить заказ, в данном заказе нет активных товаров";
        clearOrder();
        for (lOrder o : re){
            addZak(o.getCount(),o.getlProduct(),o.getModificator(),o.getNameM(), (int) o.getPriceM());
        }
        if ( no) return "В данном заказе не все позиции товаров активны для продажи";
        return null;
    }

    public void addZak(int count, lProduct product, String modificator, String nameM, int priceM){
        mRealmHelper.addOrder(count,product,modificator, nameM, priceM);
        mRxEventBus.post(mRealmHelper.getOrderInfo());
        mRxEventBus.post(product);
    }

    public void minusZak(int count, lProduct product, String modificator, int priceM){
        mRealmHelper.minusOrder(count,product,modificator, priceM);
        mRxEventBus.post(mRealmHelper.getOrderInfo());
        mRxEventBus.post(product);
    }

    public void deleteZak(lProduct product, String modificator){
        mRealmHelper.deleteOrder(product,modificator);
        mRxEventBus.post(mRealmHelper.getOrderInfo());
        mRxEventBus.post(product);
    }

    public List<lOrder> getOrders(){ return mRealmHelper.getOrder();}

    public void clearOrder(){
        mRealmHelper.clearOrder();
        mRxEventBus.post(mRealmHelper.getOrderInfo());
    }

    public Observable<List<StreetResult.Street>> getStreets(){
        return Observable.create((ObservableEmitter<List<StreetResult.Street>> obs) -> {
            if (mPreferencesHelper.getStreets().size() > 0) {
                obs.onNext(mPreferencesHelper.getStreets());
                obs.onComplete();
            }
            mApiHelper.getToken(login,pass)
                .flatMap(token -> mApiHelper.getAdressList(token))
                .map(r -> r.get(0).getStreets())
                .map(list -> new ArrayList<>(list))
                .subscribeOn(Schedulers.io())
                .subscribe(r -> {
                    mPreferencesHelper.setStreets(r);
                    obs.onNext(r); obs.onComplete();
                }, t-> {
                    if (!obs.isDisposed()){ obs.onError(t); obs.onComplete();}
                });
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public int createAdres(String street, String home, String entrance, String apartment, String floor, String doorphone, String comment){
        return  mRealmHelper.createAdres(street, home, entrance, apartment, floor, doorphone, comment);
    }

    public Observable<List<lAdres>> getAdres(){ return Observable.fromArray(mRealmHelper.getAdres())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()); }

    public List<lAdres> getAdresNoObs(){ return  mRealmHelper.getAdres(); }

    public void delete(int id){ mRealmHelper.delete(id); }

    public Observable<Restorans> getRestorans(){
        return Observable.create((ObservableEmitter<Restorans> obs) -> {
            if (mPreferencesHelper.getRestorans() != null) {
                obs.onNext(mPreferencesHelper.getRestorans());
                obs.onComplete();
            }
            mApiHelper.getToken(login,pass)
                    .flatMap(s->mApiHelper.getRestorans(s))
                    .subscribeOn(Schedulers.io())
                    .subscribe(r -> {
                        mPreferencesHelper.setRestorans(r);
                        obs.onNext(r); obs.onComplete();
                    }, t-> {
                        if (!obs.isDisposed()) { obs.onError(t); obs.onComplete();  }
                    });
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<SmartRestoran>> getSmartRestorans(){
        return Observable.create((ObservableEmitter<List<SmartRestoran>> obs) -> {
            obs.onNext(mPreferencesHelper.getSmartRestoran());
            mSmartApi.getRestorans()
                    .subscribeOn(Schedulers.io())
                    .subscribe(r -> {
                        mPreferencesHelper.setSmartRestoran(r);
                        obs.onNext(r); obs.onComplete();
                    }, t-> {
                        if (!obs.isDisposed()) {  obs.onError(t); obs.onComplete(); }
                    });
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<Boolean> addOrder(CusOrder order){
        return mApiHelper.getToken(login,pass)
                .flatMap(s->mApiHelper.addOrder(s,order))
                .subscribeOn(Schedulers.io())
                .map(r->true)
                .onErrorReturn(t->false)
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<Boolean> addOrderSmart(CusOrder order){
        return mSmartApi.addOrder(order)
                .subscribeOn(Schedulers.io())
                .map(r->true)
                .onErrorReturn(t->false)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Boolean> addOrderFinale(CusOrder order) {
        return Observable.mergeArray(addOrderSmart(order), addOrder(order)).toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(rs-> rs.get(0) || rs.get(1));
    }

    public lProduct getProductById(String id){
        return mRealmHelper.getProductById(id);
    }

    public Observable<History> getHistory(){
        return Observable.create((ObservableEmitter<History> obs) -> {
            if (mPreferencesHelper.getHistory() != null) obs.onNext(mPreferencesHelper.getHistory());
            mApiHelper.getToken(login,pass)
                    .subscribeOn(Schedulers.io())
                    .flatMap(s->mApiHelper.getHistory(s,getPreferencesHelper().getPhone()))
                    .subscribeOn(Schedulers.io())
                    .subscribe(r -> {
                        mPreferencesHelper.setHistory(r);
                        obs.onNext(r);
                        obs.onComplete();
                    }, t-> { if (!obs.isDisposed()) { obs.onComplete(); } });
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public void clearHistory(){
        mPreferencesHelper.clearHistory();
    }

    public static class StarUpd{
        public boolean needList;
        public StarUpd(boolean needList) { this.needList = needList; }
    }

    public void removeIdStar(String id){
        mPreferencesHelper.removeIdStar(id);
        mRxEventBus.post(new StarUpd(mPreferencesHelper.getStars().length == 0));
    }

    public void addIdStar(String id){
        mPreferencesHelper.addIdStar(id);
        mRxEventBus.post(new StarUpd(mPreferencesHelper.getStars().length == 1));
    }

    public boolean isStart(String id){
        for (String star : mPreferencesHelper.getStars()){
            if (star.equals(id)) return true;
        }
        return false;
    }

    public Observable<List<Action>> getActions(){
        return Observable.create((ObservableEmitter<List<Action>> obs) -> {
            obs.onNext(mPreferencesHelper.getActions());
            mSmartApi.getActions()
                    .subscribeOn(Schedulers.io())
                    .subscribe(r -> {
                mPreferencesHelper.setActions(r);
                obs.onNext(r);
                obs.onComplete();
            }, t-> { if (!obs.isDisposed()) {  obs.onComplete();  } });
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Product>> getSales(){
        return mSmartApi.getSales()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
    }

    public static Long ordId;
    public Observable<SberApi.Response> getUrl(int amount){
        ordId = System.currentTimeMillis() + ((new Random().nextInt(100)) * 100);
        return mSberApi.pay(amount * 100,ordId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<SberApi.ResponsePay> googlePay(String token, int price){
        ordId = System.currentTimeMillis() + ((new Random().nextInt(100)) * 100);
        return mSberApi.googlePay(new SberApi.GoogleLayClass(token,
                ordId,
                price * 100
        ))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private static String cText = "Акцию можно заказать в период в 13:00-16:00, предварительный заказ можно сделать позвонив в колл-центр  \n8 (843) 233-44-33";
    public Observable<String> checkProduct(){
        return mSmartApi.isSales()
                .cache()
                .subscribeOn(Schedulers.io())
                .map(r->r.data)
                .onErrorReturn(t->false)
                .map(r->{
                    Calendar c = ViewUtil.time();
                    if (c.get(Calendar.HOUR_OF_DAY) >= 13 && c.get(Calendar.HOUR_OF_DAY) <= 15
                            && c.get(Calendar.DAY_OF_WEEK) != 7 && c.get(Calendar.DAY_OF_WEEK) != 1 && r) return "";
                    return cText;
                })
                .onErrorReturn(t->"Не возможно оформить заказ. Отсутствует подключение к интернету.")
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<SmartApi.SmartPushs> getPushs(){
        return Observable.create((ObservableEmitter<SmartApi.SmartPushs> obs) -> {
            if (mPreferencesHelper.getPushs() != null) obs.onNext(mPreferencesHelper.getPushs());
            mSmartApi.getPushs(mPreferencesHelper.getPhone())
                    .subscribeOn(Schedulers.io())
                    .subscribe(r -> {
                        mPreferencesHelper.setPushs(r);
                        obs.onNext(r);
                        obs.onComplete();
                    }, t-> { if (!obs.isDisposed()) {  obs.onComplete();  }  });
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Boolean> sendMarks(String id, int f, int s, int t, String comment){
        ApiHelper.Marks marks = new ApiHelper.Marks();
        marks.comment = comment;
        marks.deliveryId = id;
        marks.marks.add(new ApiHelper.Mark("63768149-1859-9605-014d-f98e67010002",f));
        marks.marks.add(new ApiHelper.Mark("63768149-1859-9605-014d-f98e67010003",s));
        marks.marks.add(new ApiHelper.Mark("63768149-1859-9605-014d-f98e67010004",t));
        return mApiHelper.getToken(login,pass)
                .subscribeOn(Schedulers.io())
                .flatMap(token->mApiHelper.sendMark(token,marks))
                .subscribeOn(Schedulers.io())
                .map(o->true)
                .onErrorReturn((throwable)->false)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(b->{
                    String text = "Оставил отзыв. Спасибо за доставку!";
                    if (comment.length() > 3) text = comment;
                    if (b) { mRealmHelper.addMark(id,text,""); }
                });
    }

    public String getMark(String id){
        lMark m = mRealmHelper.getMark(id);
        if (m != null) return m.getText();
        return null;
    }

    public Observable<String> checkSales(CusOrder order){
        return mApiHelper.getToken(login,pass)
                .subscribeOn(Schedulers.io())
                .flatMap(token->mApiHelper.checkSale(token,order))
                .subscribeOn(Schedulers.io())
                .map(r-> r.loyatyResult)
                .map(l-> {
                    String s = "";
                    for (ApiHelper.ProgramResult r : l.programResults) s = s + r.name + "; ";
                    return s;
                })
                .onErrorReturn(t->"");
    }

    public Observable<Boolean> checkVersion(){
        return mSmartApi.getVersion()
                .map(b->b.getData())
                .map(s-> {
                    String[] version1 = s.split("[.]");
                    String[] version2 = BuildConfig.VERSION_NAME.split("[.]");
                    int v1 = 0, v2 = 0;
                    for (String a : version1) v1 = v1 * 1000 + Integer.valueOf(a);
                    for (String a : version2) v2 = v2 * 1000 + Integer.valueOf(a);
                    return v1 <= v2;
                })
                .onErrorReturn(t->true)
                .subscribeOn(Schedulers.io());
    }
}
