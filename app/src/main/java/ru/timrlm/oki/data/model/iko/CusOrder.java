package ru.timrlm.oki.data.model.iko;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.timrlm.oki.ui.devilary.DevilaryActivity;

public class CusOrder {

    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("order")
    @Expose
    private Order order;
    @SerializedName("deliveryTerminalId")
    @Expose
    private String deliveryTerminalId;
    @SerializedName("coupon")
    @Expose
    private String coupon;

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getDeliveryTerminalId() {
        return deliveryTerminalId;
    }

    public void setDeliveryTerminalId(String deliveryTerminalId) {
        this.deliveryTerminalId = deliveryTerminalId;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public static class Address {

        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("street")
        @Expose
        private String street;
        @SerializedName("home")
        @Expose
        private String home;
        @SerializedName("entrance")
        @Expose
        private String entrance;
        @SerializedName("apartment")
        @Expose
        private String apartment;
        @SerializedName("floor")
        @Expose
        private String floor;
        @SerializedName("doorphone")
        @Expose
        private String doorphone;
        @SerializedName("comment")
        @Expose
        private String comment;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getHome() {
            return home;
        }

        public void setHome(String home) {
            this.home = home;
        }

        public String getHousing() {
            return entrance;
        }

        public void setHousing(String housing) {
            this.entrance = housing;
        }

        public String getApartment() {
            return apartment;
        }

        public void setApartment(String apartment) {
            this.apartment = apartment;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getEntrance() {
            return entrance;
        }

        public void setEntrance(String entrance) {
            this.entrance = entrance;
        }

        public String getFloor() {
            return floor;
        }

        public void setFloor(String floor) {
            this.floor = floor;
        }

        public String getDoorphone() {
            return doorphone;
        }

        public void setDoorphone(String doorphone) {
            this.doorphone = doorphone;
        }

        public void setAll(String city, String street, String home, String entrance, String apartment, String floor, String doorphone, String comment) {
            this.city = city;
            this.street = street;
            this.home = home;
            this.entrance = entrance;
            this.apartment = apartment;
            this.floor = floor;
            this.doorphone = doorphone;
            this.comment = comment;
        }
    }

    public static class Customer {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("phone")
        @Expose
        private String phone;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public void setAll(String id, String name, String phone) {
            this.id = id;
            this.name = name;
            this.phone = phone;
        }
    }

    public static class Item {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("amount")
        @Expose
        private Integer amount;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("sum")
        @Expose
        private long sum;
        @SerializedName("modifiers")
        @Expose
        private List<Modifiers> modifiers = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public long getSum() {
            return sum;
        }

        public void setSum(long sum) {
            this.sum = sum;
        }

        public void setAll(String id, String name, Integer amount, String code) {
            this.id = id;
            this.name = name;
            this.amount = amount;
            this.code = code;
        }

        public List<Modifiers> getModifiers() {
            return modifiers;
        }

        public void setModifiers(List<Modifiers> modifiers) {
            this.modifiers = modifiers;
        }
    }

    public static class Modifiers{
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("amount")
        @Expose
        private Integer amount;
        @SerializedName("groupId")
        @Expose
        private String groupId;
        @SerializedName("groupName")
        @Expose
        private String groupName;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }
    }

    public static class Order {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("isSelfService")
        @Expose
        private String isSelfService;
        @SerializedName("items")
        @Expose
        private List<Item> items = null;
        @SerializedName("address")
        @Expose
        private Address address;
        @SerializedName("paymentItems")
        @Expose
        private List<PaymentItem> paymentItems = null;
        @SerializedName("comment")
        @Expose
        private String comment;
        @SerializedName("orderTypeId")
        @Expose
        private String orderTypeId;
        @SerializedName("personsCount")
        @Expose
        private Integer personsCount = DevilaryActivity.COUNT;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getIsSelfService() {
            return isSelfService;
        }

        public void setIsSelfService(String isSelfService) {
            this.isSelfService = isSelfService;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

        public List<PaymentItem> getPaymentItems() {
            return paymentItems;
        }

        public void setPaymentItems(List<PaymentItem> paymentItems) {
            this.paymentItems = paymentItems;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getOrderTypeId() {
            return orderTypeId;
        }

        public void setOrderTypeId(String orderTypeId) {
            this.orderTypeId = orderTypeId;
        }
    }

    public static class PaymentItem {

        @SerializedName("sum")
        @Expose
        private String sum;
        @SerializedName("paymentType")
        @Expose
        private PaymentType paymentType;
        @SerializedName("isProcessedExternally")
        @Expose
        private Boolean isProcessedExternally;
        @SerializedName("additionalData")
        @Expose
        private String additionalData;


        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }

        public PaymentType getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(PaymentType paymentType) {
            this.paymentType = paymentType;
        }

        public Boolean getIsProcessedExternally() {
            return isProcessedExternally;
        }

        public void setIsProcessedExternally(Boolean isProcessedExternally) {
            this.isProcessedExternally = isProcessedExternally;
        }

        public String getAdditionalData() {
            return additionalData;
        }

        public void setAdditionalData(String additionalData) {
            this.additionalData = additionalData;
        }
    }

    public static class PaymentType {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("comment")
        @Expose
        private Object comment;
        @SerializedName("combinable")
        @Expose
        private Boolean combinable;
        @SerializedName("externalRevision")
        @Expose
        private Integer externalRevision;
        @SerializedName("applicableMarketingCampaigns")
        @Expose
        private Object applicableMarketingCampaigns;
        @SerializedName("deleted")
        @Expose
        private Boolean deleted;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getComment() {
            return comment;
        }

        public void setComment(Object comment) {
            this.comment = comment;
        }

        public Boolean getCombinable() {
            return combinable;
        }

        public void setCombinable(Boolean combinable) {
            this.combinable = combinable;
        }

        public Integer getExternalRevision() {
            return externalRevision;
        }

        public void setExternalRevision(Integer externalRevision) {
            this.externalRevision = externalRevision;
        }

        public Object getApplicableMarketingCampaigns() {
            return applicableMarketingCampaigns;
        }

        public void setApplicableMarketingCampaigns(Object applicableMarketingCampaigns) {
            this.applicableMarketingCampaigns = applicableMarketingCampaigns;
        }

        public Boolean getDeleted() {
            return deleted;
        }

        public void setDeleted(Boolean deleted) {
            this.deleted = deleted;
        }

    }
}