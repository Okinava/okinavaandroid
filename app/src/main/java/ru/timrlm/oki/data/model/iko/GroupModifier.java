package ru.timrlm.oki.data.model.iko;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupModifier {

    @SerializedName("maxAmount")
    @Expose
    private Integer maxAmount;
    @SerializedName("minAmount")
    @Expose
    private Integer minAmount;
    @SerializedName("modifierId")
    @Expose
    private String modifierId;
    @SerializedName("required")
    @Expose
    private Boolean required;
    @SerializedName("childModifiers")
    @Expose
    private List<ChildModifier> childModifiers = null;
    @SerializedName("childModifiersHaveMinMaxRestrictions")
    @Expose
    private Boolean childModifiersHaveMinMaxRestrictions;

    public Integer getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Integer minAmount) {
        this.minAmount = minAmount;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public List<ChildModifier> getChildModifiers() {
        return childModifiers;
    }

    public void setChildModifiers(List<ChildModifier> childModifiers) {
        this.childModifiers = childModifiers;
    }

    public Boolean getChildModifiersHaveMinMaxRestrictions() {
        return childModifiersHaveMinMaxRestrictions;
    }

    public void setChildModifiersHaveMinMaxRestrictions(Boolean childModifiersHaveMinMaxRestrictions) {
        this.childModifiersHaveMinMaxRestrictions = childModifiersHaveMinMaxRestrictions;
    }

}
