package ru.timrlm.oki.data.model.iko;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Foods {

    @SerializedName("groups")
    @Expose
    private List<Group> groups = null;
    @SerializedName("productCategories")
    @Expose
    private List<ProductCategory> productCategories = null;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;
    @SerializedName("revision")
    @Expose
    private Integer revision;
    @SerializedName("uploadDate")
    @Expose
    private String uploadDate;

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<ProductCategory> getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(List<ProductCategory> productCategories) {
        this.productCategories = productCategories;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

}