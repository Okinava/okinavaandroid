package ru.timrlm.oki.data.model.realm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by timerlan on 28.03.2018.
 */

public class lProduct extends RealmObject {
    @PrimaryKey
    private String id;
    private String code;
    private String description;
    private Boolean isDeleted;
    private String name;
    private Double carbohydrateAmount;
    private Double carbohydrateFullAmount;
    private Boolean doNotPrintInCheque;
    private Double energyAmount;
    private Double energyFullAmount;
    private Double fatAmount;
    private Double fatFullAmount;
    private Double fiberAmount;
    private Double fiberFullAmount;
    private String measureUnit;
    private Double price;
    private String type;
    private Boolean useBalanceForSell;
    private Double weight;
    private Boolean isIncludedInMenu;
    private Integer order;
    private String parentGroup;
    private String img;
    private String modifaers;
    private String modifaersId;

    public void setAll(String id, String code, String description, Boolean isDeleted, String name, Double carbohydrateAmount,
                       Double carbohydrateFullAmount, Boolean doNotPrintInCheque, Double energyAmount,
                       Double energyFullAmount, Double fatAmount, Double fatFullAmount, Double fiberAmount,
                       Double fiberFullAmount, String measureUnit, Double price, String type, Boolean useBalanceForSell,
                       Double weight, Boolean isIncludedInMenu, Integer order, String parentGroup, String img, String modifaers, String modifaersId) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.isDeleted = isDeleted;
        this.name = name;
        this.carbohydrateAmount = carbohydrateAmount;
        this.carbohydrateFullAmount = carbohydrateFullAmount;
        this.doNotPrintInCheque = doNotPrintInCheque;
        this.energyAmount = energyAmount;
        this.energyFullAmount = energyFullAmount;
        this.fatAmount = fatAmount;
        this.fatFullAmount = fatFullAmount;
        this.fiberAmount = fiberAmount;
        this.fiberFullAmount = fiberFullAmount;
        this.measureUnit = measureUnit;
        this.price = price;
        this.type = type;
        this.useBalanceForSell = useBalanceForSell;
        this.weight = weight;
        this.isIncludedInMenu = isIncludedInMenu;
        this.order = order;
        this.parentGroup = parentGroup;
        this.img = img;
        this.modifaers = modifaers;
        this.modifaersId = modifaersId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getName() {
        if (name.indexOf("-") == 0) { return name.substring(1); }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCarbohydrateAmount() {
        return carbohydrateAmount;
    }

    public void setCarbohydrateAmount(Double carbohydrateAmount) {
        this.carbohydrateAmount = carbohydrateAmount;
    }

    public Double getCarbohydrateFullAmount() {
        return carbohydrateFullAmount;
    }

    public void setCarbohydrateFullAmount(Double carbohydrateFullAmount) {
        this.carbohydrateFullAmount = carbohydrateFullAmount;
    }

    public Boolean getDoNotPrintInCheque() {
        return doNotPrintInCheque;
    }

    public void setDoNotPrintInCheque(Boolean doNotPrintInCheque) {
        this.doNotPrintInCheque = doNotPrintInCheque;
    }

    public Double getEnergyAmount() {
        return energyAmount;
    }

    public void setEnergyAmount(Double energyAmount) {
        this.energyAmount = energyAmount;
    }

    public Double getEnergyFullAmount() {
        return energyFullAmount;
    }

    public void setEnergyFullAmount(Double energyFullAmount) {
        this.energyFullAmount = energyFullAmount;
    }

    public Double getFatAmount() {
        return fatAmount;
    }

    public void setFatAmount(Double fatAmount) {
        this.fatAmount = fatAmount;
    }

    public Double getFatFullAmount() {
        return fatFullAmount;
    }

    public void setFatFullAmount(Double fatFullAmount) {
        this.fatFullAmount = fatFullAmount;
    }

    public Double getFiberAmount() {
        return fiberAmount;
    }

    public void setFiberAmount(Double fiberAmount) {
        this.fiberAmount = fiberAmount;
    }

    public Double getFiberFullAmount() {
        return fiberFullAmount;
    }

    public void setFiberFullAmount(Double fiberFullAmount) {
        this.fiberFullAmount = fiberFullAmount;
    }

    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    public long getPrice() {
        return Math.round(price);
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getUseBalanceForSell() {
        return useBalanceForSell;
    }

    public void setUseBalanceForSell(Boolean useBalanceForSell) {
        this.useBalanceForSell = useBalanceForSell;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Boolean getIncludedInMenu() {
        return isIncludedInMenu;
    }

    public void setIncludedInMenu(Boolean includedInMenu) {
        isIncludedInMenu = includedInMenu;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(String parentGroup) {
        this.parentGroup = parentGroup;
    }

    public String getImg() { return img; }

    public void setImg(String img) { this.img = img; }

    public String getModifaers() {
        return modifaers;
    }

    public void setModifaers(String modifaers) {
        this.modifaers = modifaers;
    }

    public String getModifaersId() {
        return modifaersId;
    }

    public void setModifaersId(String modifaersId) {
        this.modifaersId = modifaersId;
    }
}
