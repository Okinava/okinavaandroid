package ru.timrlm.oki.data.model.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class lMark  extends RealmObject {

    @PrimaryKey
    private String id;
    private String time;
    private String text;

    public lMark setAll(String id, String text, String time) {
        this.id = id;
        this.text = text;
        this.time = time;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
