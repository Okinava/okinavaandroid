package ru.timrlm.oki.data.model.iko;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChildModifier {

    @SerializedName("maxAmount")
    @Expose
    private Integer maxAmount;
    @SerializedName("minAmount")
    @Expose
    private Integer minAmount;
    @SerializedName("modifierId")
    @Expose
    private String modifierId;
    @SerializedName("required")
    @Expose
    private Boolean required;
    @SerializedName("defaultAmount")
    @Expose
    private Integer defaultAmount;
    @SerializedName("hideIfDefaultAmount")
    @Expose
    private Boolean hideIfDefaultAmount;

    public Integer getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Integer minAmount) {
        this.minAmount = minAmount;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Integer getDefaultAmount() {
        return defaultAmount;
    }

    public void setDefaultAmount(Integer defaultAmount) {
        this.defaultAmount = defaultAmount;
    }

    public Boolean getHideIfDefaultAmount() {
        return hideIfDefaultAmount;
    }

    public void setHideIfDefaultAmount(Boolean hideIfDefaultAmount) {
        this.hideIfDefaultAmount = hideIfDefaultAmount;
    }

}