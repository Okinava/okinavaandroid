package ru.timrlm.oki.data.model.iko;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("additionalInfo")
    @Expose
    private Object additionalInfo;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("seoDescription")
    @Expose
    private Object seoDescription;
    @SerializedName("seoKeywords")
    @Expose
    private Object seoKeywords;
    @SerializedName("seoText")
    @Expose
    private Object seoText;
    @SerializedName("seoTitle")
    @Expose
    private Object seoTitle;
    @SerializedName("tags")
    @Expose
    private Object tags;
    @SerializedName("carbohydrateAmount")
    @Expose
    private Double carbohydrateAmount;
    @SerializedName("carbohydrateFullAmount")
    @Expose
    private Double carbohydrateFullAmount;
    @SerializedName("differentPricesOn")
    @Expose
    private List<Object> differentPricesOn = null;
    @SerializedName("doNotPrintInCheque")
    @Expose
    private Boolean doNotPrintInCheque;
    @SerializedName("energyAmount")
    @Expose
    private Double energyAmount;
    @SerializedName("energyFullAmount")
    @Expose
    private Double energyFullAmount;
    @SerializedName("fatAmount")
    @Expose
    private Double fatAmount;
    @SerializedName("fatFullAmount")
    @Expose
    private Double fatFullAmount;
    @SerializedName("fiberAmount")
    @Expose
    private Double fiberAmount;
    @SerializedName("fiberFullAmount")
    @Expose
    private Double fiberFullAmount;
    @SerializedName("groupId")
    @Expose
    private String groupId;
    @SerializedName("groupModifiers")
    @Expose
    private List<GroupModifier> groupModifiers = null;
    @SerializedName("measureUnit")
    @Expose
    private String measureUnit;
    @SerializedName("modifiers")
    @Expose
    private List<Object> modifiers = null;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("productCategoryId")
    @Expose
    private String productCategoryId;
    @SerializedName("prohibitedToSaleOn")
    @Expose
    private List<Object> prohibitedToSaleOn = null;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("useBalanceForSell")
    @Expose
    private Boolean useBalanceForSell;
    @SerializedName("weight")
    @Expose
    private Double weight;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("isIncludedInMenu")
    @Expose
    private Boolean isIncludedInMenu;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("parentGroup")
    @Expose
    private String parentGroup;

    public Object getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(Object additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getSeoDescription() {
        return seoDescription;
    }

    public void setSeoDescription(Object seoDescription) {
        this.seoDescription = seoDescription;
    }

    public Object getSeoKeywords() {
        return seoKeywords;
    }

    public void setSeoKeywords(Object seoKeywords) {
        this.seoKeywords = seoKeywords;
    }

    public Object getSeoText() {
        return seoText;
    }

    public void setSeoText(Object seoText) {
        this.seoText = seoText;
    }

    public Object getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(Object seoTitle) {
        this.seoTitle = seoTitle;
    }

    public Object getTags() {
        return tags;
    }

    public void setTags(Object tags) {
        this.tags = tags;
    }

    public Double getCarbohydrateAmount() {
        return carbohydrateAmount;
    }

    public void setCarbohydrateAmount(Double carbohydrateAmount) {
        this.carbohydrateAmount = carbohydrateAmount;
    }

    public Double getCarbohydrateFullAmount() {
        return carbohydrateFullAmount;
    }

    public void setCarbohydrateFullAmount(Double carbohydrateFullAmount) {
        this.carbohydrateFullAmount = carbohydrateFullAmount;
    }

    public List<Object> getDifferentPricesOn() {
        return differentPricesOn;
    }

    public void setDifferentPricesOn(List<Object> differentPricesOn) {
        this.differentPricesOn = differentPricesOn;
    }

    public Boolean getDoNotPrintInCheque() {
        return doNotPrintInCheque;
    }

    public void setDoNotPrintInCheque(Boolean doNotPrintInCheque) {
        this.doNotPrintInCheque = doNotPrintInCheque;
    }

    public Double getEnergyAmount() {
        return energyAmount;
    }

    public void setEnergyAmount(Double energyAmount) {
        this.energyAmount = energyAmount;
    }

    public Double getEnergyFullAmount() {
        return energyFullAmount;
    }

    public void setEnergyFullAmount(Double energyFullAmount) {
        this.energyFullAmount = energyFullAmount;
    }

    public Double getFatAmount() {
        return fatAmount;
    }

    public void setFatAmount(Double fatAmount) {
        this.fatAmount = fatAmount;
    }

    public Double getFatFullAmount() {
        return fatFullAmount;
    }

    public void setFatFullAmount(Double fatFullAmount) {
        this.fatFullAmount = fatFullAmount;
    }

    public Double getFiberAmount() {
        return fiberAmount;
    }

    public void setFiberAmount(Double fiberAmount) {
        this.fiberAmount = fiberAmount;
    }

    public Double getFiberFullAmount() {
        return fiberFullAmount;
    }

    public void setFiberFullAmount(Double fiberFullAmount) {
        this.fiberFullAmount = fiberFullAmount;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public List<GroupModifier> getGroupModifiers() {
        return groupModifiers;
    }

    public void setGroupModifiers(List<GroupModifier> groupModifiers) {
        this.groupModifiers = groupModifiers;
    }

    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    public List<Object> getModifiers() {
        return modifiers;
    }

    public void setModifiers(List<Object> modifiers) {
        this.modifiers = modifiers;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public List<Object> getProhibitedToSaleOn() {
        return prohibitedToSaleOn;
    }

    public void setProhibitedToSaleOn(List<Object> prohibitedToSaleOn) {
        this.prohibitedToSaleOn = prohibitedToSaleOn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getUseBalanceForSell() {
        return useBalanceForSell;
    }

    public void setUseBalanceForSell(Boolean useBalanceForSell) {
        this.useBalanceForSell = useBalanceForSell;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Boolean getIsIncludedInMenu() {
        return isIncludedInMenu;
    }

    public void setIsIncludedInMenu(Boolean isIncludedInMenu) {
        this.isIncludedInMenu = isIncludedInMenu;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(String parentGroup) {
        this.parentGroup = parentGroup;
    }

}