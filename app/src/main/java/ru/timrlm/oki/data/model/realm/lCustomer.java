package ru.timrlm.oki.data.model.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ru.timrlm.oki.data.model.iko.Customer;

public class lCustomer extends RealmObject {

    @PrimaryKey private String phone;
    private String id;
    private String birthday;
    private String comment;
    private String cultureName;
    private String email;
    private Integer iikoCardOrdersSum;
    private Boolean isBlocked;
    private String middleName;
    private String name;
    private String rank;
    private Integer sex;
    private String surname;
    private Integer balance;
    private String walletId;
    private String status;
    private String card;

    public void setAll(Customer customer) {
        this.id = customer.getId();
        this.birthday = customer.getBirthday();
        this.comment = customer.getComment();
        this.cultureName = customer.getCultureName();
        this.email = customer.getEmail();
        this.iikoCardOrdersSum = customer.getIikoCardOrdersSum();
        this.isBlocked = customer.getBlocked();
        this.middleName = customer.getMiddleName();
        this.name = customer.getName();
        this.phone = customer.getPhone();
        this.rank = customer.getRank();
        this.sex = customer.getSex();
        this.surname = customer.getSurname();
        try{ this.status = customer.getCategories().get(0).getName();} catch (Exception e){ this.status = "Новый"; }
        try{ this.card = customer.getCards().get(0).getNumber(); } catch (Exception e){ this.card = "Нет бонусной карты"; }
        try {
            this.balance = customer.getWalletBalances().get(0).getBalance().intValue();
            this.walletId = customer.getWalletBalances().get(0).getWallet().getId();
        }catch (Exception e){
            this.balance = 0;
            this.walletId = "";
        }
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBirthday() {
        return birthday == null ? "Дата рождения не указана" : birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCultureName() {
        return cultureName;
    }

    public void setCultureName(String cultureName) {
        this.cultureName = cultureName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIikoCardOrdersSum() {
        return iikoCardOrdersSum;
    }

    public void setIikoCardOrdersSum(Integer iikoCardOrdersSum) {
        this.iikoCardOrdersSum = iikoCardOrdersSum;
    }

    public Boolean getBlocked() {
        return isBlocked;
    }

    public void setBlocked(Boolean blocked) {
        isBlocked = blocked;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Integer getSex() {
        if (sex == null) return 0;
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCard() {
        try {
            return card.length() > 0 ? card : "Нет бонусной карты";
        }catch (Exception e){
            return "Нет бонусной карты";
        }
    }

    public void setCard(String card) {
        this.card = card;
    }
}
