package ru.timrlm.oki.data.model.iko;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateCustomer {

    @SerializedName("customer")
    @Expose
    private Customer customer;

    public Customer getCustomer() { return customer; }

    public void setCustomer(Customer customer) { this.customer = customer; }

    public CreateCustomer(Customer customer) { this.customer = customer; }

    public CreateCustomer(){}
}