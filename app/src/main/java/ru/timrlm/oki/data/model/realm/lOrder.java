package ru.timrlm.oki.data.model.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class lOrder extends RealmObject {

    private String id;
    private lProduct lProduct;
    private int count = 1;
    private long sum = 0;
    private String modificator = "";
    private String nameM = "";
    private long priceM = 0;

    public lOrder setAll(ru.timrlm.oki.data.model.realm.lProduct lProduct, int count, String modificator, String nameM, int priceM) {
        this.id = lProduct.getId();
        this.lProduct = lProduct;
        this.count = count;
        this.priceM = priceM;
        this.sum = count * (lProduct.getPrice() + priceM);
        this.modificator = modificator;
        this.nameM = nameM;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ru.timrlm.oki.data.model.realm.lProduct getlProduct() {
        return lProduct;
    }

    public void setlProduct(ru.timrlm.oki.data.model.realm.lProduct lProduct) {
        this.lProduct = lProduct;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getSum() {
        return sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }

    public String getModificator() {
        return modificator;
    }

    public void setModificator(String modificator) {
        this.modificator = modificator;
    }

    public String getNameM() {
        return nameM;
    }

    public void setNameM(String nameM) {
        this.nameM = nameM;
    }

    public long getPriceM() {
        return priceM;
    }

    public void setPriceM(long priceM) {
        this.priceM = priceM;
    }
}
