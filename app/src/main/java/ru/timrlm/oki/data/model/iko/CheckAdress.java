package ru.timrlm.oki.data.model.iko;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckAdress {

    @SerializedName("addressInZone")
    @Expose
    private Boolean addressInZone;

    public Boolean getAddressInZone() {
        return addressInZone;
    }

    public void setAddressInZone(Boolean addressInZone) {
        this.addressInZone = addressInZone;
    }
}
