package ru.timrlm.oki.data.model.realm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by timerlan on 28.03.2018.
 */

public class lGroup extends RealmObject {

    @PrimaryKey
    private String id;
    private boolean isDeleted;
    private String name;
    private boolean isIncludedInMenu;
    private int order;
    private String parentGroup;
    private String img;

    public void setAll(String id, Boolean isDeleted, String name, Boolean isIncludedInMenu, Integer order, String parentGroup, String img) {
        this.id = id;
        this.isDeleted = isDeleted;
        this.name = name;
        this.isIncludedInMenu = isIncludedInMenu;
        this.order = order;
        this.parentGroup = parentGroup;
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIncludedInMenu() {
        return isIncludedInMenu;
    }

    public void setIncludedInMenu(Boolean includedInMenu) {
        isIncludedInMenu = includedInMenu;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(String parentGroup) {
        this.parentGroup = parentGroup;
    }

    public String getImg() { return img; }

    public void setImg(String img) { this.img = img; }
}
