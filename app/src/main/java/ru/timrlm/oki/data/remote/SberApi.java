package ru.timrlm.oki.data.remote;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.timrlm.oki.BuildConfig;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.data.model.smart.SmartRestoran;

public interface SberApi {
    String ENDPOINT = "https://securepayments.sberbank.ru/payment/";

    @GET("rest/register.do?userName=okinavakzn-api&password=Smart-resto2018*&returnUrl=https://smart-resto-success.ru&failUrl=https://smart-resto-error.ru")
    Observable<Response> pay(@Query("amount") int amount, @Query("orderNumber") long orderNumber);

    @POST("google/payment.do")
    Observable<ResponsePay> googlePay(@Body GoogleLayClass layClass);

    class Creator {
        public static SberApi newApi() {
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .readTimeout(15, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(SberApi.ENDPOINT)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            return retrofit.create(SberApi.class);
        }
    }

    class ResponsePay{
        @SerializedName("success")
        @Expose
        public Boolean success;
        @SerializedName("data")
        @Expose
        public ResponsePayData data;
    }

    class ResponsePayData{
        @SerializedName("orderId")
        @Expose
        public String orderId;
        @SerializedName("acsUrl")
        @Expose
        public String acsUrl;
        @SerializedName("paReq")
        @Expose
        public String paReq;

        @SerializedName("termUrl")
        @Expose
        public String termUrl;

    }

    class GoogleLayClass{
        @SerializedName("merchant")
        @Expose
        public String merchant = "okinavakzn";

        @SerializedName("orderNumber")
        @Expose
        public String orderNumber;


        @SerializedName("paymentToken")
        @Expose
        public String paymentToken;

        @SerializedName("amount")
        @Expose
        public int amount;

        @SerializedName("preAuth")
        @Expose
        public Boolean preAuth = false;

        @SerializedName("returnUrl")
        @Expose
        public String returnUrl = "https://smart-resto.ru?success=smart-resto-success";

        @SerializedName("failUrl")
        @Expose
        public String failUrl = "https://smart-resto-error.ru";

        public GoogleLayClass(){}

        public GoogleLayClass(String paymentToken, Long orderNumber, int amount) {
            this.paymentToken = paymentToken;
            this.orderNumber = orderNumber.toString();
            this.amount = amount;
        }
    }

    class Response{
        @SerializedName("orderId")
        @Expose
        private String orderId;

        @SerializedName("formUrl")
        @Expose
        private String formUrl;

        @SerializedName("errorCode")
        @Expose
        private String errorCode;

        @SerializedName("errorMessage")
        @Expose
        private String errorMessage;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getFormUrl() {
            return formUrl;
        }

        public void setFormUrl(String formUrl) {
            this.formUrl = formUrl;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }
    }
}
