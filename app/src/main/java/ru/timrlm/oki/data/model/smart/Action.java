package ru.timrlm.oki.data.model.smart;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Action {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("how_it_make")
    @Expose
    private List<HowItMake> howItMake = null;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("action_order")
    @Expose
    private String actionOrder;
    @SerializedName("image")
    @Expose
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<HowItMake> getHowItMake() {
        return howItMake;
    }

    public void setHowItMake(List<HowItMake> howItMake) {
        this.howItMake = howItMake;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getActionOrder() {
        return actionOrder;
    }

    public void setActionOrder(String actionOrder) {
        this.actionOrder = actionOrder;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public class HowItMake {

        @SerializedName("action_desc")
        @Expose
        private String actionDesc;
        @SerializedName("action_title")
        @Expose
        private String actionTitle;

        public String getActionDesc() {
            return actionDesc;
        }

        public void setActionDesc(String actionDesc) {
            this.actionDesc = actionDesc;
        }

        public String getActionTitle() {
            return actionTitle;
        }

        public void setActionTitle(String actionTitle) {
            this.actionTitle = actionTitle;
        }

    }
}
