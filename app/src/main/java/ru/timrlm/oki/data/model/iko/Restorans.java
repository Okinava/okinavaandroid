package ru.timrlm.oki.data.model.iko;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Restorans {

    @SerializedName("deliveryTerminals")
    @Expose
    private List<DeliveryTerminal> deliveryTerminals = null;

    public List<DeliveryTerminal> getDeliveryTerminals() {
        return deliveryTerminals;
    }

    public void setDeliveryTerminals(List<DeliveryTerminal> deliveryTerminals) {
        this.deliveryTerminals = deliveryTerminals;
    }

    public class DeliveryTerminal {

        @SerializedName("organizationId")
        @Expose
        private String organizationId;
        @SerializedName("deliveryRestaurantName")
        @Expose
        private String deliveryRestaurantName;
        @SerializedName("deliveryTerminalId")
        @Expose
        private String deliveryTerminalId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("openingHours")
        @Expose
        private List<OpeningHour> openingHours = null;
        @SerializedName("externalRevision")
        @Expose
        private Integer externalRevision;
        @SerializedName("technicalInformation")
        @Expose
        private Object technicalInformation;

        public String getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(String organizationId) {
            this.organizationId = organizationId;
        }

        public String getDeliveryRestaurantName() {
            return deliveryRestaurantName;
        }

        public void setDeliveryRestaurantName(String deliveryRestaurantName) {
            this.deliveryRestaurantName = deliveryRestaurantName;
        }

        public String getDeliveryTerminalId() {
            return deliveryTerminalId;
        }

        public void setDeliveryTerminalId(String deliveryTerminalId) {
            this.deliveryTerminalId = deliveryTerminalId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public List<OpeningHour> getOpeningHours() {
            return openingHours;
        }

        public void setOpeningHours(List<OpeningHour> openingHours) {
            this.openingHours = openingHours;
        }

        public Integer getExternalRevision() {
            return externalRevision;
        }

        public void setExternalRevision(Integer externalRevision) {
            this.externalRevision = externalRevision;
        }

        public Object getTechnicalInformation() {
            return technicalInformation;
        }

        public void setTechnicalInformation(Object technicalInformation) {
            this.technicalInformation = technicalInformation;
        }

    }

    public class OpeningHour {

        @SerializedName("dayOfWeek")
        @Expose
        private Integer dayOfWeek;
        @SerializedName("from")
        @Expose
        private String from;
        @SerializedName("to")
        @Expose
        private String to;
        @SerializedName("allDay")
        @Expose
        private Boolean allDay;
        @SerializedName("closed")
        @Expose
        private Boolean closed;

        public Integer getDayOfWeek() {
            return dayOfWeek;
        }

        public void setDayOfWeek(Integer dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public Boolean getAllDay() {
            return allDay;
        }

        public void setAllDay(Boolean allDay) {
            this.allDay = allDay;
        }

        public Boolean getClosed() {
            return closed;
        }

        public void setClosed(Boolean closed) {
            this.closed = closed;
        }

    }
}