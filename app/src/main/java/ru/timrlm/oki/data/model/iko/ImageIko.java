package ru.timrlm.oki.data.model.iko;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by timerlan on 09.04.2018.
 */

public class ImageIko {
    @SerializedName("imageId")
    @Expose
    private String imageId;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("uploadDate")
    @Expose
    private String uploadDate;

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

}
