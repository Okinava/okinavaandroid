package ru.timrlm.oki.data.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.timrlm.oki.data.model.iko.CheckAdress;
import ru.timrlm.oki.data.model.iko.CreateCustomer;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.iko.Customer;
import ru.timrlm.oki.data.model.iko.Foods;
import ru.timrlm.oki.data.model.iko.History;
import ru.timrlm.oki.data.model.iko.OrderResponse;
import ru.timrlm.oki.data.model.iko.Restorans;
import ru.timrlm.oki.data.model.iko.StreetResult;

public interface ApiHelper {

    String ENDPOINT = "https://iiko.biz:9900/";
    String orgId = "50322c02-141c-11e5-80d2-d8d38565926f";
    @GET("api/0/auth/access_token")
    Observable<String> getToken(@Query("user_id") String user_id, @Query("user_secret") String user_secret);

    @GET("api/0/nomenclature/"+orgId)
    Observable<Foods> getFoods(@Query("access_token") String access_token);

    @GET("api/0/customers/get_customer_by_phone?organization="+orgId)
    Observable<Customer> getCustomer(@Query("access_token") String access_token, @Query("phone") String phone);

    @POST("api/0/customers/create_or_update?organization="+orgId)
    Observable<String> createCustomer(@Query("access_token") String access_token, @Body CreateCustomer customer);

    @GET("api/0/cities/cities?organization="+orgId)
    Observable<List<StreetResult>> getAdressList(@Query("access_token") String access_token);

    @GET("api/0/deliverySettings/getDeliveryTerminals?organization="+orgId)
    Observable<Restorans> getRestorans(@Query("access_token") String access_token);

    @POST("api/0/orders/add")
    Observable<OrderResponse> addOrder(@Query("access_token") String access_token, @Body CusOrder order);

    @POST("api/0/orders/checkAddress?organizationId="+orgId)
    Observable<CheckAdress> checkAdress(@Query("access_token") String access_token, @Body CusOrder.Address adress);

    @GET("api/0/orders/deliveryHistoryByPhone?organization="+orgId)
    Observable<History> getHistory(@Query("access_token") String access_token, @Query("phone") String phone);

    @POST("api/0/orders/sendDeliveryOpinion")
    Observable<Object> sendMark(@Query("access_token") String access_token, @Body Marks order);

    @POST("api/0/orders/calculate_checkin_result")
    Observable<CheckSale> checkSale(@Query("access_token") String access_token, @Body CusOrder order);

    /******** Helper class that sets up a new services *******/
    class Creator {

        public static ApiHelper newApi() {
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .readTimeout(15, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiHelper.ENDPOINT)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            return retrofit.create(ApiHelper.class);
        }
    }

    class Marks{
        @SerializedName("organization")
        @Expose
        public String organization = orgId;
        @SerializedName("deliveryId")
        @Expose
        public String deliveryId;
        @SerializedName("comment")
        @Expose
        public String comment;
        @SerializedName("marks")
        @Expose
        public List<Mark> marks = new ArrayList<>();
    }

    class Mark{
        @SerializedName("surveyItemId")
        @Expose
        public String surveyItemId;
        @SerializedName("mark")
        @Expose
        public Integer mark;

        public Mark(String surveyItemId, Integer mark) {
            this.surveyItemId = surveyItemId;
            this.mark = mark;
        }

        public Mark(){}
    }

    class CheckSale{
        @SerializedName("loyatyResult")
        @Expose
        public LoyatyResult loyatyResult;
    }

    class LoyatyResult{
        @SerializedName("programResults")
        @Expose
        public List<ProgramResult> programResults;
    }
    class ProgramResult{
        @SerializedName("name")
        @Expose
        public String name;
    }
}
