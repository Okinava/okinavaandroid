package ru.timrlm.oki.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.timrlm.oki.data.model.iko.History;
import ru.timrlm.oki.data.model.iko.Product;
import ru.timrlm.oki.data.model.iko.Restorans;
import ru.timrlm.oki.data.model.iko.StreetResult;
import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.data.model.smart.SmartRestoran;
import ru.timrlm.oki.data.remote.SmartApi;
import ru.timrlm.oki.di.ApplicationContext;

@Singleton
public class PreferencesHelper {

    public static final String PREF_FILE_NAME = "android_boilerplate_pref_file";
    public static final String PHONE = "PHONE";
    public static final String STAR = "STAR";
    public static final String FIRST_LAUNCH = "FIRST_LAUNCH";
    public static final String MARK = "MARK";
    public static final String PROMO = "PROMO";
    private Gson gson = new Gson();

    private final SharedPreferences mPref;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void clear() {
        mPref.edit().clear().apply();
    }


    public String[] getStars(){
        String s = mPref.getString(STAR,"");
        if ( s.length() == 0 ) return new String[]{};
        return s.split(";;;");
    }

    public void addIdStar(String id){
        String s = mPref.getString(STAR,"");
        mPref.edit().putString(STAR, s + id + ";;;").apply();
    }

    public void removeIdStar(String id){
        String[] starts = getStars();
        String s = "";
        for (String start : starts){
            if (!start.equals(id)) s += start + ";;;";
        }
        mPref.edit().putString(STAR, s).apply();
    }


    public void setPhone(String value){ mPref.edit().putString(PHONE,value).apply();}
    public String getPhone(){ return  mPref.getString(PHONE,"");}

    public void setMark(Boolean value){ mPref.edit().putBoolean(MARK,value).apply();}
    public Boolean getMark(){ return  mPref.getBoolean(MARK,false);}

    public void setPromo(String value){ mPref.edit().putString(PROMO,value).apply();}
    public String getPromo(){ return  mPref.getString(PROMO,"");}

    public Boolean isFirstLaunch(){
        boolean result = mPref.getBoolean(FIRST_LAUNCH,true);
        mPref.edit().putBoolean(FIRST_LAUNCH,false).commit();
        return result;
    }

    public List<Action> getActions(){
        String json = mPref.getString("Actions","");
        if (json.length() == 0){ return new ArrayList<>(); }
        Action[] ac = gson.fromJson(json,Action[].class);
        return Arrays.asList(ac);
    }

    public void setActions(List<Action> actions) {
        mPref.edit().putString("Actions",gson.toJson(actions)).apply();
    }

    public List<StreetResult.Street> getStreets(){
        String json = mPref.getString("Streets","");
        if (json.length() == 0){ return new ArrayList<>(); }
        StreetResult.Street[] ac = gson.fromJson(json,StreetResult.Street[].class);
        return Arrays.asList(ac);
    }
    public void setStreets(List<StreetResult.Street> streets){
        mPref.edit().putString("Streets",gson.toJson(streets)).apply();
    }

    public Restorans getRestorans(){
        String json = mPref.getString("Restorans","");
        if (json.length() == 0){ return null; }
        return gson.fromJson(json,Restorans.class);
    }
    public void setRestorans(Restorans streets){
        mPref.edit().putString("Restorans",gson.toJson(streets)).apply();
    }

    public History getHistory(){
        String json = mPref.getString("History","");
        if (json.length() == 0){ return null; }
        return gson.fromJson(json,History.class);
    }
    public void setHistory(History history) {
        mPref.edit().putString("History", gson.toJson(history)).apply();
    }

    public void clearHistory(){
        mPref.edit().putString("History", "").apply();
    }

    public List<SmartRestoran> getSmartRestoran(){
        String json = mPref.getString("SmartRestoran","");
        if (json.length() == 0){ return new ArrayList<>(); }
        SmartRestoran[] ac = gson.fromJson(json,SmartRestoran[].class);
        return Arrays.asList(ac);
    }
    public void setSmartRestoran(List<SmartRestoran> history) {
        mPref.edit().putString("SmartRestoran", gson.toJson(history)).apply();
    }

    public List<Product> getSales(){
        String json = mPref.getString("Product","");
        if (json.length() == 0){ return new ArrayList<>(); }
        Product[] ac = gson.fromJson(json,Product[].class);
        return Arrays.asList(ac);
    }

    public void setSales(List<Product> actions) {
        mPref.edit().putString("Product",gson.toJson(actions)).apply();
    }

    public SmartApi.SmartPushs getPushs(){
        String json = mPref.getString("SmartPushs","");
        if (json.length() == 0){ return null; }
        return gson.fromJson(json,SmartApi.SmartPushs.class);
    }

    public void setPushs(SmartApi.SmartPushs actions) {
        mPref.edit().putString("SmartPushs",gson.toJson(actions)).apply();
    }
}
