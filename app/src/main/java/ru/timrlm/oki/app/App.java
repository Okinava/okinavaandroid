package ru.timrlm.oki.app;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.instacart.library.truetime.TrueTimeRx;

import io.fabric.sdk.android.Fabric;
import io.reactivex.schedulers.Schedulers;
import ru.timrlm.oki.di.component.ApplicationComponent;

import ru.timrlm.oki.di.component.DaggerApplicationComponent;
import ru.timrlm.oki.di.module.ApplicationModule;

import static io.fabric.sdk.android.Fabric.TAG;


public class App extends Application {

    ApplicationComponent mApplicationComponent;

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        TrueTimeRx.build()
                .initializeRx("time.google.com")
                .subscribeOn(Schedulers.io())
                .subscribe(date -> {
                    Log.v("qwe", "TrueTime was initialized and we have a time: " + date);
                }, throwable -> {
                    throwable.printStackTrace();
                });
        Fabric.with(this, new Crashlytics());
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }
}
