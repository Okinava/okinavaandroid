package ru.timrlm.oki.ui.sale.salelist;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.ui.product.pager.MyFragmentPagerAdapter;
import ru.timrlm.oki.util.ViewUtil;

import static ru.timrlm.oki.ui.product.ProductActivity.product;

public class SaleListActivity extends BaseSwipeBackActivity implements SaleListMVPView {
    public static Action action;
    @Inject SaleListPresenter mPresenter;
    @BindView(R.id.pager) ViewPager pv;
    List<Action> actions;

    @Override
    public void setActions(List<Action> actions, int pos) {
        this.actions = actions;
        pv.setAdapter(new SaleFragmentPA(getSupportFragmentManager(),actions));
        pv.setCurrentItem(pos);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        mPresenter.load(action);
    }

    @OnClick(R.id.top_back)
    void back(){super.onBackPressed();}

    @OnClick(R.id.top_share)
    void share() {
        Action a = actions.get(pv.getCurrentItem());
        ViewUtil.shareText(SaleListActivity.this,a.getTitle(),a.getDescription());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public int setlayout() { return R.layout.activity_salelist; }
}
