package ru.timrlm.oki.ui.prof.set;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.timrlm.oki.R;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.util.ViewUtil;

import static butterknife.OnTextChanged.Callback.AFTER_TEXT_CHANGED;

public class SetProfActivity extends BaseSwipeBackActivity implements SerProfMvpView, DatePickerDialog.OnDateSetListener {
    @Inject SetProfPresenter mPresenter;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.phone) TextView phone;
    @BindView(R.id.gender) Spinner gender;
    @BindView(R.id.date) Button date;
    @BindView(R.id.save) Button save;
    Calendar dateAndTime = Calendar.getInstance();
    String setDate = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        mPresenter.load();
    }

    @Override
    public void setUser(String name, String phone, int sex, String date) {
        this.name.setText(name);
        this.phone.setText(phone);
        this.gender.setSelection(sex);
        this.date.setText(date);
        if (!date.equals("Дата рождения не указана")){  this.date.setEnabled(false); }
        Observable.timer(200, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(t->{
            ViewUtil.hideKeyboard(SetProfActivity.this);
        });
    }

    @OnClick(R.id.date)
    void date(){
        if (date.isEnabled()) {
            dateAndTime.add(Calendar.YEAR, -10);
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    this,
                    dateAndTime.get(Calendar.YEAR),
                    dateAndTime.get(Calendar.MONTH),
                    dateAndTime.get(Calendar.DAY_OF_MONTH)// Inital day selection
            );
            dpd.setMaxDate(dateAndTime);
            dpd.show(getFragmentManager(), "Datepickerdialog");
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {
        setDate = year + "-" + (month > 9 ? String.valueOf(month) : "0" + month ) + "-" + (dayOfMonth > 9 ? String.valueOf(dayOfMonth) : "0" + dayOfMonth );
        date.setText( (dayOfMonth > 9 ? String.valueOf(dayOfMonth) : "0" + dayOfMonth )+ "." + (month > 9 ? String.valueOf(month) : "0" + month )+ "." + year);
    }

    @OnClick(R.id.save)
    void save(){
        showProgres();
        mPresenter.chage(name.getText().toString(),gender.getSelectedItemPosition(), setDate);
    }

    @OnClick(R.id.exit)
    void exit(){ mPresenter.exit(); }

    @Override
    public void fin() {
        try{ hideProgres(); }catch (Exception e){}
        finish();
    }

    @OnTextChanged(value = R.id.name, callback = AFTER_TEXT_CHANGED)
    public void afterTextChanged(Editable s) {
        if (s.length()>0 ){
            save.setEnabled(true);
            save.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }else{
            save.setEnabled(false);
            save.setBackgroundColor(getResources().getColor(R.color.lightgray));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public int setlayout() { return R.layout.activity_setprof; }
}
