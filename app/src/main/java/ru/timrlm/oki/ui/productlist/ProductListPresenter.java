package ru.timrlm.oki.ui.productlist;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.local.RealmHelper;
import ru.timrlm.oki.data.model.iko.Product;
import ru.timrlm.oki.data.model.realm.lGroup;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.ui.foodgroup.FoodGroupPresenter;
import ru.timrlm.oki.ui.main.MainMvpView;
import ru.timrlm.oki.util.RxEventBus;
import ru.timrlm.oki.util.ViewUtil;

/**
 * Created by timerlan on 28.03.2018.
 */

class ProductListPresenter extends BasePresenter<ProductListMvpView> {
    private final DataManager mDataManager;
    private final RxEventBus mRxEventBus;
    private lGroup group;

    @Inject
    public ProductListPresenter(DataManager dataManager) {
        mDataManager = dataManager;
        mRxEventBus = dataManager.getRxEventBus();
    }

    @Override
    public void attachView(ProductListMvpView mvpView) {
        super.attachView(mvpView);
        mRxEventBus.filteredObservable(DataManager.StarUpd.class).subscribe(starUpd -> {
            loadProduct(this.group);
        },Throwable::printStackTrace);
        mRxEventBus.filteredObservable(FoodGroupPresenter.needScroll.class).subscribe(r->getMvpView().scrollTOp(),Throwable::printStackTrace);
        mRxEventBus.filteredObservable(lProduct.class)
                .subscribe(r->getMvpView().reload()
                        ,Throwable::printStackTrace);

    }

    @Override
    public void detachView() { super.detachView(); }

    public void loadProduct(lGroup group){
        this.group = group;
        if (group != null) {
            if (mDataManager.isGroupHavePodGroup(group.getId())) {
                getMvpView().setProducts(mDataManager.getProductsWhithPodGroups(group.getId()), mDataManager.getOrders());
            } else {
                getMvpView().setProducts(mDataManager.getProducts(group.getId()), mDataManager.getOrders());
            }
        }
    }

    public boolean isStar(lProduct product){
        return mDataManager.isStart(product.getId());
    }

    public void changeStart(lProduct product, boolean isStar){
        if (!isStar) mDataManager.addIdStar(product.getId());
        else mDataManager.removeIdStar(product.getId());
    }

    public void minZak(lProduct product) {
        mDataManager.minusZak(1,product,"",0);
    }

    public List<lOrder> getOrders() {
        return mDataManager.getOrders();
    }

    public void modif(lProduct product) {
        List<String> guids = Arrays.asList(product.getModifaers().split(";;;"));
        List<lProduct> products = mDataManager.getProducts(guids);
        getMvpView().modDoalog(products, product);
    }

    public void addZak(lProduct product, lProduct m) {
        if (product.getParentGroup().equals(DataManager.SALES_ID)){
            mDataManager.checkProduct().subscribe(s -> {
                if (s.equals("")) {
                    mDataManager.addZak(1,product,m.getId(),m.getName(),0);
                    getMvpView().resul(product.getName() + " ("+ m.getName() + ") добавлен в корзину");
                }else{
                    getMvpView().errors(s);
                }
            });
            return;
        }
        mDataManager.addZak(1,product,m.getId(),m.getName(),0);
        getMvpView().resul(product.getName() + " ("+ m.getName() + ") добавлен в корзину");
    }

    public void addZak(lProduct product){
        if (product.getParentGroup().equals(DataManager.SALES_ID)){
            mDataManager.checkProduct().subscribe(s -> {
                if (s.equals("")) {
                    mDataManager.addZak(1,product,"","",0);
                    getMvpView().resul(product.getName() + " добавлен в корзину");
                }else{
                    getMvpView().errors(s);
                }
            },t->{});
            return;
        }
        mDataManager.addZak(1,product,"","",0);
        if ( getMvpView() != null) getMvpView().resul(product.getName() + " добавлен в корзину");
    }
}
