package ru.timrlm.oki.ui.devilary.pay;

import java.util.List;

import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.ui.base.MvpView;

interface PayMvpView extends MvpView {
    void setInfo(lCustomer customer, int sum, List<lOrder> orders);
    void setAdres(String adres);
    void error(String text);
    void end();
    void openSber(CusOrder order);

    void c_ok(String r);
    void c_null();

    void sec_3d(String acsUrl, String paReq, String termUrl, String orderId);
}
