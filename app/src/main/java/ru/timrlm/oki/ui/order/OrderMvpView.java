package ru.timrlm.oki.ui.order;

import java.util.List;

import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.ui.base.MvpView;

interface OrderMvpView extends MvpView {
    void setOrders(List<lOrder> orders);
    void setSum(int sum);
    void setPromo(String pr);

    void start();

    void haveSales();
}
