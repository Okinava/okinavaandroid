package ru.timrlm.oki.ui.product.fragment;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.inject.Inject;

import io.reactivex.Observable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.util.ViewUtil;

@ConfigPersistent
class ProductFragmentPresenter extends BasePresenter<ProductFragmentMvpView> {
    private final DataManager mDataManager;

    @Inject
    public ProductFragmentPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    public void addZak(int count, lProduct lProduct, String guid, String nameM, int priceM, ProductFragmentMvpView view){
        if (lProduct.getParentGroup().equals(DataManager.SALES_ID)){
            mDataManager.checkProduct().subscribe(s -> {
                if (s.equals("")) {
                    mDataManager.addZak(count,lProduct,guid,nameM,priceM);
                    view.resul();
                    view.clear();
                }else{
                    view.errors(s);
                }
            });
            return;
        }
        mDataManager.addZak(count,lProduct,guid,nameM,priceM);
        view.resul();
        view.clear();
    }

    public void openOrder(){
        mDataManager.getRxEventBus().post(new ProductFragment());
    }

    public void setModifaers(List<String> guids) {
        List<lProduct> products = mDataManager.getProducts(guids);
        Observable.fromArray(products).flatMapIterable(l->l)
                .map(p->p.getName() + "  +  " + p.getPrice() + " " + ViewUtil.R)
                .toList()
                .subscribe(l->getMvpView().setModifaers(l,products));
    }

    public Boolean isStar(String id){
        return mDataManager.isStart(id);
    }

    public void chageStar(String id){
        if (mDataManager.isStart(id)){ mDataManager.removeIdStar(id);
        }else{ mDataManager.addIdStar(id); }
    }
}
