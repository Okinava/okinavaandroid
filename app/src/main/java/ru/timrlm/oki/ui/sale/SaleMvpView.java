package ru.timrlm.oki.ui.sale;

import java.util.List;

import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.ui.base.MvpView;

interface SaleMvpView extends MvpView {
    void setActions(List<Action> actions);
}
