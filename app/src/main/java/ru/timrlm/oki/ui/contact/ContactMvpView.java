package ru.timrlm.oki.ui.contact;

import java.util.List;

import ru.timrlm.oki.data.model.iko.Restorans;
import ru.timrlm.oki.data.model.smart.SmartRestoran;
import ru.timrlm.oki.ui.base.MvpView;

interface ContactMvpView extends MvpView {
    void setRest(List<SmartRestoran> terminals);
    void showRest(List<SmartRestoran> terminals);
    void error();
}
