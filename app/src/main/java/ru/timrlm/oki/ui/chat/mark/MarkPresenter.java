package ru.timrlm.oki.ui.chat.mark;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.util.RxUtil;

@ConfigPersistent
class MarkPresenter extends BasePresenter<MarkMvpView> {
    private DataManager mDataManager;
    private Disposable disposable;

    @Inject
    public MarkPresenter(DataManager dataManager) {
        this.mDataManager = dataManager;
    }

    public void send(String id, Float f, Float s, Float t, String comment) {
        disposable = mDataManager.sendMarks(id, f.intValue(), s.intValue(), t.intValue(), comment)
                .subscribe(b->{
                    if (b) { getMvpView().result(); } else { getMvpView().error(); }
                });
    }

    @Override
    public void detachView() {
        RxUtil.dispose(disposable);
        super.detachView();
    }
}
