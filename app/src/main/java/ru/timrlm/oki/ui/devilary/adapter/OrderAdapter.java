package ru.timrlm.oki.ui.devilary.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.util.ViewUtil;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderView> {
    private List<lOrder> mOrders = new ArrayList<>();
    private Context mContext;
    private OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void minus(lOrder product, int position) { }
        @Override
        public void plus(lOrder product, int position) { }
        @Override
        public void delete(lOrder order, int position) { }
    };

    public void setOrders(List<lOrder> orders){
        mOrders = orders;
        notifyDataSetChanged();
    }

    public List<lOrder> getOrders() { return mOrders; }

    public void setOnClickListener(OnClickListener onClickListener) { this.onClickListener = onClickListener; }

    @Override
    public int getItemCount() { return mOrders.size(); }

    @NonNull
    @Override
    public OrderView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new OrderView(LayoutInflater.from(mContext).inflate(R.layout.rv_order, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull OrderView holder, int position) {
        holder.name.setText(mOrders.get(position).getlProduct().getName());
        holder.count.setText("x"+mOrders.get(position).getCount());
        holder.dop.setText(mOrders.get(position).getNameM());
        if (mOrders.get(position).getlProduct().getParentGroup().equals(DataManager.SALES_ID)){
            holder.dop.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
            holder.dop.setText("Акция");
        }else{
            holder.dop.setTextColor(ContextCompat.getColor(mContext,R.color.lightgray));
            holder.dop.setText(mOrders.get(position).getNameM());
        }
        if (mOrders.get(position).getlProduct().getWeight() >= 1 ){
            holder.weight.setText(mContext.getString(R.string.weight,mOrders.get(position).getlProduct().getWeight()));
        }else{
            Double v = mOrders.get(position).getlProduct().getWeight() * 1000;
            holder.weight.setText( v.intValue() + " гр.");
        }
        holder.price.setText(mOrders.get(position).getSum()+ " " + ViewUtil.R);
        Glide.with(mContext)
                .load(mOrders.get(position).getlProduct().getImg())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_refresh)
                        .error(R.drawable.ic_photo)
                )
                .into(holder.img);
    }

    class OrderView extends RecyclerView.ViewHolder{
        @Nullable @BindView(R.id.img) ImageView img;
        @Nullable @BindView(R.id.name) TextView name;
        @Nullable @BindView(R.id.weight) TextView weight;
        @Nullable @BindView(R.id.count) TextView count;
        @Nullable @BindView(R.id.dop) TextView dop;
        @Nullable @BindView(R.id.price) TextView price;
        @Nullable @BindView(R.id.btn) View btn;

        public OrderView(View view){
            super(view);
            ButterKnife.bind(this,view);
            view.setOnLongClickListener((v)->{
                MaterialSimpleListAdapter adapter = new MaterialSimpleListAdapter((d, index, item)->{
                    d.dismiss();
                    onClickListener.delete(mOrders.get(getAdapterPosition()),getAdapterPosition());
                });
                adapter.add(new MaterialSimpleListItem.Builder(mContext).content("Удалить из корзины").build());
                new MaterialDialog.Builder(mContext).adapter(adapter,null).show();
                return false;
            });
        }

        @OnClick(R.id.minus)
        synchronized void minus(){
            try{ onClickListener.minus(mOrders.get(getAdapterPosition()),getAdapterPosition()); } catch (Exception e){}}

        @OnClick(R.id.plus)
        synchronized void plus(){ try{ onClickListener.plus(mOrders.get(getAdapterPosition()),getAdapterPosition());} catch (Exception e){} }
    }

    public interface OnClickListener {
        void minus(lOrder order, int position);
        void plus(lOrder order, int position);
        void delete(lOrder order, int position);
    }
}
