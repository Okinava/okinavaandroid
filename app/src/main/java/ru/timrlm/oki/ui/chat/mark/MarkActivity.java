package ru.timrlm.oki.ui.chat.mark;

import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.remote.SmartApi;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.util.ViewUtil;

public class MarkActivity extends BaseSwipeBackActivity implements MarkMvpView {
    @Inject MarkPresenter mPresenter;
    @BindView(R.id.one) RatingBar one;
    @BindView(R.id.two) RatingBar two;
    @BindView(R.id.three) RatingBar three;
    @BindView(R.id.comment) TextView comment;
    public static String id;

    @OnClick(R.id.send)
    void send(){
        showProgres();
        mPresenter.send(id, one.getRating(), two.getRating(), three.getRating(), comment.getText().toString());
    }

    @Override
    public void result() {
        hideProgres();
        finish();
    }

    @Override
    public void error() {
        hideProgres();
        super.showError("Не удалось оставить отзыв");
    }

    @Override
    public int setlayout() {
        return R.layout.activity_mark;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @OnClick({R.id.back_card,R.id.back_top})
    void chat(){ finish(); }
}
