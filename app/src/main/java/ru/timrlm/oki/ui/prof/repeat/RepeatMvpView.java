package ru.timrlm.oki.ui.prof.repeat;

import ru.timrlm.oki.ui.base.MvpView;

interface RepeatMvpView extends MvpView {

    void setTime(String t);

    void ok();
    void ok(String text);
    void er(String text);

}
