package ru.timrlm.oki.ui.sale;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.smart.Action;

public class SaleAdapter extends RecyclerView.Adapter<SaleAdapter.SaleView>{
    private List<Action> actions;
    private Context mContext;
    private OnSaleDOIT onSaleDOIT = new OnSaleDOIT() {@Override public void read(Action a) { }@Override public void share(Action a) { }};

    public SaleAdapter(List<Action> actions,OnSaleDOIT onSaleDOIT) {
        this.actions = actions;
        this.onSaleDOIT = onSaleDOIT;
    }

    @NonNull
    @Override
    public SaleView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new SaleView(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_sale,parent,false)); }

    @Override
    public void onBindViewHolder(@NonNull SaleView holder, int position) {
        Glide.with(mContext)
                .load(actions.get(position).getImage())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_refresh)
                        .error(R.drawable.ic_photo)
                )
                .into(holder.img);
        holder.name.setText(actions.get(position).getTitle());
        holder.desc.setText(actions.get(position).getDescription());
    }

    @Override
    public int getItemCount() { return actions.size(); }

    class SaleView extends RecyclerView.ViewHolder{
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.desc) TextView desc;

        SaleView(View view){
            super(view);
            ButterKnife.bind(this,view);
            view.setOnClickListener((v)-> onSaleDOIT.read(actions.get(getAdapterPosition())));
        }

        @OnClick(R.id.share)
        void share(){ onSaleDOIT.share(actions.get(getAdapterPosition())); }

        @OnClick(R.id.read)
        void read(){ onSaleDOIT.read(actions.get(getAdapterPosition())); }
    }

    interface OnSaleDOIT{
        void read(Action a);
        void share(Action a);
    }
}
