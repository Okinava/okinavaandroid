package ru.timrlm.oki.ui.productlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.realm.lGroup;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.ui.product.ProductActivity;
import ru.timrlm.oki.ui.productlist.adapter.ProductAdapter;
import ru.timrlm.oki.util.ViewUtil;

/**
 * Created by timerlan on 28.03.2018.
 */

public class ProductListFragment extends Fragment implements ProductListMvpView, ProductAdapter.OnClickListener  {
    @Inject ProductListPresenter mPresenter;
    @BindView(R.id.rv) RecyclerView rv;
    private lGroup group;
    private View rootView;
    private ProductAdapter mProductAdapter = new ProductAdapter();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.rv,container,false);
        ButterKnife.bind(this,rootView);
        ((MainActivity) getActivity()).activityComponent().inject(this);
        mPresenter.attachView(this);
        mPresenter.loadProduct(group);
        return rootView;
    }

    @Override
    public void setProducts(List<lProduct> products, List<lOrder> orders) {
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setAdapter(mProductAdapter);
        mProductAdapter.setOnClickListener(this);
        mProductAdapter.setProduct(products, orders);
    }

    @Override
    public void onClick(lProduct product) {
        ProductActivity.product = product;
        ProductActivity.products = null;
        startActivity(new Intent(getActivity(),ProductActivity.class));
    }

    @Override
    public void reload() {
        mProductAdapter.setOrders(mPresenter.getOrders());
        mProductAdapter.notifyDataSetChanged();
    }

    @Override
    public void by(lProduct product) {
        ViewUtil.vibrate(rootView.getContext());
        if (product.getModifaers().length()>0) {
            mPresenter.modif(product);
            return;
        }
        mPresenter.addZak(product);
        mProductAdapter.setOrders(mPresenter.getOrders());
    }

    @Override
    public void resul(String s) {
        Snackbar.make(rootView,s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void errors(String s) {
        new MaterialDialog.Builder(getActivity())
                .title("Ой!")
                .content(s)
                .positiveText("Хорошо")
                .show();
    }

    @Override
    public void modDoalog(List<lProduct> products, lProduct product) {
        List<String> names = new ArrayList<>();
        for (lProduct p : products) names.add(p.getName());
        new MaterialDialog.Builder(getContext())
                .title("Выберите модификатор для " + product.getName())
                .items(names)
                .itemsCallbackSingleChoice(-1,(dialog,v,i,t)->{
                    mPresenter.addZak(product,products.get(i));
                    return true;
                }).show();
    }

    @Override
    public void minus(lProduct product) {
        ViewUtil.vibrate(rootView.getContext());
        Snackbar.make(rootView,product.getName() + " удален из корзины", Snackbar.LENGTH_LONG).show();
        mPresenter.minZak(product);
        mProductAdapter.setOrders(mPresenter.getOrders());
    }

    @Override
    public void scrollTOp() { try{rv.scrollToPosition(0);}catch (Exception e){}}

    @Override
    public void star(lProduct product) {
        boolean isStar = mPresenter.isStar(product);
        MaterialSimpleListAdapter adapter = new MaterialSimpleListAdapter((d, index, item)->{
            ViewUtil.vibrate(getActivity());
            d.dismiss();
            mPresenter.changeStart(product,isStar);
            Snackbar.make(rootView,product.getName() + (isStar ? " удален из избранного" : " добавлен в избранное"), Snackbar.LENGTH_LONG).show();
        });
        adapter.add(new MaterialSimpleListItem.Builder(getActivity()).content(isStar ? "Удалить из избранного" : "Добвавить в избаннное").build());
        new MaterialDialog.Builder(getActivity()).adapter(adapter,null).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.detachView();
    }

    public static ProductListFragment newInstance(lGroup group) {
        ProductListFragment fragment = new ProductListFragment();
        fragment.group = group;
        return fragment;
    }
}
