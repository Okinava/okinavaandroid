package ru.timrlm.oki.ui.prof.set;

import javax.inject.Inject;

import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;

@ConfigPersistent
class SetProfPresenter extends BasePresenter<SerProfMvpView> {
    private DataManager mDataManager;

    @Inject
    public SetProfPresenter(DataManager mDataManager) { this.mDataManager = mDataManager; }

    public void load(){
        lCustomer user = mDataManager.findUser(mDataManager.getPreferencesHelper().getPhone());
        getMvpView().setUser(user.getName(),user.getPhone(),user.getSex(),user.getBirthday());
    }

    public void chage(String name, int sex, String birth ){
        mDataManager.updUser(mDataManager.getPreferencesHelper().getPhone(),name,sex,birth)
                .subscribe(l-> getMvpView().fin(),throwable -> {
                    if (getMvpView() != null){ getMvpView().fin(); }
                } );
    }

    public void exit(){
        mDataManager.clearHistory();
        mDataManager.getPreferencesHelper().setPhone("");
        mDataManager.getRxEventBus().post(new lCustomer());
        getMvpView().fin();
    }
}
