package ru.timrlm.oki.ui.foodgroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.Promo;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.ui.main.MainMvpView;

/**
 * Created by timerlan on 28.03.2018.
 */

public class FoodGroupPresenter extends BasePresenter<FoodGroupMvpView> {
    private DataManager mDataManager;

    @Inject
    public FoodGroupPresenter(DataManager dataManager){
        mDataManager = dataManager;

    }

    @Override
    public void attachView(FoodGroupMvpView mvpView) {
        super.attachView(mvpView);
        mDataManager.getRxEventBus().filteredObservable(lCustomer.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(c-> setBonus(c.getBalance()),Throwable::printStackTrace);
        mDataManager.getRxEventBus().filteredObservable(DataManager.StarUpd.class)
                .subscribe(starUpd -> {
                    if (starUpd.needList) getMvpView().reinit();
                    },Throwable::printStackTrace);
    }

    @Override
    public void detachView() { super.detachView(); }

    public void load(){
        getMvpView().setPromo(mDataManager.getPreferencesHelper().getPromo());
        getMvpView().setGroups(mDataManager.getGroups());
        lCustomer user = mDataManager.findUser(mDataManager.getPreferencesHelper().getPhone());
        if (user != null) setBonus(user.getBalance() == null ? 0 : user.getBalance());
    }

    private void setBonus(int bonus){
        if (getMvpView() != null)
        switch (bonus){
            case 1: getMvpView().setBonus(bonus,"бонус");
            case 2-4: getMvpView().setBonus(bonus,"бонуса");
            default: getMvpView().setBonus(bonus,"бонусов");
        }
    }

    void needScroll(){ mDataManager.getRxEventBus().post(new needScroll()); }

    public void sendUISearch(boolean b) {
        mDataManager.getRxEventBus().post(new UISearch(b));
    }

    public void setPromo(String s) {
        mDataManager.getPreferencesHelper().setPromo(s);
    }

    public static class UISearch{
        public boolean need;
        public UISearch(boolean need) { this.need = need; }
    }

    public static class needScroll{ public needScroll() { }}
}
