package ru.timrlm.oki.ui.contact.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.iko.Restorans;
import ru.timrlm.oki.data.model.smart.SmartRestoran;
import ru.timrlm.oki.util.ViewUtil;

public class RestoranAdapter extends RecyclerView.Adapter<RestoranAdapter.RestoranView>{
    private List<SmartRestoran> deliveryTerminals;
    private Context mContext;

    public RestoranAdapter(List<SmartRestoran> deliveryTerminals) { this.deliveryTerminals = deliveryTerminals; }

    @NonNull
    @Override
    public RestoranView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new RestoranView(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_rest,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull RestoranView holder, int position) {
        holder.name.setText(deliveryTerminals.get(position).getAddress());
        Glide.with(mContext)
                .load(deliveryTerminals.get(position).getImage())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_refresh)
                        .error(R.drawable.ic_photo)
                )
                .into(holder.img);
    }

    @Override
    public int getItemCount() { return deliveryTerminals.size(); }

    class RestoranView extends RecyclerView.ViewHolder{
        @BindView(R.id.name) TextView name;
        @BindView(R.id.img) ImageView img;

        public RestoranView(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
