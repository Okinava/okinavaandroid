package ru.timrlm.oki.ui.contact;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.util.RxUtil;

@ConfigPersistent
class ContactPresenter extends BasePresenter<ContactMvpView> {
    private DataManager mDataManager;

    @Inject
    public ContactPresenter(DataManager mDataManager) { this.mDataManager = mDataManager; }

    private Disposable disposable;
    public void load() {
        disposable = mDataManager.getSmartRestorans().subscribe(r-> getMvpView().setRest(r), t-> load());
    }

    private Disposable disposableT;
    public void getRest(){
        disposableT = mDataManager.getSmartRestorans().subscribe(r-> getMvpView().showRest(r),
                t-> getMvpView().error());
    }

    @Override
    public void detachView() {
        RxUtil.dispose(disposable);
        RxUtil.dispose(disposableT);
        super.detachView();
    }
}
