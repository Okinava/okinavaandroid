package ru.timrlm.oki.ui.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import me.imid.swipebacklayout.lib.app.SwipeBackActivity;
import ru.timrlm.oki.R;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.ui.chat.adapter.ChatAdapter;
import ru.timrlm.oki.ui.chat.mark.MarkActivity;

public class ChatActivity extends BaseSwipeBackActivity implements ChatMvpView, ChatAdapter.OnRatingBarChangeListener {
    @Inject ChatPresenter mPresenter;
    @BindView(R.id.rv) RecyclerView rv;
    private ChatAdapter mChatAdapter = new ChatAdapter();

    void init(){
        rv.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
        rv.setAdapter(mChatAdapter);
        mChatAdapter.setOnRatingBarChangeListener(this);
        mPresenter.load();
    }

    @Override
    public void noResult() {

    }

    @Override
    public void setR(List<ChatAdapter.ChatItem> r) {
        mChatAdapter.setCi(r);
        if (r.size()>0) rv.scrollToPosition(r.size()-1);
    }

    @Override
    public void ratingBarChange(String id) {
        MarkActivity.id = id;
        startActivityForResult(new Intent(ChatActivity. this,MarkActivity.class), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.reload();
    }

    @Override
    public void setReloaded(List<ChatAdapter.ChatItem> r) {
        mChatAdapter.setCi(r);
    }

    @Override
    public int setlayout() { return R.layout.activity_chat; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @OnClick({R.id.back_card,R.id.back_top})
    void chat(){ finish(); }
}
