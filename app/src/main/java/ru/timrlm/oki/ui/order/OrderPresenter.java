package ru.timrlm.oki.ui.order;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.local.RealmHelper;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;

@ConfigPersistent
class OrderPresenter extends BasePresenter<OrderMvpView> {
    private final DataManager mDataManager;

    @Inject
    public OrderPresenter(DataManager mDataManager) { this.mDataManager = mDataManager; }

    public void load(){
        for ( lOrder o : mDataManager.getOrders()){
            if (o.getlProduct().getDeleted()) mDataManager.deleteZak(o.getlProduct(),o.getModificator());
        }
        getMvpView().setOrders(mDataManager.getOrders());
        getMvpView().setSum(mDataManager.getOrderInfo().sum);
        mDataManager.getRxEventBus().filteredObservable(RealmHelper.OrderInfo.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o->getMvpView().setSum(o.sum),Throwable::printStackTrace);
        if (mDataManager.getPreferencesHelper().getPromo().length() > 0) {
            getMvpView().setPromo(mDataManager.getPreferencesHelper().getPromo());
        }
    }

    public void plus(lOrder order){
        mDataManager.addZak(1,order.getlProduct(),order.getModificator(), order.getNameM(),(int) order.getPriceM());
    }

    public void minus(lOrder order){
        mDataManager.minusZak(1,order.getlProduct(),order.getModificator(),(int) order.getPriceM());
    }

    public void delete(lOrder order){
        mDataManager.deleteZak(order.getlProduct(),order.getModificator());
    }

    public boolean isAuto(){
        return mDataManager.findUser(mDataManager.getPreferencesHelper().getPhone()) != null;
    }

    public void check(){
        for ( lOrder o : mDataManager.getOrders()){
            if (o.getlProduct().getParentGroup().equals(DataManager.SALES_ID)) {
                mDataManager.checkProduct().subscribe(s -> {
                    if (s.equals("")) {
                        getMvpView().start();
                    }else{
                        getMvpView().haveSales();
                    }
                });
                return;
            }
        }
        getMvpView().start();
    }
}
