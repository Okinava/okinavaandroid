package ru.timrlm.oki.ui.sale;

import javax.inject.Inject;

import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;

@ConfigPersistent
class SalePresenter extends BasePresenter<SaleMvpView> {
    private DataManager mDataManager;

    @Inject
    public SalePresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    public void load() {
        mDataManager.getActions().subscribe(a->getMvpView().setActions(a), t -> {
            if (getMvpView() != null) load();
        });
    }
}
