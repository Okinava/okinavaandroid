package ru.timrlm.oki.ui.chat;

import java.util.List;

import ru.timrlm.oki.ui.base.MvpView;
import ru.timrlm.oki.ui.chat.adapter.ChatAdapter;

interface ChatMvpView extends MvpView {
    void noResult();

    void setR(List<ChatAdapter.ChatItem> r);

    void setReloaded(List<ChatAdapter.ChatItem> r);
}
