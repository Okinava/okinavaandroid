package ru.timrlm.oki.ui.devilary.newadress;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.iko.StreetResult;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.ui.order.search.SearchActivity;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.ui.prof.set.SetProfActivity;
import ru.timrlm.oki.util.ViewUtil;

import static butterknife.OnTextChanged.Callback.AFTER_TEXT_CHANGED;

public class NewAdressActivity extends BaseSwipeBackActivity implements NewAdressMvpView {
    @Inject NewAdressPresenter mPresenter;
    @BindView(R.id.save) Button save;
    @BindView(R.id.street) EditText street;
    @BindView(R.id.home) EditText home;
    @BindView(R.id.entrance) EditText entrance;
    @BindView(R.id.apartment) EditText apartment;
    @BindView(R.id.floor) EditText floor;
    @BindView(R.id.doorphone) EditText doorphone;
    @BindView(R.id.comment) EditText comment;

    private void init() {
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        Observable.timer(200, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(t->{
            ViewUtil.hideKeyboard(NewAdressActivity.this);
        });
    }

    @OnClick(R.id.save)
    void save(){
        showProgres();
        mPresenter.save(street.getText().toString(),home.getText().toString(),entrance.getText().toString(),apartment.getText().toString(),
                floor.getText().toString(),doorphone.getText().toString(),comment.getText().toString());
    }

    @Override
    public void success(int pos) {
        hideProgres();
        Intent intent = new Intent();
        intent.putExtra("pos",pos);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void error() {
        hideProgres();
        findViewById(R.id.focus).findFocus();
        showError("Ой! Мы пока не осуществляем доставку по данному адресу");
    }

    @Override
    public void error(String mes) {
        hideProgres();
        findViewById(R.id.focus).findFocus();
        showError(mes);
    }

    @Override
    public void showStreets(List<String> streets) {
        hideProgres();
        SearchActivity.list = streets;
        startActivityForResult(new Intent(NewAdressActivity.this,SearchActivity.class),2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        String name = data.getStringExtra("text");
        street.setText(name);
    }

    @OnClick(R.id.street)
    void street() {
        showProgres();
        mPresenter.getStreets();
    }

    @OnTextChanged(value = R.id.home, callback = AFTER_TEXT_CHANGED)
    public void afterTextChanged(Editable s) {
        if (s.length()>0 && street.length()>0){
            save.setEnabled(true);
            save.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }else{
            save.setEnabled(false);
            save.setBackgroundColor(getResources().getColor(R.color.lightgray));
        }
    }

    @Override
    public int setlayout() { return R.layout.activity_newadress; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}

