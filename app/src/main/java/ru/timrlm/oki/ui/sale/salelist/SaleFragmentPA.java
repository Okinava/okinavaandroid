package ru.timrlm.oki.ui.sale.salelist;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.ui.product.fragment.ProductFragment;
import ru.timrlm.oki.ui.sale.salelistitem.SaleListItemFragmet;

public class SaleFragmentPA extends FragmentPagerAdapter {

   private List<Action> actions;

    public SaleFragmentPA(FragmentManager fm, List<Action> actions) {
        super(fm);
        this.actions = actions;
    }

    @Override
    public Fragment getItem(int position) { return SaleListItemFragmet.newInstance(actions.get(position)); }

    @Override
    public int getCount() { return actions.size(); }

}

