package ru.timrlm.oki.ui.devilary;

import java.util.List;

import ru.timrlm.oki.data.model.realm.lAdres;
import ru.timrlm.oki.ui.base.MvpView;

interface DevilaryMvpView extends MvpView {
    void setAdres(List<String> adres);
    void setRestorans(List<String> restorans);
    void setUser(String name, String phone, int sum);
}
