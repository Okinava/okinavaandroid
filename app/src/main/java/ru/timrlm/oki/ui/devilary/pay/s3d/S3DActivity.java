package ru.timrlm.oki.ui.devilary.pay.s3d;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.timrlm.oki.R;

public class S3DActivity extends AppCompatActivity {
    public static String acsUrl, paReq, termUrl,orderId;
    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.web) WebView mWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);
        mTitle.setText("Оплата картой онлайн");
        WebViewClient client = new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.d("TIMTIMT",url);
//
//                if (url.contains("smart-resto-success")){
////                    showProgres();
//                    mWeb.setVisibility(View.INVISIBLE);
////                    mPresenter.add(cusOrder);
//                } else if (url.contains("smart-resto-error")){
//                    mWeb.setVisibility(View.INVISIBLE);
////                    error();
//                } else{
//                    super.onPageStarted(view, url, favicon);
//                }
            }
        };
        mWeb.setWebViewClient(client);
        mWeb.getSettings().setJavaScriptEnabled(true);
        try {
            String postData = "paReq="+URLEncoder.encode(paReq,"UTF8")+
                    "&termUrl="+URLEncoder.encode(termUrl, "UTF-8") +
                    "&orderId="+URLEncoder.encode(orderId, "UTF-8");
            mWeb.postUrl(acsUrl,postData.getBytes());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
