package ru.timrlm.oki.ui.main;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.local.RealmHelper;
import ru.timrlm.oki.data.model.iko.Customer;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.ui.foodgroup.FoodGroupPresenter;
import ru.timrlm.oki.ui.product.fragment.ProductFragment;
import ru.timrlm.oki.util.RxEventBus;

@ConfigPersistent
public class MainPresenter extends BasePresenter<MainMvpView> {

    private final DataManager mDataManager;

    @Inject
    public MainPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);
        getMvpView().setCount(mDataManager.getOrderInfo().count);
        mDataManager.getRxEventBus().filteredObservable(RealmHelper.OrderInfo.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o->getMvpView().setCount(o.count),Throwable::printStackTrace);
        mDataManager.getRxEventBus().filteredObservable(FoodGroupPresenter.UISearch.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o->{ if (getMvpView() != null) getMvpView().setSearchOrChat(o.need); },Throwable::printStackTrace); }

    public void sendUISearch(boolean b) {
        mDataManager.getRxEventBus().post(new FoodGroupPresenter.UISearch(b));
    }
}
