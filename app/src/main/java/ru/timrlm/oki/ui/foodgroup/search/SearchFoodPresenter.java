package ru.timrlm.oki.ui.foodgroup.search;

import android.util.Log;

import javax.inject.Inject;

import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;

@ConfigPersistent
class SearchFoodPresenter extends BasePresenter<SearchFoodMvpView> {
    private DataManager mDataManager;

    @Inject
    public SearchFoodPresenter(DataManager mDataManager) { this.mDataManager = mDataManager; }

    public void getAllProducts(){
        getMvpView().setList(mDataManager.getProducts());
    }

    public void getProductsByName(String name){
        getMvpView().setList(mDataManager.getProductsByName(name));
    }
}
