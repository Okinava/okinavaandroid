package ru.timrlm.oki.ui.chat.mark;

import ru.timrlm.oki.ui.base.MvpView;

interface MarkMvpView extends MvpView {
    void result();
    void error();
}
