package ru.timrlm.oki.ui.foodgroup.search;

import java.util.List;

import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.base.MvpView;

interface SearchFoodMvpView extends MvpView{
    void setList(List<lProduct> products);
}
