package ru.timrlm.oki.ui.prof.repeat;


import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.util.RxUtil;
import ru.timrlm.oki.util.ViewUtil;

@ConfigPersistent
class RepeatPresenter extends BasePresenter<RepeatMvpView> {
    private DataManager mDataManager;

    @Inject
    public RepeatPresenter(DataManager dataManager) {
        this.mDataManager = dataManager;
    }

    private Disposable disposable;
    public void timer(String date) {
        try {
            String t = ViewUtil.time(date);
            if (t.equals("00:00")) return;
            getMvpView().setTime(t);
            disposable = Observable.timer(1,TimeUnit.MINUTES)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((l)->{
                        String tt = ViewUtil.time(date);
                        if (tt.equals("00:00")) return;
                        getMvpView().setTime(tt);
                    },(e)->{});
        }catch (Exception e){

        }
    }

    @Override
    public void detachView() {
        RxUtil.dispose(disposable);
        super.detachView();
    }

    public void check(List<CusOrder.Item> items) {
        String text = mDataManager.addHis(items);
        if (text == null) { getMvpView().ok(); return;}
        if (text.equals("В данном заказе не все позиции товаров активны для продажи")) { getMvpView().ok(text); return;}
        getMvpView().er(text);
    }
}
