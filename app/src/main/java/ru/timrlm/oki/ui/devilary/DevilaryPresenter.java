package ru.timrlm.oki.ui.devilary;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.util.RxUtil;

@ConfigPersistent
class DevilaryPresenter extends BasePresenter<DevilaryMvpView> {
    private final DataManager mDataManager;

    @Inject
    public DevilaryPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    public void load(){
        lCustomer user = mDataManager.findUser(mDataManager.getPreferencesHelper().getPhone());
        getMvpView().setUser(user.getName() == null ? "Нет имени" : user.getName(),user.getPhone(),mDataManager.getOrderInfo().sum);
    }

    private Disposable disposable;
    public void loadDost(){
        disposable = mDataManager.getAdres()
                .flatMapIterable(l->l)
                .map(a-> a.getStreet()+" "+a.getHome() + (a.getApartment().length()>0 ? ", кв. " + a.getApartment() : ""))
                .toList()
                .subscribe(l->getMvpView().setAdres(l));
    }

    private Disposable disposableT;
    public void loadSam(){
        disposableT = mDataManager.getRestorans()
                .map(r->r.getDeliveryTerminals())
                .flatMapIterable(l->l)
                .map(s->s.getName())
                .toList()
                .subscribe(res->
                {if (getMvpView() != null) getMvpView().setRestorans(res);},throwable -> {
                    getMvpView().setRestorans(new ArrayList<>());
                });
    }

    Disposable disposableTh;
    public void deleteAdres(int pos){
        disposableTh = mDataManager.getAdres().subscribe(l-> mDataManager.delete(l.get(pos).getId()));
    }

    @Override
    public void detachView() {
        RxUtil.dispose(disposable);
        RxUtil.dispose(disposableT);
        RxUtil.dispose(disposableTh);
        super.detachView();
    }
}
