package ru.timrlm.oki.ui.sale;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.ui.sale.salelist.SaleListActivity;
import ru.timrlm.oki.util.ViewUtil;

public class SaleFragment extends Fragment implements SaleMvpView, SaleAdapter.OnSaleDOIT {
    @Inject SalePresenter mPresenter;
    private View rootView;
    @BindView(R.id.rv) RecyclerView rv;
    @BindView(R.id.refresh) View refresh;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sale, container,false);
        ButterKnife.bind(this,rootView);
        ((MainActivity) getActivity()).activityComponent().inject(this);
        mPresenter.attachView(this);
        mPresenter.load();
        return rootView;
    }

    @Override
    public void setActions(List<Action> actions) {
        refresh.setVisibility(View.GONE);
        rv.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        rv.setAdapter(new SaleAdapter(actions,this));
    }

    @Override
    public void read(Action a) {
        SaleListActivity.action = a;
        startActivity(new Intent(rootView.getContext(), SaleListActivity.class));
    }


    @OnClick(R.id.title)
    public void scroll(){ if (rv.getAdapter() != null) rv.scrollToPosition(0);}

    @Override
    public void share(Action a) {
        ViewUtil.shareText(rootView.getContext(),a.getTitle(),a.getDescription());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.detachView();
    }
}
