package ru.timrlm.oki.ui.prof.repeat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.iko.History;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.ui.order.OrderActivity;
import ru.timrlm.oki.ui.prof.repeat.adapter.RepeatAdapter;

public class RepeatActivity extends BaseSwipeBackActivity implements RepeatMvpView {
    @Inject RepeatPresenter mPresenter;
    public static History.DeliveryHistory dh;
    @BindView(R.id.id) TextView id;
    @BindView(R.id.time) TextView time;
    @BindView(R.id.price) TextView price;
    @BindView(R.id.rv) RecyclerView rv;
    @BindView(R.id.left) CardView left;
    @BindView(R.id.center) CardView center;
    @BindView(R.id.right) CardView right;
    @BindView(R.id.lone) View lone;
    @BindView(R.id.ltwo) View ltwo;
    private RepeatAdapter adapter = new RepeatAdapter();

    @Override
    public int setlayout() { return R.layout.activity_repeat; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        id.setText(getString(R.string.order_num, dh.getNumber()));
        rv.setLayoutManager(new LinearLayoutManager(RepeatActivity.this));
        rv.setAdapter(adapter);
        adapter.setItems(dh.getItems());
        int sum = 0;
        for (CusOrder.Item i : dh.getItems()){
            sum += i.getSum();
        }
        price.setText(getString(R.string.price, sum));

        switch (dh.getStatus()) {
            case "NEW":
                left.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lgreen));
                break;
            case "WAITING":
                left.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lgreen));
                break;
            case "ON_WAY":
                left.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lgreen));
                center.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lgreen));
                lone.setVisibility(View.VISIBLE);
                break;
            case "CLOSED":
                left.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lgreen));
                center.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lgreen));
                right.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lgreen));
                lone.setVisibility(View.VISIBLE);
                ltwo.setVisibility(View.VISIBLE);
                break;
            case "DELIVERED":
                left.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lgreen));
                center.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lgreen));
                right.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lgreen));
                lone.setVisibility(View.VISIBLE);
                ltwo.setVisibility(View.VISIBLE);
                break;
        }
        mPresenter.timer(dh.getDate());
    }

    @OnClick(R.id.phone)
    void phone(){
        if(ContextCompat.checkSelfPermission(
                RepeatActivity.this,android.Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RepeatActivity.this, new
                    String[]{android.Manifest.permission.CALL_PHONE}, 0);
        }else {
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8 (843) 233-44-33")));
        }
    }

    @OnClick(R.id.btn)
    void repeat(){
        new MaterialDialog.Builder(RepeatActivity.this)
                .content("Вы хотите повторить этот заказ?")
                .positiveText("Да")
                .negativeText("Нет")
                .onPositive((d,p)->{
                    mPresenter.check(dh.getItems());
                })
                .show();
    }

    @Override
    public void ok() {
        startActivity(new Intent(RepeatActivity.this,OrderActivity.class));
        finish();
    }

    @Override
    public void ok(String text) {
        new MaterialDialog.Builder(RepeatActivity.this)
                .content(text)
                .positiveText("Хорошо")
                .dismissListener((d)->{
                    startActivity(new Intent(RepeatActivity.this,OrderActivity.class));
                    finish();
                })
                .show();
    }

    @Override
    public void er(String text) {
        new MaterialDialog.Builder(RepeatActivity.this)
                .content(text)
                .positiveText("Хорошо")
                .show();
    }

    @Override
    public void setTime(String t) {
        time.setText(t);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @OnClick({R.id.back_card,R.id.back_top})
    void chat(){ finish(); }
}
