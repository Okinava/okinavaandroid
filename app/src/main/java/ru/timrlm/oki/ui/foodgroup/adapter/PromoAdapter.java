package ru.timrlm.oki.ui.foodgroup.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.Promo;

public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.PromoView> {
    private List<Promo> nPromos;
    private Context mContext;

    public PromoAdapter(List<Promo> nPromos) { this.nPromos = nPromos; }

    @NonNull
    @Override
    public PromoView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new PromoView(LayoutInflater.from(mContext).inflate(R.layout.rv_promo,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull PromoView holder, int position) {
        holder.title.setText(nPromos.get(position).getTitle());
        Glide.with(mContext)
                .load(nPromos.get(position).getImg())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_refresh)
                        .error(R.drawable.ic_photo)
                )
                .into(holder.img);
    }

    @Override
    public int getItemCount() { return nPromos.size(); }

    class PromoView extends RecyclerView.ViewHolder{
        @BindView(R.id.img) ImageView img;
        @BindView(R.id.title) TextView title;

        public PromoView(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
