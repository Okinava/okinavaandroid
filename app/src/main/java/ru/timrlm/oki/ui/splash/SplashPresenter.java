package ru.timrlm.oki.ui.splash;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.iko.Foods;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.util.RxUtil;

@ConfigPersistent
class SplashPresenter extends BasePresenter<SplashMvpView> {
    private DataManager mDataManager;

    @Inject
    public SplashPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
        mDataManager.getRxEventBus().filteredObservable(Foods.class).subscribe(f->getMvpView().startFood(), Throwable::printStackTrace);
        mDataManager.getRxEventBus().filteredObservable(DataManager.NOCONNECT.class)
                .subscribe(f->{
                   if (mDataManager.getGroups().size() == 0) getMvpView().noconect();
                }, Throwable::printStackTrace);
    }

    Disposable disposable;
    public void checkVersion() {
        RxUtil.dispose(disposable);
        disposable = mDataManager.checkVersion()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r->{
                    if (r){
                        checkProduct();
                    }else {
                        getMvpView().downloadNew();
                    }
                },t->checkProduct());
    }

    public void checkProduct(){
        int res = mDataManager.getGroups().size();
        if (res != 0){
            getMvpView().startFood();
        }
    }

    public void open(){
        if (getMvpView() != null) {
            if (mDataManager.isFirstLaunch()) {
                getMvpView().startTutorial();
            } else {
                getMvpView().startMain();
            }
        }
    }

    public void reload() {
        mDataManager.load();
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.dispose(disposable);
    }
}
