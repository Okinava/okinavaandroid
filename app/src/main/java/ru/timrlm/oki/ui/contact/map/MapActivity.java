package ru.timrlm.oki.ui.contact.map;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.smart.SmartRestoran;
import ru.timrlm.oki.ui.base.BaseActivity;

public class MapActivity extends BaseActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    public static List<SmartRestoran> restoranList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public int setlayout() { return R.layout.activity_map; }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Drawable circleDrawable = getResources().getDrawable(R.drawable.point);
        BitmapDescriptor icon = getMarkerIconFromDrawable(circleDrawable);
        mMap = googleMap;
        for( SmartRestoran r : restoranList) {
            try {
                mMap.addMarker(new MarkerOptions().position(new LatLng(Float.valueOf(r.getCoordinates().split(" ")[1]), Float.valueOf(r.getCoordinates().split(" ")[0]))).title(r.getName())
                        .snippet("Вс-Чт: с 10:00 до 01:00; Пт-Сб: с 10:00 до 02:00")
                        .icon(icon));
            }catch (Exception e){}
        }
        mMap.moveCamera(CameraUpdateFactory.zoomTo(10));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(55.788211,49.121299)));
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
