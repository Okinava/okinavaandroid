package ru.timrlm.oki.ui.contact;


import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.iko.Restorans;
import ru.timrlm.oki.data.model.smart.SmartRestoran;
import ru.timrlm.oki.ui.contact.adapter.RestoranAdapter;
import ru.timrlm.oki.ui.contact.map.MapActivity;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.util.ViewUtil;

public class ContactFragment extends Fragment implements ContactMvpView {
    @Inject ContactPresenter mPresenter;
    private View rootView;
    @BindView(R.id.rv) RecyclerView rv;
    @BindView(R.id.appbar) AppBarLayout appBarLayout;

    @Override
    public void setRest(List<SmartRestoran> terminals) {
        rootView.findViewById(R.id.refresh).setVisibility(View.GONE);
        rv.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        rv.setAdapter(new RestoranAdapter(terminals));
    }

    @OnClick(R.id.phone)
    void phone(){
        if(ContextCompat.checkSelfPermission(
                rootView.getContext(),android.Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new
                    String[]{android.Manifest.permission.CALL_PHONE}, 0);
        }else {
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8 (843) 233-44-33")));
        }
    }

    @OnClick(R.id.email)
    void email(){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "Отправить письмо","commerce@okinavakazan.ru", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "text");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "text");
        startActivity(Intent.createChooser(emailIntent, "Отправить письмо..."));
    }

    @OnClick(R.id.site)
    void site(){
        String url = "https://www.okinavakazan.ru";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.vk)
    void vk(){
        String url = "https://www.vk.com/okinava_kazan";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.insta)
    void insta(){
        String url = "https://www.instagram.com/okinavakazan/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.map)
    void list(RadioButton radioButton){
        ((MainActivity) getActivity()).showProgres();
       mPresenter.getRest();
    }

    @Override
    public void showRest(List<SmartRestoran> terminals) {
        ((MainActivity) getActivity()).hideProgres();
        MapActivity.restoranList = terminals;
        startActivity(new Intent(getActivity(), MapActivity.class));
        ((RadioButton) rootView.findViewById(R.id.map)).setChecked(false);
        ((RadioButton) rootView.findViewById(R.id.list)).setChecked(true);
    }

    @Override
    public void error() {
        ((MainActivity) getActivity()).hideProgres();
        ((MainActivity) getActivity()).showError("Нет интернет соединения");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_contact,container,false);
        ButterKnife.bind(this,rootView);
        ((MainActivity) getActivity()).activityComponent().inject(this);
        mPresenter.attachView(this);
        mPresenter.load();
        return rootView;
    }

    @OnClick(R.id.title)
    public void scroll(){
        ((NestedScrollView) rootView.findViewById(R.id.need)).scrollTo(0,0);
        appBarLayout.setExpanded(appBarLayout.getHeight() - appBarLayout.getBottom() != 0,true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.detachView();
    }
}
