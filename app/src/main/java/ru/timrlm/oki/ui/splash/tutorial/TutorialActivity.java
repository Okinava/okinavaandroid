package ru.timrlm.oki.ui.splash.tutorial;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.ui.splash.SplashActivity;

public class TutorialActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {
    @BindView(R.id.btn) AppCompatButton btn;
    @BindView(R.id.vp) ViewPager vp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);
        vp.setAdapter(new TutotialVPAdapter(TutorialActivity.this));
        vp.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

    @Override
    public void onPageSelected(int position) {
        switch (position){
            case 6: btn.setText("Начать пользоваться"); break;
            default: btn.setText("Далее"); break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) { }

    @OnClick(R.id.btn)
    void next(){
        if (vp.getCurrentItem() == 6) {
            startActivity(new Intent(TutorialActivity.this, MainActivity.class));
            finish();
        }else{
            vp.setCurrentItem(vp.getCurrentItem()+1,true);
        }
    }
}
