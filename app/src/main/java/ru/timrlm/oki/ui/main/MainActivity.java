package ru.timrlm.oki.ui.main;


import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.ui.auto.AutoActivity;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.ui.chat.ChatActivity;
import ru.timrlm.oki.ui.contact.ContactFragment;
import ru.timrlm.oki.ui.foodgroup.FoodGroupFragment;
import ru.timrlm.oki.ui.foodgroup.search.SearchFoodActivity;
import ru.timrlm.oki.ui.main.vpadapter.MyViewPager;
import ru.timrlm.oki.ui.main.vpadapter.ViewPagerAdapter;
import ru.timrlm.oki.ui.order.OrderActivity;
import ru.timrlm.oki.ui.prof.ProfFragment;
import ru.timrlm.oki.ui.sale.SaleFragment;
import ru.timrlm.oki.util.ViewUtil;

public class MainActivity extends BaseActivity implements MainMvpView, BottomNavigationView.OnNavigationItemSelectedListener {
    @Inject MainPresenter mMainPresenter;
    @BindView(R.id.bottom_bar) BottomNavigationViewEx bNavigationBar;
    FragmentTransaction mTransaction;
    private int current = 0;
    private Fragment fragment;

    public void init() {
        bNavigationBar.enableShiftingMode(false);
        bNavigationBar.enableItemShiftingMode(false);
        bNavigationBar.setOnNavigationItemSelectedListener(this);
        bNavigationBar.setIconTintList(0, getResources().getColorStateList(R.color.colorPrimary));
        bNavigationBar.setTextTintList(0, getResources().getColorStateList(R.color.colorPrimary));
        bNavigationBar.setIconSize(16,16);
        bNavigationBar.setIconsMarginTop(ViewUtil.dpToPx(10));
        bNavigationBar.setTextSize(11);

        mTransaction = getSupportFragmentManager().beginTransaction();
        fragment = new FoodGroupFragment();
        mTransaction.replace(R.id.frame, fragment);
        mTransaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (bNavigationBar.getMenuItemPosition(item) != current) {
            for (int i = 0; i < bNavigationBar.getItemCount(); i++) {
                bNavigationBar.setIconTintList(i, getResources().getColorStateList(R.color.lightgray));
                bNavigationBar.setTextTintList(i, getResources().getColorStateList(R.color.lightgray));
            }
            bNavigationBar.setIconTintList(bNavigationBar.getMenuItemPosition(item), getResources().getColorStateList(R.color.colorPrimary));
            bNavigationBar.setTextTintList(bNavigationBar.getMenuItemPosition(item), getResources().getColorStateList(R.color.colorPrimary));

            mTransaction = getSupportFragmentManager().beginTransaction();
            mTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            mTransaction.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_in_right);
            current = bNavigationBar.getMenuItemPosition(item);
            switch (bNavigationBar.getMenuItemPosition(item)) {
                case 0: fragment = new FoodGroupFragment();break;
                case 1: fragment = new SaleFragment();break;
                case 2: fragment = new ProfFragment();break;
                case 3: fragment = new ContactFragment();break;
            }
            mTransaction.replace(R.id.frame, fragment);
            mTransaction.commit();
            mMainPresenter.sendUISearch(false);
        }else{
            try {
                switch (current) {
                    case 0:
                        ((FoodGroupFragment) fragment).scroll();
                        break;
                    case 1:
                        ((SaleFragment) fragment).scroll();
                        break;
                    case 2:
                        ((ProfFragment) fragment).scroll();
                        break;
                    case 3:
                        ((ContactFragment) fragment).scroll();
                        break;
                }
            }catch (Exception e){}
        }
        return true;
    }

    @Override
    public void setCount(int count) {
        findViewById(R.id.circle).setVisibility( count == 0 ? View.GONE : View.VISIBLE);
        findViewById(R.id.shop_card).setVisibility( count == 0 ? View.GONE : View.VISIBLE);
    }

    @OnClick({R.id.shop_card,R.id.shop_top})
    void shop(){
        startActivity(new Intent(MainActivity.this, OrderActivity.class));
    }

    private boolean search = false;
    @Override
    public void setSearchOrChat(boolean b) {
        search = b;
        ((ImageButton)findViewById(R.id.chat_top)).setImageResource(b ? R.drawable.ic_search : R.drawable.ic_menu_chat);
    }

    @OnClick({R.id.chat_top,R.id.shop_card})
    void chat(){
        if (search){
            startActivity(new Intent(MainActivity.this, SearchFoodActivity.class));
        }else {
            startActivity(new Intent(MainActivity.this, ChatActivity.class));
        }
    }

    @Override
    public int setlayout() { return R.layout.activity_main; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mMainPresenter.attachView(this);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMainPresenter.detachView();
    }
}
