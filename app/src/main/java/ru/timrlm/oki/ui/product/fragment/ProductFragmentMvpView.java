package ru.timrlm.oki.ui.product.fragment;

import java.util.List;

import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.base.MvpView;

interface ProductFragmentMvpView extends MvpView {
    void setModifaers(List<String> lProductList, List<lProduct> products);

    void clear();

    void resul();

    void errors(String s);
}
