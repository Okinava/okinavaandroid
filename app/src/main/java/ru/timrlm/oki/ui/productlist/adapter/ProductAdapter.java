package ru.timrlm.oki.ui.productlist.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.util.ViewUtil;

/**
 * Created by timerlan on 28.03.2018.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHoulder> {
    private List<lProduct> mProducts = new ArrayList<>();
    private List<lOrder> mOrders;
    private Context mContext;
    private OnClickListener onClickListener = new OnClickListener() {
        @Override public void onClick(lProduct product) { }
        @Override public void by(lProduct product) { }
        @Override public void minus(lProduct product) { }
        @Override public void star(lProduct product) { }
    };

    public void setOrders(List<lOrder> mOrders) {
        this.mOrders = mOrders;
    }

    public void setOnClickListener(OnClickListener onClickListener) { this.onClickListener = onClickListener; }

    public void setProduct(List<lProduct> products, List<lOrder> orders){
        mProducts = products;
        mOrders = orders;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) { return mProducts.get(position).getId() == null ? 0 : 1; }

    @Override
    public int getItemCount() { return mProducts.size(); }

    @NonNull
    @Override
    public ProductViewHoulder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new ProductViewHoulder(LayoutInflater.from(mContext).inflate(viewType == 1 ? R.layout.rv_product
                : R.layout.rv_title
                ,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHoulder holder, int position) {
        if (getItemViewType(position) == 1) {
            holder.name.setText(mProducts.get(position).getName());
            holder.desc.setText(mProducts.get(position).getDescription());
            holder.price.setText(mProducts.get(position).getPrice() + " " + ViewUtil.R);
            Glide.with(mContext)
                    .load(mProducts.get(position).getImg())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_refresh)
                            .error(R.drawable.ic_photo)
                    )
                    .into(holder.img);
            holder.btn.setOnClickListener((v)->  onClickListener.onClick(mProducts.get(position)) );
            holder.btn.setOnLongClickListener(v->{
                onClickListener.star(mProducts.get(position));
                return false;
            });
            int count = 0;
            for( lOrder o : mOrders)
               if ( o.getId().equals(mProducts.get(position).getId()) ) count = o.getCount();
            if (mProducts.get(position).getModifaers().length()>0) count = 0;

            holder.by.setVisibility(count > 0 ? View.GONE : View.VISIBLE);
            holder.counter.setVisibility(count == 0 ? View.GONE : View.VISIBLE);
            holder.count.setText("x" + count);

            holder.by.setOnClickListener((v)->{
                onClickListener.by(mProducts.get(position));
//                notifyItemChanged(position);
            });
            holder.plus.setOnClickListener((v)->{
                onClickListener.by(mProducts.get(position));
//                notifyItemChanged(position);
            });
            holder.minus.setOnClickListener((v)-> {
                onClickListener.minus(mProducts.get(position));
//                notifyItemChanged(position);
            });
        }else{
            holder.title.setText(mProducts.get(position).getName());
        }
    }

    class ProductViewHoulder extends RecyclerView.ViewHolder{
        @Nullable @BindView(R.id.img) ImageView img;
        @Nullable @BindView(R.id.name) TextView name;
        @Nullable @BindView(R.id.desc) TextView desc;
        @Nullable @BindView(R.id.price) TextView price;
        @Nullable @BindView(R.id.btn) View btn;
        @Nullable @BindView(R.id.by) View by;
        @Nullable @BindView(R.id.counter) View counter;
        @Nullable @BindView(R.id.count) TextView count;
        @Nullable @BindView(R.id.plus) View plus;
        @Nullable @BindView(R.id.minus) View minus;

        @Nullable @BindView(R.id.title) TextView title;

        public ProductViewHoulder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public interface OnClickListener {
        void onClick(lProduct product);
        void by(lProduct product);
        void minus(lProduct product);
        void star(lProduct product);
    }
}
