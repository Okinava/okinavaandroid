package ru.timrlm.oki.ui.order.search;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnLongClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.util.ViewUtil;

import static android.widget.ListPopupWindow.WRAP_CONTENT;

public class SelectAdapter extends RecyclerView.Adapter<SelectAdapter.SelectView> {
    private List<String> list;
    private int pos = -1;
    private Context mContext;
    private boolean needRemove = false;
    private int icon;
    private OnSelectListener onSelectListener = (s)->{};
    private OnDeleteListener onDeleteListener = (p)->{};
    public void setOnSelectListener(OnSelectListener onSelectListener) { this.onSelectListener = onSelectListener; }
    public void setOnDeleteListener(OnDeleteListener onDeleteListener) { this.onDeleteListener = onDeleteListener; }

    public SelectAdapter(List<String> list, int pos){ this.list = list; this.pos = pos;}

    public SelectAdapter(List<String> list, int pos, int icon){ this.list = list; this.pos = pos; needRemove = true; this.icon = icon; }

    public void update(List<String> list){
        this.list = list;
        notifyDataSetChanged();
    }

    public int getPos() { return pos; }
    public boolean disPos = false;

    @NonNull
    @Override
    public SelectView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new SelectView(LayoutInflater.from(mContext).inflate(R.layout.view_select,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull SelectView holder, int position) {
        holder.select.setVisibility(pos == position && !disPos ? View.VISIBLE : View.INVISIBLE);
        holder.text.setTypeface(null, pos == position && !disPos ? Typeface.BOLD : Typeface.NORMAL);
        holder.text.setText(list.get(position));
        if (icon != 0) holder.img.setImageDrawable(mContext.getDrawable(icon));
        if (icon == R.drawable.empty) {
            holder.img.setVisibility(View.GONE);
            holder.space.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() { return list.size(); }

    public class SelectView extends RecyclerView.ViewHolder{
        @BindView(R.id.text) TextView text;
        @BindView(R.id.img) ImageView img;
        @BindView(R.id.select) View select;
        @BindView(R.id.space) View space;

        public SelectView(View view){
            super(view);
            ButterKnife.bind(this,view);
        }

        @OnClick(R.id.btn)
        public void onClick(View v) {
            if (getAdapterPosition()!=pos){
                int oldpos = pos;
                pos = getAdapterPosition();
                try{ notifyItemChanged(oldpos); notifyItemChanged(pos); } catch (Exception e){}
                onSelectListener.onSelect(list.get(getAdapterPosition()));
                ViewUtil.vibrate(mContext);
            }
        }

        @OnLongClick(R.id.btn)
        public boolean delete(){
            if (needRemove) {
                new MaterialDialog.Builder(mContext)
                        .title("Удалить")
                        .content("Вы действительно хотите удалить " + list.get(getAdapterPosition()) + "?")
                        .positiveText("Да")
                        .negativeText("Нет")
                        .onPositive((d,w)->{
                            if (getAdapterPosition() == pos) pos = -1;
                            onDeleteListener.onDelete(getAdapterPosition());
                            list.remove(getAdapterPosition());
                            notifyItemRemoved(getAdapterPosition());
                        })
                        .show();
            }
            return true;
        }
    }

    public interface OnSelectListener{ void onSelect(String text);}
    public interface OnDeleteListener{ void onDelete(int pos);}
}
