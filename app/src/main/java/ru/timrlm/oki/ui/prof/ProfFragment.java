package ru.timrlm.oki.ui.prof;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.iko.History;
import ru.timrlm.oki.ui.auto.AutoActivity;
import ru.timrlm.oki.ui.devilary.newadress.NewAdressActivity;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.ui.order.OrderActivity;
import ru.timrlm.oki.ui.order.search.SelectAdapter;
import ru.timrlm.oki.ui.prof.adapter.HistoryAdapter;
import ru.timrlm.oki.ui.prof.repeat.RepeatActivity;
import ru.timrlm.oki.ui.prof.set.SetProfActivity;
import ru.timrlm.oki.util.AppBarStateChangeListener;
import ru.timrlm.oki.util.ViewUtil;

public class ProfFragment extends Fragment implements PforMvpView, SwipeRefreshLayout.OnRefreshListener, HistoryAdapter.Listener {
    @Inject ProfPresenter mPresenter;
    private View rootView;
    @BindView(R.id.rv_history) RecyclerView rv_history;
    @BindView(R.id.refresh) SwipeRefreshLayout swipeRefreshLayout;
    private HistoryAdapter Hadapter = new HistoryAdapter();

    @Override
    public void onStart() {
        super.onStart();
        rv_history.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        rv_history.setAdapter(Hadapter);
        Hadapter.setListener(this);
        mPresenter.isAuto();
    }

    @Override
    public void auto(boolean b) {
        rootView.findViewById(R.id.need).setVisibility(b ? View.GONE : View.VISIBLE);
        rv_history.setHasFixedSize(true);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void setAdres(List<String> adres) {  Hadapter.setAdres(adres); }

    @Override
    public void onRefresh() {
        mPresenter.reload();
    }

    @OnClick(R.id.title)
    public void scroll(){  rv_history.scrollTo(0,0); }

    @Override
    public void setHistory(List<History.DeliveryHistory> history) {
        swipeRefreshLayout.setRefreshing(false);
        history.add(0,null);
        Hadapter.setHistories(history);
    }

    @Override
    public void endR() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setUser(String name, String status, int bonus, String phone, String bonusCode, int sex, String date) {
        Hadapter.setUser(name,status,bonus,phone,bonusCode,sex,date);
    }

    @Override
    public void updateCustomer(boolean isAuto) { mPresenter.isAuto(); }
    @OnClick(R.id.btn)
    void auto(){ startActivity(new Intent(getActivity(), AutoActivity.class)); }

    @Override
    public void delete(int pos) {  mPresenter.deleteAdres(pos); }

    @Override
    public void settings() {startActivity(new Intent(getActivity(), SetProfActivity.class)); }

    @Override
    public void add() { startActivityForResult(new Intent(getActivity(), NewAdressActivity.class),2); }

    @Override
    public void longClick(History.DeliveryHistory item) {
        new MaterialDialog.Builder(getActivity())
                .content("Вы хотите повторить этот заказ?")
                .positiveText("Да")
                .negativeText("Нет")
                .onPositive((d,p)->{
                    mPresenter.check(item.getItems());
                })
                .show();
    }

    @Override
    public void ok() {
        startActivity(new Intent(getActivity(),OrderActivity.class));
    }

    @Override
    public void ok(String text) {
        new MaterialDialog.Builder(getActivity())
                .content(text)
                .positiveText("Хорошо")
                .dismissListener((d)->{
                    startActivity(new Intent(getActivity(),OrderActivity.class));
                })
                .show();
    }

    @Override
    public void er(String text) {
        new MaterialDialog.Builder(getActivity())
                .content(text)
                .positiveText("Хорошо")
                .show();
    }


    @Override
    public void click(History.DeliveryHistory item) {
        if (item == null) return;
        RepeatActivity.dh = item;
        startActivity(new Intent(getContext(), RepeatActivity.class));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.loadDost();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_needauto,container,false);
        ButterKnife.bind(this,rootView);
        ((MainActivity) getActivity()).activityComponent().inject(this);
        mPresenter.attachView(this);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.detachView();
    }
}
