package ru.timrlm.oki.ui.prof;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.local.RealmHelper;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.iko.History;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.ui.main.MainMvpView;
import ru.timrlm.oki.ui.product.fragment.ProductFragment;
import ru.timrlm.oki.util.RxUtil;

@ConfigPersistent
class ProfPresenter extends BasePresenter<PforMvpView> {
    private DataManager mDataManager;
    private Disposable disposableR;

    @Inject
    public ProfPresenter(DataManager mDataManager) { this.mDataManager = mDataManager; }

    @Override
    public void attachView(PforMvpView mvpView) {
        super.attachView(mvpView);
        disposableR = mDataManager.getRxEventBus().filteredObservable(lCustomer.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(c-> {
                    getMvpView().setHistory(new ArrayList<>());
                    isAuto();
                } , Throwable::printStackTrace);
    }

    Disposable disposableA;
    public void isAuto(){
        lCustomer user = mDataManager.findUser(mDataManager.getPreferencesHelper().getPhone());
        if (user != null){
            getMvpView().setUser(user.getName() == null ? "Нет имени" : user.getName(),
                    user.getStatus(),
                    user.getBalance(),
                    user.getPhone(),
                    user.getCard(),
                    user.getSex(),
                    user.getBirthday());
            loadDost();
        }
        getMvpView().auto(user != null);
        getHistory();
    }

    private Disposable disposableQ;
    public void loadDost(){
        disposableQ = mDataManager.getAdres()
                .flatMapIterable(l->l)
                .subscribeOn(Schedulers.io())
                .map(a-> a.getStreet()+" "+a.getHome() + (a.getApartment().length()>0 ? ", кв. " + a.getApartment() : ""))
                .subscribeOn(Schedulers.io())
                .toList()
                .subscribeOn(Schedulers.io())
                .subscribe(l->{ if (getMvpView()!=null) getMvpView().setAdres(l); }, Throwable::printStackTrace);
    }

    private Disposable disposableV;
    public void reload(){
        RxUtil.dispose(disposableV);
        disposableV = mDataManager.reloadUser(mDataManager.getPreferencesHelper().getPhone())
            .subscribe(customer -> { }, t -> getHistory(), ()-> getHistory());
    }

    private Disposable disposable;
    public void getHistory(){
        RxUtil.dispose(disposable);
        disposable = mDataManager.getHistory()
                .subscribeOn(Schedulers.io())
                .map(history -> convert(history))
                .subscribeOn(Schedulers.io())
                .subscribe(
                       his-> getMvpView().setHistory(his),
                       t -> getMvpView().endR(),
                        () -> getMvpView().endR()
               );
    }

    public void deleteAdres(int pos){
        mDataManager.getAdres().subscribe(l-> mDataManager.delete(l.get(pos).getId()));
    }

    @Override
    public void detachView() {
        RxUtil.dispose(disposable);
        RxUtil.dispose(disposableR);
        RxUtil.dispose(disposableQ);
        RxUtil.dispose(disposableV);
        RxUtil.dispose(disposableA);
        super.detachView();
    }

    private List<History.DeliveryHistory> convert(History history){
        if (history == null) return new ArrayList<>();
        ArrayList<History.DeliveryHistory> result = new ArrayList<>();
        for (History.CustomersDeliveryHistory hi : history.getCustomersDeliveryHistory())
            for (History.DeliveryHistory h : hi.getDeliveryHistory()) result.add(h);
        Collections.sort(result,((o1, o2) -> {
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date do1 = formatter.parse(o1.getDate());
                Date do2 = formatter.parse(o2.getDate());
                if (do1.getTime() > do2.getTime()) return -1;
                else return 1;
            }catch (Exception e) { return 0; }
        }));
        return result;
    }

    public void check(List<CusOrder.Item> items) {
        String text = mDataManager.addHis(items);
        if (text == null) { getMvpView().ok(); return;}
        if (text.equals("В данном заказе не все позиции товаров активны для продажи")) { getMvpView().ok(text); return;}
        getMvpView().er(text);
    }
}
