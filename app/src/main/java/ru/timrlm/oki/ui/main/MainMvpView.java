package ru.timrlm.oki.ui.main;

import ru.timrlm.oki.data.model.iko.Customer;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.ui.base.MvpView;

public interface MainMvpView extends MvpView {
    void setCount(int count);
    void setSearchOrChat(boolean b);
}
