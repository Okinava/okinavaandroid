package ru.timrlm.oki.ui.auto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.imid.swipebacklayout.lib.app.SwipeBackActivity;
import ru.timrlm.oki.R;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;

public class WebActivity extends BaseSwipeBackActivity {
    public static String url = "";
    public static String title = "";
    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.web) WebView mWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mTitle.setText(title);
        mWeb.getSettings().setJavaScriptEnabled(true);
//        mWeb.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
        mWeb.loadUrl(url);
    }

    @Override
    public int setlayout() { return R.layout.activity_web; }
}
