package ru.timrlm.oki.ui.auto;

import android.util.Log;

import java.util.Random;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;

@ConfigPersistent
class AutoPresenter extends BasePresenter<AutoMvpView>{
    private final DataManager mDataManager;

    @Inject
    public AutoPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    void send(String phone){
        mDataManager.checkUser(phone, false).subscribe(c->{
            try{ if (c.getCategories().get(0).getId().equals("5527ee9e-31ba-47f5-9909-0773580d8765")) getMvpView().ban(); else sendTo(phone); }
            catch (Exception e){ sendTo(phone); }
        },throwable -> { if (getMvpView()!=null)  getMvpView().error();});
    }

    private void sendTo(String phone){
        Random r = new Random();
        Integer code = (r.nextInt(8) + 1)*1000+r.nextInt(9)*100+r.nextInt(9)*10+r.nextInt(9);
        mDataManager.sendSms("7"+phone,"Код авторизации Окинава: " + code.toString())
                .subscribe(o->getMvpView().success(code),t->getMvpView().error());
    }
}
