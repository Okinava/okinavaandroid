package ru.timrlm.oki.ui.chat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.timrlm.oki.R;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatView>{
    private List<ChatItem> mCi = new ArrayList<>();
    private OnRatingBarChangeListener mOnRatingBarChangeListener =  (id)->{};

    public void setCi(List<ChatItem> mCi) {
        this.mCi = mCi;
        notifyDataSetChanged();
    }

    public void setOnRatingBarChangeListener(OnRatingBarChangeListener mOnRatingBarChangeListener) {
        this.mOnRatingBarChangeListener = mOnRatingBarChangeListener;
    }

    @Override
    public int getItemViewType(int position) { return mCi.get(position).type; }

    @NonNull
    @Override
    public ChatView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChatView(LayoutInflater.from(parent.getContext()).inflate(
                viewType == 0 ? R.layout.rv_chat_one :
                viewType == 1 ? R.layout.rv_chat_two :
                viewType == 2 ? R.layout.rv_chat_three :
                        R.layout.rv_chat_four, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ChatView holder, int position) {
        switch (getItemViewType(position)){
            case 0:
                holder.date.setText(mCi.get(position).object);
                break;
            case 1:
                holder.date.setText(mCi.get(position).time);
                holder.text.setText(mCi.get(position).object);
                break;
            case 2: holder.ratingBar.setOnRatingBarChangeListener((v,r,u)->{
                    mOnRatingBarChangeListener.ratingBarChange(mCi.get(position).id);
                });
                break;
            case 3:
                holder.date.setText(mCi.get(position).time);
                holder.text.setText(mCi.get(position).object);
                break;
        }
    }

    @Override
    public int getItemCount() { return mCi.size(); }

    class ChatView extends RecyclerView.ViewHolder {
        @Nullable @BindView(R.id.date) TextView date;
        @Nullable @BindView(R.id.text) TextView text;
        @Nullable @BindView(R.id.ratingBar) RatingBar ratingBar;
        ChatView(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public static class ChatItem{
        public String id;
        public int type;
        public String object;
        public Long t;
        public String time;

        public ChatItem(String id, int type, String object, Long t, String time) {
            this.id = id;
            this.type = type;
            this.object = object;
            this.t = t;
            this.time = time;
        }
    }

    public interface OnRatingBarChangeListener{
        void ratingBarChange(String id);
    }
}
