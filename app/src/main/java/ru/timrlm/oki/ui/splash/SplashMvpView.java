package ru.timrlm.oki.ui.splash;

import ru.timrlm.oki.ui.base.MvpView;

interface SplashMvpView extends MvpView {
    void startFood();
    void startMain();
    void startTutorial();
    void noconect();

    void downloadNew();
}
