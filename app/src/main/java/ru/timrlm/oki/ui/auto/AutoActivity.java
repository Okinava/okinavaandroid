package ru.timrlm.oki.ui.auto;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.ui.auto.pass.PassActivity;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;

public class AutoActivity extends BaseSwipeBackActivity implements AutoMvpView {
    @Inject AutoPresenter mPresenter;
    @BindView(R.id.number) EditText mNumberView;
    @BindView(R.id.view) View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
    }

    @Override
    public int setlayout() { return R.layout.activity_auto; }

    @OnClick(R.id.btn)
    void auto(){
        if ( mNumberView.getText().toString().length() > 9) {
            showProgres();
            mPresenter.send(mNumberView.getText().toString());
        }else {
            Snackbar.make(view,"Неверный формат номера телефона",Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void success(int code) {
        hideProgres();
        PassActivity.code = String .valueOf(code);
        PassActivity.phone = mNumberView.getText().toString();
        PassActivity.ac = this;
        startActivity(new Intent(AutoActivity.this, PassActivity.class));
    }

    @OnClick(R.id.person_data)
    void person_data(){
        WebActivity.url = "https://okinavakazan.ru/popd.pdf";
        WebActivity.title = "Персональные данные";
        startActivity(new Intent(AutoActivity.this, WebActivity.class));
    }

    @Override
    public void ban() {
        new MaterialDialog.Builder(this)
                .title("Ошибка")
                .content("К сожалению данный номер в черном списке. Для авторизации обратитесь к администратору.")
                .positiveText("Хорошо")
                .onPositive((w,d)->finish())
                .show();
    }

    @Override
    public void error() {
        hideProgres();
        showError();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
