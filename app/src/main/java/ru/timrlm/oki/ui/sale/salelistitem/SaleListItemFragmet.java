package ru.timrlm.oki.ui.sale.salelistitem;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.ui.product.ProductActivity;
import ru.timrlm.oki.ui.product.fragment.ProductFragment;
import ru.timrlm.oki.util.ViewUtil;

public class SaleListItemFragmet extends Fragment {
    public Action action;
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_salelistitem, container, false);
        ButterKnife.bind(this,rootView);
        set();
        return rootView;
    }

    private void set() {
        Glide.with(this)
                .load(action.getImage())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_refresh)
                        .error(R.drawable.ic_photo)
                )
                .into(((ImageView) rootView.findViewById(R.id.img)));
        ((TextView) rootView.findViewById(R.id.desc)).setText(action.getDescription());
        ((TextView) rootView.findViewById(R.id.name)).setText(action.getTitle());
        ((TextView) rootView.findViewById(R.id.t_one)).setText(action.getHowItMake().get(0).getActionTitle());
        ((TextView) rootView.findViewById(R.id.t_two)).setText(action.getHowItMake().get(1).getActionTitle());
        ((TextView) rootView.findViewById(R.id.d_one)).setText(action.getHowItMake().get(0).getActionDesc());
        ((TextView) rootView.findViewById(R.id.d_two)).setText(action.getHowItMake().get(1).getActionDesc());
    }

    @OnClick(R.id.phone)
    void phone(){
        if(ContextCompat.checkSelfPermission(
                rootView.getContext(),android.Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new
                    String[]{android.Manifest.permission.CALL_PHONE}, 0);
        }else {
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8 (843) 233-44-33")));
        }
    }

    @OnClick(R.id.email)
    void email(){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "Отправить письмо","commerce@okinavakazan.ru", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "text");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "text");
        startActivity(Intent.createChooser(emailIntent, "Отправить письмо..."));
    }

    public static SaleListItemFragmet newInstance(Action action) {
        SaleListItemFragmet saleListItemFragmet = new SaleListItemFragmet();
        saleListItemFragmet.action = action;
        return saleListItemFragmet;
    }
}
