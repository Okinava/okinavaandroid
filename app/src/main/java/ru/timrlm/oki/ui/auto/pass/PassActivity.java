package ru.timrlm.oki.ui.auto.pass;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ru.timrlm.oki.R;
import ru.timrlm.oki.ui.auto.AutoActivity;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.util.ViewUtil;

import static butterknife.OnTextChanged.Callback.AFTER_TEXT_CHANGED;
import static butterknife.OnTextChanged.Callback.BEFORE_TEXT_CHANGED;

public class PassActivity  extends BaseActivity implements PassMvpView {
    @Inject PassPresenter mPresenter;
    @BindView(R.id.number) EditText mNumberView;
    @BindView(R.id.view) View view;
    public static String code;
    public static String phone;
    public static AutoActivity ac;
    private MaterialDialog progres;

    @OnTextChanged(value = R.id.number, callback = AFTER_TEXT_CHANGED)
    public void afterEmailInput(Editable editable) {
        if ( mNumberView.getText().toString().length() == 4) {
            if (mNumberView.getText().toString().equals(code)) {
                ViewUtil.hideKeyboard(PassActivity.this);
                progres = new MaterialDialog.Builder(this)
                        .content("Идет загрузка..")
                        .progress(true,0)
                        .cancelable(false)
                        .show();
                mPresenter.customerAuto(phone);
            }else {
                ViewUtil.hideKeyboard(PassActivity.this);
                new MaterialDialog.Builder(this)
                        .title("Ошибка")
                        .content("Неверный код авторизации")
                        .positiveText("Хорошо")
                        .onPositive((d,f)->{
                            finish();
                            startActivity(new Intent(PassActivity.this, PassActivity.class));
                        })
                        .show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Введите код из смс");
        activityComponent().inject(this);
        mPresenter.attachView(this);
    }

    @Override
    public void success() {
        progres.dismiss();
        finish();
        ac.finish();
    }

    @Override
    public void error() {
        progres.dismiss();
        new MaterialDialog.Builder(this)
                .title("Ошибка")
                .content("К сожалению произошла ошибка. Повторите попытку позже.")
                .positiveText("Хорошо")
                .cancelable(false)
                .onPositive((w,d)->{
                    finish();
                    ac.finish();
                })
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public int setlayout() { return R.layout.activity_pass; }
}
