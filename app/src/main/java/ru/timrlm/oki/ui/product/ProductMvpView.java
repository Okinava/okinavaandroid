package ru.timrlm.oki.ui.product;

import java.util.List;

import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.base.MvpView;

/**
 * Created by timerlan on 09.04.2018.
 */

interface ProductMvpView extends MvpView {
    void setProducts(List<lProduct> product, int position);
    void setCountZakaz(int count);
}
