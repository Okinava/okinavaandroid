package ru.timrlm.oki.ui.foodgroup.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.realm.lGroup;

/**
 * Created by timerlan on 28.03.2018.
 */

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.GroupViewHoulder> {
    private List<lGroup> mGroups = new ArrayList<>();
    private Context mContext;
    private OnClickListener onClickListener = (group -> {});

    public void setOnClickListener(OnClickListener onClickListener) { this.onClickListener = onClickListener; }

    public void setGroups(List<lGroup> groups){
        mGroups = groups;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() { return mGroups.size(); }

    @NonNull
    @Override
    public GroupViewHoulder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new GroupViewHoulder(LayoutInflater.from(mContext).inflate(R.layout.rv_group,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull GroupViewHoulder holder, int position) {
        holder.name.setText(mGroups.get(position).getName());
        Glide.with(mContext)
                .load(mGroups.get(position).getImg())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_refresh)
                        .error(R.drawable.ic_photo)
                )
                .into(holder.img);
    }

    class GroupViewHoulder extends RecyclerView.ViewHolder{

        @BindView(R.id.img) ImageView img;
        @BindView(R.id.name) TextView name;

        public GroupViewHoulder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }

        @OnClick(R.id.btn)
        void click(){ onClickListener.onClick(mGroups.get(getAdapterPosition()));}
    }

    public interface OnClickListener {
        void onClick(lGroup group);
    }
}
