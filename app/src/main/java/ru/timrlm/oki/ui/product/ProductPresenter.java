package ru.timrlm.oki.ui.product;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.local.RealmHelper;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.ui.product.fragment.ProductFragment;

/**
 * Created by timerlan on 09.04.2018.
 */

@ConfigPersistent
class ProductPresenter extends BasePresenter<ProductMvpView> {
    private final DataManager mDataManager;

    @Inject
    public ProductPresenter(DataManager dataManager){
        mDataManager = dataManager;
    }

    @Override
    public void attachView(ProductMvpView mvpView) {
        super.attachView(mvpView);
        getMvpView().setCountZakaz(mDataManager.getOrderInfo().count);
        mDataManager.getRxEventBus().filteredObservable(RealmHelper.OrderInfo.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o->getMvpView().setCountZakaz(o.count),Throwable::printStackTrace);
    }

    public void load(lProduct product){
        List<lProduct> list = mDataManager.getProductsFromProduct(product);
        int pos = -1;
        for (lProduct p : list){
            pos += 1;
            if (p.getId().equals(product.getId())) {
                getMvpView().setProducts(list,pos);
                return;
            }
        }
    }
}
