package ru.timrlm.oki.ui.foodgroup.search.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.productlist.adapter.ProductAdapter;

public class SearchFoodAdapter extends RecyclerView.Adapter<SearchFoodAdapter.SearchFoodView> {
    private Context mContext;
    private List<lProduct> mList;
    private SearchFoodAdapter.OnClickListener mListener;

    public SearchFoodAdapter(List<lProduct> products, SearchFoodAdapter.OnClickListener listener){
        mList = products;
        mListener = listener;
    }

    public void upd(List<lProduct> products){
        mList = products;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SearchFoodView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new SearchFoodView(LayoutInflater.from(mContext).inflate(viewType == 1 ? R.layout.view_search_food
                        : R.layout.rv_title
                ,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull SearchFoodView holder, int position) {
        if (getItemViewType(position) == 1) {
            Glide.with(mContext)
                    .load(mList.get(position).getImg())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_refresh)
                            .error(R.drawable.ic_photo)
                    )
                    .into(holder.imageView);
            holder.nameView.setText(mList.get(position).getName());
            holder.descView.setText(mList.get(position).getDescription());
            holder.btn.setOnClickListener((v)->  mListener.onClick(mList.get(position)) );
        }else {
            holder.title.setText(mList.get(position).getName());
        }
    }

    @Override
    public int getItemViewType(int position) { return mList.get(position).getId() == null ? 0 : 1; }

    @Override
    public int getItemCount() { return mList.size(); }

    class SearchFoodView extends RecyclerView.ViewHolder{
        @Nullable @BindView(R.id.img) AppCompatImageView imageView;
        @Nullable @BindView(R.id.name) AppCompatTextView nameView;
        @Nullable @BindView(R.id.desc) AppCompatTextView descView;
        @Nullable @BindView(R.id.btn) View btn;

        @Nullable @BindView(R.id.title) TextView title;

        SearchFoodView(View view){
            super(view);
            ButterKnife.bind(this,view);

        }
    }
    public interface OnClickListener{
        void onClick(lProduct product);
    }
}
