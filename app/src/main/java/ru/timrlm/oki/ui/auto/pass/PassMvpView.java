package ru.timrlm.oki.ui.auto.pass;

import ru.timrlm.oki.ui.base.MvpView;

interface PassMvpView extends MvpView {
    void success();
    void error();
}
