package ru.timrlm.oki.ui.sale.salelist;

import java.util.List;

import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.ui.base.MvpView;

interface SaleListMVPView extends MvpView {
    void setActions(List<Action> actions, int pos);
}
