package ru.timrlm.oki.ui.devilary;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.afollestad.materialdialogs.MaterialDialog;
import com.instacart.library.truetime.TrueTime;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.ui.auto.pass.PassActivity;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.ui.devilary.newadress.NewAdressActivity;
import ru.timrlm.oki.ui.devilary.pay.PayActivity;
import ru.timrlm.oki.ui.order.OrderActivity;
import ru.timrlm.oki.ui.order.search.SelectAdapter;
import ru.timrlm.oki.util.ViewUtil;

public class DevilaryActivity extends BaseSwipeBackActivity implements DevilaryMvpView, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    @Inject DevilaryPresenter mPresenter;
    @BindView(R.id.rv) RecyclerView rv;
    @BindView(R.id.new_adress) Button adrButton;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.phone) TextView phone;
    @BindView(R.id.time) EditText time;
    @BindView(R.id.sc) View sc;
    @BindView(R.id.count) TextView count;
    private SelectAdapter mAdapter;
    private int position = 0;
    private int sum = 0;

    private void init(){
        mPresenter.load();
        mPresenter.loadDost();
    }

    @OnClick(R.id.btn)
    void next(){
        if (!checkTime(cal)) return;
        if (newYear(cal)) return;
        if (((RadioButton) findViewById(R.id.dost)).isChecked()) {
            if (sum < 600) showError("Сумма заказа для создания заказа должна не меньше 600 рублей");
            else if (mAdapter.getPos() == -1) showError("Необходимо выбрать адрес доставки");
            else {
                PayActivity.pos = mAdapter.getPos();
                PayActivity.time = (String) time.getTag();
                PayActivity.type = 0;
                PayActivity.comment = ((TextView) findViewById(R.id.comment)).getText().toString();
                startActivity(new Intent(DevilaryActivity.this, PayActivity.class));
            }
        }else{
            PayActivity.pos = mAdapter.getPos();
            PayActivity.time = (String) time.getTag();
            PayActivity.type = 1;
            PayActivity.comment = ((TextView) findViewById(R.id.comment)).getText().toString();
            startActivity(new Intent(DevilaryActivity.this, PayActivity.class));
        }
    }

    @OnCheckedChanged(R.id.dost)
    public void changeType(RadioButton radioButton){
        if (radioButton.isChecked()){
            mPresenter.loadDost();
        }else{
            showProgres();
            mPresenter.loadSam();
        }
        (findViewById(R.id.rv_btn)).setVisibility(radioButton.isChecked() ? View.VISIBLE : View.GONE);
        ((TextView)(findViewById(R.id.rv_title))).setText(radioButton.isChecked() ? "Адреса доставки" : "Адреса самовывоза");
        ((TextView)(findViewById(R.id.time_title))).setText(radioButton.isChecked() ? "Время доставки" : "Время самовывоза");
    }

    @Override
    public void setRestorans(List<String> restorans) {
        hideProgres();
        mAdapter = new SelectAdapter(restorans,restorans.size() - 1,R.drawable.order_adress);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(mAdapter);
        setTime(25);
    }

    @Override
    public void setUser(String name, String phone, int sum) { this.name.setText(name); this.phone.setText(phone); this.sum = sum; }

    @Override
    public void setAdres(List<String> adres) {
        mAdapter = new SelectAdapter(adres,adres.size() - 1,R.drawable.order_adress);
        rv.setLayoutManager(new LinearLayoutManager(this));
        mAdapter.setOnDeleteListener(this::onDelete);
        rv.setAdapter(mAdapter);
        setTime(80);
    }

    private void setTime(int plus){
        cal =  ViewUtil.time();
        cal.add(Calendar.MINUTE, plus);
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        time.setTag(dt.format(cal.getTimeInMillis()));
        time.setText(new SimpleDateFormat("HH:mm dd.MM.yyyy").format(cal.getTimeInMillis()));
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        cal.set(Calendar.HOUR_OF_DAY,hourOfDay);
        cal.set(Calendar.MINUTE,minute);
        if (checkTime(cal)){
            newYear(cal);
        }
    }

    Calendar cal;
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        TimePickerDialog tpd = TimePickerDialog.newInstance(this,getTime().get(Calendar.HOUR),getTime().get(Calendar.MINUTE),true);
        if (getTime().get(Calendar.DAY_OF_MONTH) == dayOfMonth && getTime().get(Calendar.MONTH) == monthOfYear)tpd.setMinTime(new Timepoint(getTime().get(Calendar.HOUR_OF_DAY),getTime().get(Calendar.MINUTE)));
        cal.set(Calendar.YEAR,year);
        cal.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        cal.set(Calendar.MONTH,monthOfYear);
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    @OnClick(R.id.time)
    void timeSel(){
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                getTime().get(Calendar.YEAR), // Initial year selection
                getTime().get(Calendar.MONTH), // Initial month selection
                getTime().get(Calendar.DAY_OF_MONTH)// Inital day selection
        );
        dpd.setMinDate(getTime());
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    Calendar getTime(){
        Calendar selectedDate = ViewUtil.time();
        if (((RadioButton) findViewById(R.id.dost)).isChecked()){
            selectedDate.add(Calendar.MINUTE,80);
        }else{ selectedDate.add(Calendar.MINUTE,25); }
        return selectedDate;
    }

    private boolean checkTime(Calendar cal) {
        if ((cal.get(Calendar.HOUR_OF_DAY) >= 10 && cal.get(Calendar.DAY_OF_WEEK) > 1 && cal.get(Calendar.DAY_OF_WEEK) < 6) ||
                ((cal.get(Calendar.HOUR_OF_DAY) >= 10 || cal.get(Calendar.HOUR_OF_DAY) == 0) && (cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 6 || cal.get(Calendar.DAY_OF_WEEK) == 7)))
        {
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            time.setTag(dt.format(cal.getTimeInMillis()));
            time.setText(new SimpleDateFormat("HH:mm dd.MM.yyyy").format(cal.getTimeInMillis()));
        }else {
            new MaterialDialog.Builder(this)
                    .content("К сожелению мы не работаем в это время, поэтому не можем принять Ваш заказ\nРежим работы:\n" +
                            "Вс-Чт: с 10:00 до 01:00\n" +
                            "Пт-Сб: с 10:00 до 02:00")
                    .positiveText("Хорошо")
                    .onPositive((d,r)-> timeSel())
                    .cancelable(false)
                    .show();
        }
        return ((cal.get(Calendar.HOUR_OF_DAY) >= 10 && cal.get(Calendar.DAY_OF_WEEK) > 1 && cal.get(Calendar.DAY_OF_WEEK) < 6) ||
                ((cal.get(Calendar.HOUR_OF_DAY) >= 10 || cal.get(Calendar.HOUR_OF_DAY) == 0) && (cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 6 || cal.get(Calendar.DAY_OF_WEEK) == 7)));
    }

    private boolean newYear(Calendar cal) {
        if ((cal.get(Calendar.DAY_OF_MONTH) == 31 && cal.get(Calendar.MONTH) == 11
                && cal.get(Calendar.HOUR_OF_DAY) >= 20)
            || (cal.get(Calendar.DAY_OF_MONTH) == 1 && cal.get(Calendar.MONTH) == 0)){
            new MaterialDialog.Builder(this)
                    .content("К сожелению мы не работаем в это время, поэтому не можем принять Ваш заказ\nМы не работаем с 20:00 31 декабря по 10:00 2 января!")
                    .positiveText("Хорошо")
                    .onPositive((d,r)-> timeSel())
                    .cancelable(false)
                    .show();
        }else {
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            time.setTag(dt.format(cal.getTimeInMillis()));
            time.setText(new SimpleDateFormat("HH:mm dd.MM.yyyy").format(cal.getTimeInMillis()));
        }
        return ((cal.get(Calendar.DAY_OF_MONTH) == 31 && cal.get(Calendar.MONTH) == 11
                && cal.get(Calendar.HOUR_OF_DAY) >= 20)
                || (cal.get(Calendar.DAY_OF_MONTH) == 1 && cal.get(Calendar.MONTH) == 0));
    }

    public void onDelete(int pos) {
        mPresenter.deleteAdres(pos);
        rv.getLayoutParams().height = (mAdapter.getItemCount() - 1) * ViewUtil.dpToPx(40);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        position = data.getIntExtra("pos",0);
        mPresenter.loadDost();
    }

    @OnClick(R.id.new_adress)
    void new_adress(){ startActivityForResult(new Intent(this, NewAdressActivity.class),2); }

    @Override
    public int setlayout() { return R.layout.activity_devilary; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        init();
    }

    public static int COUNT = 1;
    @OnClick(R.id.plus)
    void plus(){
        ViewUtil.vibrate(DevilaryActivity.this);
        if (COUNT < 10) {
            COUNT = COUNT + 1;
            count.setText("x" + COUNT);
        }
    }

    @OnClick(R.id.minus)
    void minus(){
        ViewUtil.vibrate(DevilaryActivity.this);
        if (COUNT > 1) {
            COUNT = COUNT - 1;
            count.setText("x" + COUNT);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
