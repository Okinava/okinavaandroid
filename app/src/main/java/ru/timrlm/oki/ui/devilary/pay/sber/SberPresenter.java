package ru.timrlm.oki.ui.devilary.pay.sber;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.util.RxUtil;
import ru.timrlm.oki.util.ViewUtil;

class SberPresenter extends BasePresenter<SberMvpView> {
    private DataManager mDataManager;
    private Disposable disposable;

    @Inject
    public SberPresenter(DataManager dataManager) { this.mDataManager = dataManager; }


    public void loadUrl(CusOrder cusOrder) {
        disposable = mDataManager.getUrl(Integer.valueOf(cusOrder.getOrder().getPaymentItems().get(0).getSum()))
                .subscribe(response -> {
                            if (response.getFormUrl() != null) getMvpView().success(response.getFormUrl());
                            else getMvpView().error();
                        }
                ,throwable -> getMvpView().error());
    }

    @Override
    public void detachView() {
        super.detachView();
        RxUtil.dispose(disposable);
    }

    public void add(CusOrder cusOrder, String orId) {
        mDataManager.addOrderFinale(cusOrder)
                .subscribe(
                    r->{
                        if (!r){
                            add(cusOrder, orId);
                            Gson gs = new Gson();
                            Crashlytics.logException(new Exception("Код оплаты: " + orId + " ; " +gs.toJson(cusOrder)));
                            return;
                        }
                        mDataManager.clearOrder();
                        mDataManager.getPreferencesHelper().setPromo("");
                        mDataManager.clearHistory();
                        getMvpView().end();
                    },throwable -> getMvpView().error()
                );
    }
}
