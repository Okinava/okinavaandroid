package ru.timrlm.oki.ui.devilary.pay;

import android.util.Base64;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.local.RealmHelper;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.iko.OrderResponse;
import ru.timrlm.oki.data.model.iko.Restorans;
import ru.timrlm.oki.data.model.realm.lAdres;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.data.remote.ApiHelper;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.util.RxUtil;

@ConfigPersistent
class PayPresenter extends BasePresenter<PayMvpView> {
    private DataManager mDataManager;
    private List<Restorans.DeliveryTerminal> deliveryTerminals;

    @Inject
    public PayPresenter(DataManager mDataManager) { this.mDataManager = mDataManager; }

    public void load(int AdrePos, int type){
        lCustomer customer = mDataManager.findUser(mDataManager.getPreferencesHelper().getPhone());
        RealmHelper.OrderInfo orderInfo = mDataManager.getOrderInfo();
        getMvpView().setInfo(customer, orderInfo.sum, mDataManager.getOrders());
        if (type == 1) {
            mDataManager.getRestorans().map(r -> r.getDeliveryTerminals()).map(l -> {
                deliveryTerminals = l;
                return l.get(AdrePos);
            }).subscribe(t -> getMvpView().setAdres(t.getAddress()), t->{});
        } else {
            mDataManager.getAdres().map(l -> l.get(AdrePos))
                    .subscribe(a -> getMvpView().setAdres(a.getStreet() + " " + a.getHome() + (a.getApartment().length() > 0 ? ", кв. " + a.getApartment() : "")), t->{});
        }
    }

    private Disposable disposable;
    public void check(String time,int AdrePos, int type, int bonus, String comment, PayType payType){
        CusOrder cusOrder = createOrder(time,AdrePos,type,bonus,comment,payType);
        disposable = mDataManager.checkSales(cusOrder)
                .cache()
                .subscribe(r->{
                    if (r.equals("")) getMvpView().c_null();
                    else getMvpView().c_ok(r);
                });
    }

    public void add(String time,int AdrePos, int type, int bonus, String comment, PayType payType, String orId){
        CusOrder cusOrder = createOrder(time,AdrePos,type,bonus,comment,payType);
        mDataManager.addOrderFinale(cusOrder)
                .subscribe(
                r->{
                    if (!r && payType == PayType.nal){
                        getMvpView().error("Не удалось создать заказ");
                        return;
                    }else if (!r){
                        add(time,AdrePos,type,bonus,comment,payType,orId);
                        Gson gs = new Gson();
                        Crashlytics.logException(new Exception("Код оплаты: " + orId + " ; " +gs.toJson(cusOrder)));
                        return;
                    }
                    mDataManager.clearOrder();
                    mDataManager.getPreferencesHelper().setPromo("");
                    mDataManager.clearHistory();
                    getMvpView().end();
                },throwable -> getMvpView().error("Не удалось создать заказ")
        );
    }

    public CusOrder createOrder(String time,int AdrePos, int type, int bonus, String comment, PayType payType){
        lCustomer lc = mDataManager.findUser(mDataManager.getPreferencesHelper().getPhone());
        List<CusOrder.Item> listO = new ArrayList<>();
        int sum = 0;
        for (lOrder o : mDataManager.getOrders()){
            CusOrder.Item item = new CusOrder.Item();
            item.setAll(o.getlProduct().getId(),o.getlProduct().getName(),o.getCount(),o.getlProduct().getCode());
            if (o.getModificator().length()>0){
                List<CusOrder.Modifiers> modifiersList = new ArrayList<>();
                CusOrder.Modifiers modifiers = new CusOrder.Modifiers();
                modifiers.setId(o.getModificator());
                modifiers.setName(o.getNameM());
                modifiers.setAmount(1);
                modifiers.setGroupId(o.getlProduct().getModifaersId());
                modifiersList.add(modifiers);
                item.setModifiers(modifiersList);
            }
            item.setSum(o.getSum());
            listO.add(item);
            sum += o.getSum();
        }
        CusOrder cusOrder = new CusOrder();
        cusOrder.setOrganization(ApiHelper.orgId);
        CusOrder.Customer customer = new CusOrder.Customer();
        customer.setAll(lc.getId(),lc.getName(),lc.getPhone());
        cusOrder.setCustomer(customer);
        CusOrder.Order order = new CusOrder.Order();
        order.setPhone(lc.getPhone());
        order.setIsSelfService(Boolean.valueOf(type == 1).toString());
        order.setComment(comment);
//        order.setComment("ТЕСТОВЫЙ ЗАКАЗ НЕ ОБРАБАТЫВАТЬ. ТЕСТ МОБИЛЬНОГО ПРИЛОЖЕНИЯ");
        order.setOrderTypeId(type == 1 ? "4229b272-a67e-44cd-8f71-88d1e4373296" : "bb6f5cee-1fb5-45a2-bf6f-aecd0a2e98d8" );
        order.setDate(time);
        order.setItems(listO);
        if ((type == 1) && (deliveryTerminals != null)) {
            cusOrder.setDeliveryTerminalId(deliveryTerminals.get(AdrePos).getDeliveryTerminalId());
        }
        if (type == 0){
            lAdres ad = mDataManager.getAdresNoObs().get(AdrePos);
            CusOrder.Address address = new CusOrder.Address();
            address.setAll("Казань",ad.getStreet(),ad.getHome(),ad.getEntrance(),ad.getApartment(),ad.getFloor(),ad.getDoorphone(),ad.getComment());
            order.setAddress(address);
        }

        List<CusOrder.PaymentItem> paymentItems = new ArrayList<>();

        CusOrder.PaymentItem nal = new CusOrder.PaymentItem();
        CusOrder.PaymentType pNal = new CusOrder.PaymentType();
        nal.setSum(String.valueOf(sum - bonus));
        nal.setIsProcessedExternally(payType == PayType.card_online);
        switch (payType) {
            case nal:
                pNal.setName("Наличные");
                pNal.setCode("CASH");
                pNal.setId("09322f46-578a-d210-add7-eec222a08871");
                break;
            case card_cur:
                pNal.setName("Карта");
                pNal.setCode("ZCARD");
                pNal.setId("371e1133-9ac2-41f1-aea5-d5791c658733");
                break;
            case card_online:
                pNal.setName("МП Онлайн");
                pNal.setCode("MCARD");
                pNal.setId("ed4ca935-3e09-4f89-bbe9-8aad1bea7d62");
                break;
        }
        nal.setPaymentType(pNal);
        paymentItems.add(nal);
        if (bonus > 0) {
            CusOrder.PaymentItem bon = new CusOrder.PaymentItem();
            bon.setIsProcessedExternally(false);
            bon.setSum(String.valueOf(bonus));
            bon.setAdditionalData("{\"searchScope\": \"PHONE\", \"credential\": \"" + lc.getPhone() + "\"}");
            CusOrder.PaymentType pBon = new CusOrder.PaymentType();
            pBon.setName("iikoCard5");
            pBon.setCode("INET");
            pBon.setId("e681c306-a257-4071-ac5f-23e3c573b968");
            bon.setPaymentType(pBon);
            paymentItems.add(bon);
        }
        order.setPaymentItems(paymentItems);
        cusOrder.setOrder(order);
        cusOrder.setCoupon(mDataManager.getPreferencesHelper().getPromo());
        return cusOrder;
    }


    public void paySber(String time, int pos, int type, int progress, String comment) {
        getMvpView().openSber(createOrder(time,pos,type,progress,comment,PayType.card_online));
    }

    public void googlePay(int sum, String time, int pos, int type, int progress, String comment, String json) {
        try {
            Object paymentMethodData = (new JSONObject(json)).get("paymentMethodData");
            Object tokenizationData = (new JSONObject(paymentMethodData.toString())).get("tokenizationData");
            String token = (new JSONObject(tokenizationData.toString())).getString("token");
            mDataManager.googlePay(toBase64(token), sum - progress)
                    .subscribe(r -> {
                        if (!r.success || r.data.acsUrl != null){
                            getMvpView().error("Нам не удалось совершить оплату через GooglePay. Вы можете выбрать другой тип оплаты");
                            return;
                        }
                        add(time,pos,type,progress,comment, PayType.card_online,String.valueOf(DataManager.ordId));
                    }, t -> {
                        getMvpView().error("Нам не удалось совершить оплату через GooglePay. Вы можете выбрать другой тип оплаты");
                    });
        }catch (Exception e){
            getMvpView().error("Нам не удалось совершить оплату через GooglePay. Вы можете выбрать другой тип оплаты");
        }
    }

    public static String toBase64(String message) {
        byte[] data;
        try {
            data = message.getBytes("UTF-8");
            String base64Sms = Base64.encodeToString(data, Base64.DEFAULT);
            return base64Sms;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    enum PayType{
        nal, card_cur, card_online;
    }

    @Override
    public void detachView() {
        RxUtil.dispose(disposable);
        super.detachView();
    }
}
