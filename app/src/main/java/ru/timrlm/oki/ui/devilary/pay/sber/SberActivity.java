package ru.timrlm.oki.ui.devilary.pay.sber;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.ui.devilary.pay.PayActivity;
import ru.timrlm.oki.ui.main.MainActivity;

public class SberActivity extends BaseSwipeBackActivity implements SberMvpView {
    public static CusOrder cusOrder;
    @Inject SberPresenter mPresenter;
    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.web) WebView mWeb;

    @Override
    public void error() {
        hideProgres();
        new MaterialDialog.Builder(this)
                .title("Ошибка")
                .content("Не получилось совершить оплату. Повторите или выберите другой тип оплаты!")
                .positiveText("Хорошо")
                .cancelable(false)
                .onPositive((d,v)-> finish())
                .show();
    }

    @Override
    public void success(String formUrl) {
        hideProgres();
        mWeb.loadUrl(formUrl);
    }

    @Override
    public void end() {
        hideProgres();
        new MaterialDialog.Builder(this)
                .title("Результат")
                .content("Заказ успешно создан")
                .positiveText("ОК")
                .onPositive((d,w)->{
                    Intent intent = new Intent(SberActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                })
                .show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        mTitle.setText("Оплата картой онлайн");
        WebViewClient client = new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (url.contains("smart-resto-success")){
                    showProgres();
                    mWeb.setVisibility(View.INVISIBLE);
                    mPresenter.add(cusOrder, String.valueOf(DataManager.ordId));
                } else if (url.contains("smart-resto-error")){
                    mWeb.setVisibility(View.INVISIBLE);
                    error();
                } else{
                    super.onPageStarted(view, url, favicon);
                }
            }
        };
        mWeb.setWebViewClient(client);
        mWeb.getSettings().setJavaScriptEnabled(true);
        showProgres();
        mPresenter.loadUrl(cusOrder);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public int setlayout() { return R.layout.activity_web; }
}
