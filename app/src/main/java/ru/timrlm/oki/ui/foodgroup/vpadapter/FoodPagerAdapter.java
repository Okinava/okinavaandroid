package ru.timrlm.oki.ui.foodgroup.vpadapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import ru.timrlm.oki.data.model.realm.lGroup;
import ru.timrlm.oki.ui.foodgroup.FoodGroupFragment;
import ru.timrlm.oki.ui.productlist.ProductListFragment;

public class FoodPagerAdapter extends FragmentPagerAdapter {

    private List<lGroup> mGroups;

    public FoodPagerAdapter(FragmentManager fm, List<lGroup> groups) {
        super(fm);
        mGroups = groups;
    }

    @Override
    public Fragment getItem(int position) { return ProductListFragment.newInstance(mGroups.get(position)); }

    @Override
    public int getCount() { return mGroups.size(); }

    @Override
    public CharSequence getPageTitle(int position) { return mGroups.get(position).getName(); }
}
