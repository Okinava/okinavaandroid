package ru.timrlm.oki.ui.auto;

import ru.timrlm.oki.ui.base.MvpView;

interface AutoMvpView extends MvpView {
    void success(int code);
    void error();
    void ban();
}
