package ru.timrlm.oki.ui.product.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.ui.order.OrderActivity;
import ru.timrlm.oki.ui.order.search.SelectAdapter;
import ru.timrlm.oki.ui.product.ProductActivity;
import ru.timrlm.oki.util.ViewUtil;

/**
 * Created by timerlan on 09.04.2018.
 */

public class ProductFragment extends Fragment implements ProductFragmentMvpView, SelectAdapter.OnSelectListener {
    @Inject ProductFragmentPresenter mPresenter;
    public lProduct product;
    private int count = 1;
    private View rootView;
    private List<lProduct> products;
    private SelectAdapter mAdapter = new SelectAdapter(new ArrayList<>(),0, R.drawable.empty);
    @BindView(R.id.count) TextView mCountView;
    @BindView(R.id.price) TextView mPriceView;
    @BindView(R.id.minus) Button mMinusView;
    @BindView(R.id.minusCard) View mMinusCardView;
    @BindView(R.id.rv) RecyclerView rv;
    @BindView(R.id.star) ImageButton start;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this,rootView);
        ((ProductActivity) getActivity()).activityComponent().inject(this);
        mPresenter.attachView(this);
        setProduct();
        start.setImageDrawable(getResources().getDrawable(mPresenter.isStar(product.getId()) ? R.drawable.star_menu : R.drawable.start_off));

        return rootView;
    }


    public static ProductFragment newInstance(lProduct product){
        ProductFragment productFragment = new ProductFragment();
        productFragment.product = product;
        return productFragment;
    }

    private void setProduct(){
        ((TextView) rootView.findViewById(R.id.name)).setText(product.getName());
        Glide.with(this)
                .load(product.getImg())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_refresh)
                        .error(R.drawable.ic_photo)
                )
                .into(((ImageView) rootView.findViewById(R.id.img)));
        mPriceView.setText(product.getPrice()+" " + ViewUtil.R);
        ((TextView) rootView.findViewById(R.id.cena)).setText(product.getPrice()+" " + ViewUtil.R);
        if (product.getWeight() >= 1 ){
            ((TextView) rootView.findViewById(R.id.weight)).setText(getString(R.string.weight,product.getWeight()));
        }else{
            Double v = product.getWeight() * 1000;
            ((TextView) rootView.findViewById(R.id.weight)).setText( v.intValue() + " гр.");
        }
        ((TextView) rootView.findViewById(R.id.desc)).setText(product.getDescription());
        ((TextView) rootView.findViewById(R.id.one_prot)).setText(getString(R.string.floa,product.getFiberAmount()));
        ((TextView) rootView.findViewById(R.id.one_fats)).setText(getString(R.string.floa,product.getFatAmount()));
        ((TextView) rootView.findViewById(R.id.one_carb)).setText(getString(R.string.floa,product.getCarbohydrateAmount()));
        ((TextView) rootView.findViewById(R.id.one_cal)).setText(getString(R.string.floa,product.getEnergyAmount()));
        if (product.getModifaers().length()>0){
            rootView.findViewById(R.id.vid).setVisibility(View.VISIBLE);
            rv.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
            List<String> guids = Arrays.asList(product.getModifaers().split(";;;"));
            rv.getLayoutParams().height =  guids.size() * ViewUtil.dpToPx(40);
            rv.setAdapter(mAdapter);
            mAdapter.setOnSelectListener(this);
            mPresenter.setModifaers(guids);
        }
    }

    @Override
    public void setModifaers(List<String> modifaers,List<lProduct> products) {
        mAdapter.update(modifaers); this.products = products;
    }

    @Override
    public void onSelect(String text) {
        lProduct product = products.get(mAdapter.getPos());
        ((TextView) rootView.findViewById(R.id.cena)).setText((product.getPrice() + this.product.getPrice()) +" "+ ViewUtil.R);
        mPriceView.setText(( (count * ( products.get(mAdapter.getPos()).getPrice() + this.product.getPrice() ) )+ " " + ViewUtil.R));
    }

    @OnClick(R.id.star)
    void start(){
        ViewUtil.vibrate(rootView.getContext());
        start.setImageDrawable(getResources().getDrawable(!mPresenter.isStar(product.getId()) ? R.drawable.star_menu : R.drawable.start_off));
        mPresenter.chageStar(product.getId());
    }

    @OnClick(R.id.plus)
    void plus(){
        ViewUtil.vibrate(rootView.getContext());
        count+=1;
        mPriceView.setText(( count * ( (products != null ? products.get(mAdapter.getPos()).getPrice() : 0) + product.getPrice() )+" " + ViewUtil.R));
        mCountView.setText(count+"");
        mMinusView.setEnabled(true);
        mMinusView.setTextColor(getResources().getColor(R.color.black));
        mMinusCardView.setBackgroundResource(R.drawable.btn_back);
    }

    @OnClick(R.id.minus)
    void minus(){
        ViewUtil.vibrate(rootView.getContext());
        count -=1;
        mPriceView.setText(( count * ( (products != null ? products.get(mAdapter.getPos()).getPrice() : 0) + product.getPrice() )+" " + ViewUtil.R));
        mCountView.setText(count+"");
        if (count == 1){
            mMinusView.setEnabled(false);
            mMinusView.setTextColor(getResources().getColor(R.color.lightgray));
            mMinusCardView.setBackgroundResource(R.drawable.btn_gray);
        }
    }

    @OnClick(R.id.zak)
    void zak(){
        ViewUtil.vibrate(rootView.getContext());
        mPresenter.addZak(count,product,
                products != null ? products.get(mAdapter.getPos()).getId() : "",
                products != null ? products.get(mAdapter.getPos()).getName() : "" ,(int) (products != null ? products.get(mAdapter.getPos()).getPrice() : 0),
                this);
    }

    @Override
    public void resul() {
        Snackbar.make(rootView.findViewById(R.id.CoordinatorLayout), product.getName()+ (products != null ? "("+products.get(mAdapter.getPos()).getName()+")" : "") +" добавлен в корзину", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void errors(String s) {
        new MaterialDialog.Builder(getContext())
                .title("Ой!")
                .content(s)
                .positiveText("Хорошо")
                .show();
    }

    @Override
    public void clear() {
        count = 1;
        mPriceView.setText(( count * ( (products != null ? products.get(mAdapter.getPos()).getPrice() : 0) + product.getPrice() )+" " + ViewUtil.R));
        mCountView.setText(count+"");
        mMinusView.setEnabled(false);
        mMinusView.setTextColor(getResources().getColor(R.color.lightgray));
        mMinusCardView.setBackgroundResource(R.drawable.btn_gray);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.detachView();
    }
}
