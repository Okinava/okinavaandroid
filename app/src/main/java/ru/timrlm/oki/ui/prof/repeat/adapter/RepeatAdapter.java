package ru.timrlm.oki.ui.prof.repeat.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.iko.CusOrder;

public class RepeatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private List<CusOrder.Item> items = new ArrayList<>();
    private Context mContext;

    public void setItems(List<CusOrder.Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new RepeatView(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_his,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((TextView)holder.itemView.findViewById(R.id.name)).setText(items.get(position).getName());
        ((TextView)holder.itemView.findViewById(R.id.count)).setText("x"+items.get(position).getAmount());
        ((TextView)holder.itemView.findViewById(R.id.price)).setText( mContext.getString(R.string.price, items.get(position).getSum()/items.get(position).getAmount()) );
    }

    @Override
    public int getItemCount() { return items.size(); }

    class RepeatView extends RecyclerView.ViewHolder{
        RepeatView(View view){ super(view); }
    }
}
