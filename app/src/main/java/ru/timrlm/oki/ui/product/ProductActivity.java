package ru.timrlm.oki.ui.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.ui.order.OrderActivity;
import ru.timrlm.oki.ui.product.pager.MyFragmentPagerAdapter;

/**
 * Created by timerlan on 09.04.2018.
 */

public class ProductActivity extends BaseSwipeBackActivity implements ProductMvpView{
    public static lProduct product;
    public static List<lProduct> products;
    @Inject ProductPresenter mPresenter;
    @BindView(R.id.pager) ViewPager pv;

    @Override
    public void setProducts(List<lProduct> products, int position) {
        pv.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager(),products));
        pv.setCurrentItem(position);
    }

    @Override
    public int setlayout() { return R.layout.activity_product; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        if ( products == null) mPresenter.load(product); else {
            Observable.fromArray(products)
                    .concatMapIterable(l->l)
                    .filter((p)->p.getParentGroup() != null)
                    .toList()
                    .subscribe((p)->{
                        products = p;
                        for (int i=0; i< products.size();i++)
                            if (product.getId().equals(products.get(i).getId()))
                                setProducts(products,i);
                    });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void setCountZakaz(int count) {
        findViewById(R.id.circle).setVisibility( count == 0 ? View.GONE : View.VISIBLE);
        findViewById(R.id.shop_card).setVisibility( count == 0 ? View.GONE : View.VISIBLE);
    }

    @OnClick({R.id.shop_top,R.id.shop_card})
    void shop(){
        startActivity(new Intent(ProductActivity.this, OrderActivity.class));
    }

    @OnClick({R.id.back_card,R.id.back_top})
    void chat(){ finish(); }
}
