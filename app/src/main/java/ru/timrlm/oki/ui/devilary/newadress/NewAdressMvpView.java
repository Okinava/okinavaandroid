package ru.timrlm.oki.ui.devilary.newadress;

import java.util.List;

import ru.timrlm.oki.ui.base.MvpView;

interface NewAdressMvpView extends MvpView {
    void showStreets(List<String> streets);
    void success(int pos);
    void error();
    void error(String mes);

}
