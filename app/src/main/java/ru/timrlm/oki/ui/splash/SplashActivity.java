package ru.timrlm.oki.ui.splash;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.R;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.ui.splash.tutorial.TutorialActivity;


public class SplashActivity extends BaseActivity implements SplashMvpView {
    @Inject SplashPresenter mPresenter;
    private Disposable mSubscribe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
    }

    @Override
    public void noconect() {
        new MaterialDialog.Builder(this)
                .title("Ой!")
                .content("Отсутствует интернет соединение, проверьте подключение. Для первого запуска необходим интернет.")
                .positiveText("Хорошо")
                .dismissListener((d)-> mPresenter.reload())
                .show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.checkVersion();
    }

    MaterialDialog dialog;
    @Override
    public void downloadNew() {
        dialog = new MaterialDialog.Builder(this)
                .title("Ой!")
                .content("К сожалению ваша версия приложения устарела, необходимо обновить приложение")
                .positiveText("Хорошо")
                .dismissListener((d)-> {
                    Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://play.google.com/store/apps/details?id=ru.timrlm.oki"));
                    startActivity(i);
                    dialog = null;
                })
                .show();
    }

    @Override
    public void startFood() {
        if (mSubscribe == null && dialog == null)
        mSubscribe = Observable.timer(1, TimeUnit.SECONDS).subscribe((t) -> mPresenter.open());
    }

    @Override
    public void startMain() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class)); finish();
    }

    @Override
    public void startTutorial() {
        startActivity(new Intent(SplashActivity.this, TutorialActivity.class)); finish();
    }

    @Override
    protected void onStop() {
        if (mSubscribe != null) mSubscribe.dispose();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public int setlayout() { return R.layout.activity_splash; }
}
