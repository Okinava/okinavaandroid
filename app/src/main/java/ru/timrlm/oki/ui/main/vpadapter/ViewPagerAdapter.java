package ru.timrlm.oki.ui.main.vpadapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ru.timrlm.oki.ui.contact.ContactFragment;
import ru.timrlm.oki.ui.foodgroup.FoodGroupFragment;
import ru.timrlm.oki.ui.prof.ProfFragment;
import ru.timrlm.oki.ui.sale.SaleFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:  return new FoodGroupFragment();
            case 1:  return new SaleFragment();
            case 2:  return new ProfFragment();
            case 3:  return new ContactFragment();
        }
        return new Fragment();
    }

    @Override
    public int getCount() { return 4; }
}
