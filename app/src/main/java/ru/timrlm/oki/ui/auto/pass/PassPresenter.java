package ru.timrlm.oki.ui.auto.pass;

import android.util.Log;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;

@ConfigPersistent
class PassPresenter extends BasePresenter<PassMvpView>{
    private DataManager mDataManager;

    @Inject
    public PassPresenter(DataManager mDataManager) { this.mDataManager = mDataManager; }

    private Disposable disposable;
    public void customerAuto(String number){
        disposable = mDataManager.checkUser(number, true).subscribe(s -> {
            if (getMvpView() != null) getMvpView().success();
        },t -> { if (getMvpView() != null) getMvpView().error(); });
    }

    @Override
    public void detachView() {
        if (disposable != null) disposable.dispose();
        super.detachView();
    }
}
