package ru.timrlm.oki.ui.foodgroup;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.design.widget.TabLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.Promo;
import ru.timrlm.oki.data.model.realm.lGroup;
import ru.timrlm.oki.ui.foodgroup.adapter.PromoAdapter;
import ru.timrlm.oki.ui.foodgroup.vpadapter.FoodPagerAdapter;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.ui.productlist.adapter.ProductAdapter;
import ru.timrlm.oki.util.AppBarStateChangeListener;
import ru.timrlm.oki.util.ViewUtil;

/**
 * Created by timerlan on 28.03.2018.
 */

public class FoodGroupFragment extends Fragment implements FoodGroupMvpView {
    @Inject FoodGroupPresenter mPresenter;
    @BindView(R.id.appbar) AppBarLayout appBarLayout;
    @BindView(R.id.tab) TabLayout mTab;
    @BindView(R.id.bounus_count) TextView bonusCountView;
    @BindView(R.id.bonus_text) TextView bonusTextView;
    @BindView(R.id.tab_vp) ViewPager vp;
    @BindView(R.id.title) TextView title;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_foodgroup, container,false);
        ButterKnife.bind(this,view);
        ((MainActivity) getActivity()).activityComponent().inject(this);
        mPresenter.attachView(this);

        mPresenter.load();

        appBarLayout.addOnOffsetChangedListener((v,o)->{
            if (ViewUtil.pxToDp(o) < -126){
                title.setVisibility(View.VISIBLE); mPresenter.sendUISearch(true);
            }else{
                title.setVisibility(View.GONE);mPresenter.sendUISearch(false);
            }
        });
        return view;
    }

    @OnClick(R.id.title)
    public void scroll(){ mPresenter.needScroll();}


    private String promo;
    @Override
    public void setPromo(String promo) {
        this.promo = promo;
    }

    @OnClick(R.id.promo)
    void promo(){
        new MaterialDialog.Builder(getActivity())
                .content("Промокод")
                .input("Введите промокод", promo, (d,t) -> {
                    mPresenter.setPromo(t.toString());
                    promo = t.toString();
                })
                .positiveText("Применить")
                .show();
    }

    @Override
    public void setBonus(int bonus, String text) {
        bonusCountView.setText(bonus+"");
        bonusTextView.setText(text);
    }

    @Override
    public void setGroups(List<lGroup> groups) {
        vp.setAdapter(new FoodPagerAdapter(getChildFragmentManager(), groups));
        mTab.setupWithViewPager(vp);
    }

    Boolean reinit = false;
    @Override
    public void reinit() {
        if (active){
            Intent intent = getActivity().getIntent();
            getActivity().finish();
            startActivity(intent);
        }else reinit = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (reinit) {
            Intent intent = getActivity().getIntent();
            getActivity().finish();
            startActivity(intent);
        }
    }

    private boolean active;
    @Override public void onStart() { super.onStart(); active = true; }
    @Override public void onPause() { super.onPause(); active = false; }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.detachView();
    }
}
