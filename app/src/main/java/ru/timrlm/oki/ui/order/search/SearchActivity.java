package ru.timrlm.oki.ui.order.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.widget.EditText;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.Observable;
import ru.timrlm.oki.R;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;

import static butterknife.OnTextChanged.Callback.AFTER_TEXT_CHANGED;

public class SearchActivity extends BaseSwipeBackActivity implements SelectAdapter.OnSelectListener{
    public static List<String> list;
    private SelectAdapter adapter = new SelectAdapter(list,-1, R.drawable.order_adress);
    @BindView(R.id.rv) RecyclerView rv;
    @BindView(R.id.search) EditText search;

    @OnTextChanged(value = R.id.search, callback = AFTER_TEXT_CHANGED)
    public void afterTextChanged(Editable s) {
        if (s.length()>0){
            Observable.fromArray(list)
                    .flatMapIterable(l->l)
                    .filter(l->l.toLowerCase().contains(s.toString().toLowerCase().trim()))
                    .toList()
                    .subscribe(list-> adapter.update(list));
        }else{
            adapter.update(list);
        }
    }

    @Override
    public void onSelect(String text) {
        Intent intent = new Intent();
        intent.putExtra("text", text);
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.clear)
    void clear(){ search.setText(""); }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        rv.setLayoutManager(new LinearLayoutManager(this));
//        rv.addItemDecoration(new SelectAdapter.SimpleDividerItemDecoration(this));
        adapter.setOnSelectListener(this);
        rv.setAdapter(adapter);
    }

    @Override
    public int setlayout() { return R.layout.activity_search; }
}
