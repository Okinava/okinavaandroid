package ru.timrlm.oki.ui.foodgroup;

import java.util.List;

import ru.timrlm.oki.data.model.Promo;
import ru.timrlm.oki.data.model.realm.lGroup;
import ru.timrlm.oki.ui.base.MvpView;

/**
 * Created by timerlan on 28.03.2018.
 */

public interface FoodGroupMvpView extends MvpView {
    void setGroups(List<lGroup> groups);
    void setBonus(int bonus, String text);
    void reinit();

    void setPromo(String promo);
}
