package ru.timrlm.oki.ui.devilary.pay;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.realm.lCustomer;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.ui.devilary.pay.adapter.OrderInPayAdapter;
import ru.timrlm.oki.ui.devilary.pay.info.InfoActivity;
import ru.timrlm.oki.ui.devilary.pay.s3d.S3DActivity;
import ru.timrlm.oki.ui.devilary.pay.sber.SberActivity;
import ru.timrlm.oki.ui.main.MainActivity;
import ru.timrlm.oki.util.ViewUtil;

public class PayActivity extends BaseSwipeBackActivity implements PayMvpView, SeekBar.OnSeekBarChangeListener {
    @Inject PayPresenter mPresenter;
    private PaymentsClient googlePaymentsClient;
    public static int pos;
    public static String time;
    public static String comment;
    public static int type = 0; // 0 - dostavka
    public int sum;
    @BindView(R.id.sum) TextView sumView;
    @BindView(R.id.bonus_count) EditText bonusCount;
    @BindView(R.id.bonus_text) TextView bonus_text;
    @BindView(R.id.seek) SeekBar bonusSeek;
    @BindView(R.id.rv) RecyclerView rv;
    @BindView(R.id.card_sales) View card_sales;
    @BindView(R.id.sales) TextView sales;
    @BindView(R.id.google_pay) View card_google;

    @Override
    public void setInfo(lCustomer customer, int sum, List<lOrder> orders) {
        this.sum = sum;
        ((TextView)findViewById(R.id.name)).setText(customer.getName() != null ? customer.getName() : "Нет имени");
        ((TextView)findViewById(R.id.phone)).setText(customer.getPhone());
        ((TextView)findViewById(R.id.time)).setText("Доставить до " + time.substring(11,16) + " " + time.substring(8,10) + "."+time.substring(5,7)+"."+time.substring(0,4));
        findViewById(R.id.no_bonus_view).setVisibility(customer.getBalance() == 0 ? View.VISIBLE : View.GONE);
        findViewById(R.id.bonus_view).setVisibility(customer.getBalance() == 0 ? View.GONE : View.VISIBLE);
        if (customer.getBalance() != 0){
            bonusCount.setText(getString(R.string.bonus_count,customer.getBalance()));
            bonusSeek.setMax(customer.getBalance() > sum/2 ? sum/2 : customer.getBalance());
            bonusSeek.setMax(bonusSeek.getMax() > 500 ? 500 : bonusSeek.getMax());
            bonusSeek.setProgress(bonusSeek.getMax());
            bonusSeek.setOnSeekBarChangeListener(this);
            onProgressChanged(bonusSeek,bonusSeek.getMax(),true);
        }else {
            sumView.setText(sum + " " + ViewUtil.R);
            bonus_text.setText("0 " + ViewUtil.R);
        }
        rv.setLayoutManager(new LinearLayoutManager(PayActivity.this));
        rv.setAdapter(new OrderInPayAdapter(orders));
    }

    @Override
    public void setAdres(String adres) { ((TextView)findViewById(R.id.adres)).setText(adres); }

    @OnClick(R.id.nal)
    void nalichka(){
        showProgres();
        mPresenter.add(time,pos,type,bonusSeek.getProgress(),comment, PayPresenter.PayType.nal,"nal");
    }

    @OnClick(R.id.info)
    void info(){
        startActivity(new Intent(PayActivity.this, InfoActivity.class));
    }

    @OnClick(R.id.google_pay)
    void cardCur(){
        try {
            int su = sum - bonusSeek.getProgress();
            PaymentDataRequest request = PaymentDataRequest.fromJson(getPaymentDataRequest( su + ".00").toString());
            if (request != null) {
                AutoResolveHelper.resolveTask(googlePaymentsClient.loadPaymentData(request), PayActivity.this, 333);
            }
        }catch (Exception e){}
    }

    @OnClick(R.id.card_online)
    void cardOnline(){
        showProgres();
        mPresenter.paySber(time,pos,type,bonusSeek.getProgress(),comment);
    }

    @Override
    public void openSber(CusOrder order) {
        hideProgres();
        SberActivity.cusOrder = order;
        startActivity(new Intent(PayActivity.this,SberActivity.class));
    }

    @Override
    public void end() {
        hideProgres();
        new MaterialDialog.Builder(this)
                .title("Результат")
                .content("Заказ успешно создан")
                .positiveText("ОК")
                .onPositive((d,w)->{
                    Intent intent = new Intent(PayActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                })
                .show();
    }

    @Override
    public void error(String text) {
        hideProgres();
        showError(text);
    }

    @Override
    public void c_ok(String r) {
        hideProgres();
        sales.setText(r);
        card_sales.setVisibility(View.VISIBLE);
    }

    @Override
    public void c_null() {
        hideProgres();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtil.hideKeyboard(this);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        mPresenter.load(pos,type);
        showProgres();
        mPresenter.check(time,pos,type,bonusSeek.getProgress(),comment,PayPresenter.PayType.card_cur);
        googlePaymentsClient = Wallet.getPaymentsClient( this,
                new Wallet.WalletOptions.Builder()
                        .setEnvironment(WalletConstants.ENVIRONMENT_PRODUCTION)
                        .build());
        try {
            IsReadyToPayRequest request = IsReadyToPayRequest.fromJson(getIsReadyToPayRequest().toString());
            Task<Boolean> task = googlePaymentsClient.isReadyToPay(request);
            task.addOnCompleteListener((t) ->{
                try {
                    boolean result = t.getResult(ApiException.class);
                    if (result) { card_google.setVisibility(View.VISIBLE); }
                } catch (ApiException e) { }
            });
        } catch (JSONException e) {  e.printStackTrace(); }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        bonusCount.setText(progress + "");
        bonus_text.setText(progress + " " + ViewUtil.R);
        sumView.setText((sum - progress) + " " + ViewUtil.R);
    }
    @Override public void onStartTrackingTouch(SeekBar seekBar) { }
    @Override public void onStopTrackingTouch(SeekBar seekBar) { }
    @Override protected void onDestroy() { super.onDestroy();mPresenter.detachView(); }
    @Override public int setlayout() { return R.layout.activity_pay; }

    private static JSONObject getBaseRequest() throws JSONException {
        return new JSONObject()
                .put("apiVersion", 2)
                .put("apiVersionMinor", 0);
    }

    private static JSONObject getTokenizationSpecification() throws JSONException {
        JSONObject tokenizationSpecification = new JSONObject();
        tokenizationSpecification.put("type", "PAYMENT_GATEWAY");
        tokenizationSpecification.put(
                "parameters",
                new JSONObject()
                        .put("gateway", "sberbank")
                        .put("gatewayMerchantId", "okinavakzn"));
        return tokenizationSpecification;
    }

    private static JSONArray getAllowedCardNetworks() {
        return new JSONArray()
                .put("MASTERCARD")
                .put("VISA");
    }

    private static JSONArray getAllowedCardAuthMethods() {
        return new JSONArray()
                .put("PAN_ONLY")
                .put("CRYPTOGRAM_3DS");
    }

    private static JSONObject getBaseCardPaymentMethod() throws JSONException {
        JSONObject cardPaymentMethod = new JSONObject();
        cardPaymentMethod.put("type", "CARD");
        cardPaymentMethod.put(
                "parameters",
                new JSONObject()
                        .put("allowedAuthMethods", getAllowedCardAuthMethods())
                        .put("allowedCardNetworks", getAllowedCardNetworks()));
        return cardPaymentMethod;
    }

    private static JSONObject getCardPaymentMethod() throws JSONException {
        JSONObject cardPaymentMethod = getBaseCardPaymentMethod();
        cardPaymentMethod.put("tokenizationSpecification", getTokenizationSpecification());
        return cardPaymentMethod;
    }

    public static JSONObject getIsReadyToPayRequest() throws JSONException {
        JSONObject isReadyToPayRequest = getBaseRequest();
        isReadyToPayRequest.put("allowedPaymentMethods", new JSONArray() .put(getBaseCardPaymentMethod()));
        return isReadyToPayRequest;
    }

    private static JSONObject getTransactionInfo(String totalPrice) throws JSONException {
        JSONObject transactionInfo = new JSONObject();
        transactionInfo.put("totalPrice", totalPrice);
        transactionInfo.put("totalPriceStatus", "FINAL");
        transactionInfo.put("currencyCode", "RUB");
        return transactionInfo;
    }


    private static JSONObject getMerchantInfo() throws JSONException {
        return new JSONObject().put("merchantName", "Окинава Казань");
    }

    public static JSONObject getPaymentDataRequest(String totalPrice) throws JSONException {
        JSONObject paymentDataRequest = getBaseRequest();
        paymentDataRequest.put(
                "allowedPaymentMethods",
                new JSONArray()
                        .put(getCardPaymentMethod()));
        paymentDataRequest.put("transactionInfo", getTransactionInfo(totalPrice));
        paymentDataRequest.put("merchantInfo", getMerchantInfo());

        return paymentDataRequest;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != 333) return;
        switch (resultCode) {
            case Activity.RESULT_OK:
                if (data == null) return;
//                super.showError("К сожалению данный тип оплаты не доступен. Выберите другой тип оплаты((");
                PaymentData paymentData = PaymentData.getFromIntent(data);
                String json = paymentData.toJson();
                showProgres();
                mPresenter.googlePay(sum,time,pos,type,bonusSeek.getProgress(),comment, json);
                break;
            case Activity.RESULT_CANCELED: break;
            case AutoResolveHelper.RESULT_ERROR:
                if (data == null) return;
                Status status = AutoResolveHelper.getStatusFromIntent(data);
                super.showError("Не удалось совершить оплату");
            break;
        }
    }

    @Override
    public void sec_3d(String acsUrl, String paReq, String termUrl, String orderId) {
        hideProgres();
        S3DActivity.acsUrl = acsUrl;
        S3DActivity.paReq = paReq;
        S3DActivity.termUrl = termUrl;
        S3DActivity.orderId = orderId;
        Intent browserIntent = new Intent(PayActivity.this, S3DActivity.class);
        startActivity(browserIntent);
    }
}
