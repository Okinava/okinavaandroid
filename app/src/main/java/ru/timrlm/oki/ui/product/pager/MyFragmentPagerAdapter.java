package ru.timrlm.oki.ui.product.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.product.fragment.ProductFragment;


/**
 * Created by timerlan on 20.03.2018.
 */

public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<lProduct> mProducts;

    public MyFragmentPagerAdapter(FragmentManager fm, List<lProduct> products) {
        super(fm);
        mProducts = products;
    }

    @Override
    public Fragment getItem(int position) { return ProductFragment.newInstance(mProducts.get(position)); }

    @Override
    public int getCount() { return mProducts.size(); }

}
