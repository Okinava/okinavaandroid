package ru.timrlm.oki.ui.foodgroup.search;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.Observable;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.ui.foodgroup.search.adapter.SearchFoodAdapter;
import ru.timrlm.oki.ui.product.ProductActivity;
import ru.timrlm.oki.ui.productlist.adapter.ProductAdapter;

import static butterknife.OnTextChanged.Callback.AFTER_TEXT_CHANGED;

public class SearchFoodActivity extends BaseSwipeBackActivity implements SearchFoodMvpView, SearchFoodAdapter.OnClickListener {
    @Inject SearchFoodPresenter mPresenter;
    @BindView(R.id.rv) RecyclerView rv;
    @BindView(R.id.search) EditText search;
    private SearchFoodAdapter mAdapter = new SearchFoodAdapter(new ArrayList<lProduct>(), this);
    private List<lProduct> products;

    @OnTextChanged(value = R.id.search, callback = AFTER_TEXT_CHANGED)
    public void afterTextChanged(Editable s) {
        if (search.getText().toString().length()>0){ mPresenter.getProductsByName(search.getText().toString());
        }else{ mPresenter.getAllProducts(); }
    }

    @Override
    public void setList(List<lProduct> products) { this.products = products; mAdapter.upd(products); }

    @Override
    public void onClick(lProduct product) {
        ProductActivity.product = product;
        ProductActivity.products = products;
        startActivity(new Intent(SearchFoodActivity.this,ProductActivity.class));
    }

    @OnClick(R.id.clear)
    void clear(){ search.setText(""); }

    @Override
    public int setlayout() { return R.layout.activity_search_food; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        rv.setLayoutManager(new LinearLayoutManager(SearchFoodActivity.this));
        rv.setAdapter(mAdapter);
        mPresenter.getAllProducts();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
