package ru.timrlm.oki.ui.productlist;

import java.util.List;

import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.ui.base.MvpView;

/**
 * Created by timerlan on 28.03.2018.
 */

interface ProductListMvpView extends MvpView {
    void setProducts(List<lProduct> products, List<lOrder> orders);
    void scrollTOp();
    void modDoalog(List<lProduct> products, lProduct product);

    void errors(String s);

    void resul(String s);

    void reload();

}
