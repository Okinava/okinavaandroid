package ru.timrlm.oki.ui.devilary.newadress;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.util.RxUtil;

@ConfigPersistent
class NewAdressPresenter extends BasePresenter<NewAdressMvpView> {
    private final DataManager mDataManager;

    @Inject
    public NewAdressPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    Disposable disposable;
    Disposable disposableT;
    public void getStreets(){
        disposable = mDataManager.getStreets()
            .flatMapIterable(s->s)
            .map(s-> s.getName())
            .toList()
            .subscribe(st -> { getMvpView().showStreets(st); },
                    throwable -> {
                getMvpView().error("Ой! Нет связи с сервером");
            });
    }

    public void save(String street, String home, String entrance, String apartment, String floor, String doorphone, String comment){
        CusOrder.Address address = new CusOrder.Address();
        address.setAll("Казань",street,home,entrance,apartment,floor,doorphone,comment);
        disposableT = mDataManager.checkAdress(address).subscribe(r->{
            if (r) getMvpView().success(mDataManager.createAdres(street, home, entrance, apartment, floor, doorphone, comment));
            else getMvpView().error(); }, throwable -> getMvpView().error());
    }

    @Override
    public void detachView() {
        RxUtil.dispose(disposable);
        RxUtil.dispose(disposableT);
        super.detachView();
    }
}
