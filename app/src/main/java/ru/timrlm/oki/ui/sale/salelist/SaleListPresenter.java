package ru.timrlm.oki.ui.sale.salelist;

import java.util.List;

import javax.inject.Inject;

import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.model.realm.lProduct;
import ru.timrlm.oki.data.model.smart.Action;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;

@ConfigPersistent
class SaleListPresenter extends BasePresenter<SaleListMVPView> {
    private DataManager mDataManager;

    @Inject
    public SaleListPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    public void load(Action action) {
        mDataManager.getActions().subscribe(a->{
            int pos = -1;
            for (Action p : a){
                pos += 1;
                if (p.getId().equals(action.getId())) {
                    getMvpView().setActions(a,pos);
                    return;
                }
            }
        });
    }
}
