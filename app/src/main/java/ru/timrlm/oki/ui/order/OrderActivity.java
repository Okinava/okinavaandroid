package ru.timrlm.oki.ui.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.instacart.library.truetime.TrueTime;
import com.instacart.library.truetime.TrueTimeRx;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.ui.auto.AutoActivity;
import ru.timrlm.oki.ui.base.BaseActivity;
import ru.timrlm.oki.ui.base.BaseSwipeBackActivity;
import ru.timrlm.oki.ui.devilary.DevilaryActivity;
import ru.timrlm.oki.ui.devilary.adapter.OrderAdapter;
import ru.timrlm.oki.util.ViewUtil;

public class OrderActivity extends BaseSwipeBackActivity implements OrderMvpView , OrderAdapter.OnClickListener {
    @Inject OrderPresenter mPresenter;
    @BindView(R.id.rv) RecyclerView rv;
    @BindView(R.id.bottom_frame) View frame;
    @BindView(R.id.zak) Button zak;
    @BindView(R.id.cena) TextView cena;
    @BindView(R.id.promo) TextView promo;
    private OrderAdapter adapter = new OrderAdapter();

    private void init(){
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);
        adapter.setOnClickListener(this);
        mPresenter.load();
    }

    @Override
    public void setOrders(List<lOrder> orders) {
        if (orders != null) adapter.setOrders(orders);
        rv.setVisibility( orders != null ? View.VISIBLE : View.GONE);
        frame.setVisibility( orders != null ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setSum(int sum) {
        if (sum > 0) cena.setText(sum+" " + ViewUtil.R); else{
            rv.setVisibility( View.GONE);
            frame.setVisibility( View.GONE);
            finish();
        }
    }

    @Override
    public void setPromo(String pr) {
        promo.setText(pr);
    }

    @Override
    public void haveSales() {
        new MaterialDialog.Builder(this)
                .content("Нельзя создать заказа. У Вас в корзине акционный товар. Акцию можно заказать в период в 13:00-16:00, предварительный заказ можно сделать позвонив в колл-центр  \n8 (843) 233-44-33")
                .positiveText("Хорошо")
                .show();
    }

    @Override
    public void start() {
        showProgres();
        TrueTimeRx.build()
                .initializeRx("time.google.com")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(date ->{
                    hideProgres();
                    startActivity(new Intent(OrderActivity.this, DevilaryActivity.class));
                }, throwable -> {
                    hideProgres();
                    new MaterialDialog.Builder(this)
                            .content("Не возможно оформить заказ. Отсутствует подключение к интернету.")
                            .positiveText("Хорошо")
                            .show();
                });
    }

    @OnClick(R.id.zak)
    void zak(){
        if (mPresenter.isAuto()) {
            mPresenter.check();
        }
        else {startActivity(new Intent(OrderActivity.this, AutoActivity.class)); }
    }

    @Override
    public void minus(lOrder order, int position) {
        ViewUtil.vibrate(this);
        List<lOrder> ord = adapter.getOrders();
        if (order.getCount()>1){
            adapter.getOrders().get(position).setSum((order.getSum()/order.getCount()) * (order.getCount() - 1));
            adapter.getOrders().get(position).setCount(order.getCount() - 1);
            adapter.notifyItemChanged(position);
        }else{
            ord.remove(position);
            adapter.notifyItemRemoved(position);
            if (adapter.getOrders().size() == 0){
                rv.setVisibility(View.GONE);
                frame.setVisibility(View.GONE);
            }
        }
        mPresenter.minus(order);
    }

    @Override
    public void plus(lOrder order, int position) {
        ViewUtil.vibrate(this);
        adapter.getOrders().get(position).setSum((order.getSum()/order.getCount()) * (order.getCount() + 1));
        adapter.getOrders().get(position).setCount(order.getCount() + 1);
        adapter.notifyItemChanged(position);
        mPresenter.plus(order);
    }

    @Override
    public void delete(lOrder order, int position) {
        ViewUtil.vibrate(this);
        adapter.getOrders().remove(position);
        adapter.notifyItemRemoved(position);
        if (adapter.getOrders().size() == 0){
            rv.setVisibility(View.GONE);
            frame.setVisibility(View.GONE);
        }
        mPresenter.delete(order);
    }

    @Override
    public int setlayout() { return R.layout.activity_order; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        mPresenter.attachView(this);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }
}
