package ru.timrlm.oki.ui.devilary.pay.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.local.RealmHelper;
import ru.timrlm.oki.data.model.realm.lOrder;
import ru.timrlm.oki.util.ViewUtil;

public class OrderInPayAdapter extends RecyclerView.Adapter<OrderInPayAdapter.OrderInPayView> {
    private List<lOrder> orders;

    public OrderInPayAdapter(List<lOrder> orders) { this.orders = orders; }

    @Override
    public int getItemCount() { return orders.size(); }

    @NonNull
    @Override
    public OrderInPayView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderInPayView(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_orderinpay,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull OrderInPayView holder, int position) {
        holder.name.setText(orders.get(position).getlProduct().getName());
        holder.desc.setText(orders.get(position).getNameM());
        holder.desc.setVisibility(orders.get(position).getModificator().length() > 0 ? View.VISIBLE : View.GONE);
        holder.count.setText("x"+orders.get(position).getCount());
        holder.price.setText(orders.get(position).getSum() + " " + ViewUtil.R);
    }

    public class OrderInPayView extends RecyclerView.ViewHolder {
        @BindView(R.id.name) TextView name;
        @BindView(R.id.desc) TextView desc;
        @BindView(R.id.count) TextView count;
        @BindView(R.id.price) TextView price;

        public OrderInPayView(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
