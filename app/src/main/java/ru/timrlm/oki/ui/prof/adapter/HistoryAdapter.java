package ru.timrlm.oki.ui.prof.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.transition.Visibility;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.timrlm.oki.R;
import ru.timrlm.oki.data.model.iko.CusOrder;
import ru.timrlm.oki.data.model.iko.History;
import ru.timrlm.oki.ui.order.search.SelectAdapter;
import ru.timrlm.oki.util.AppBarStateChangeListener;
import ru.timrlm.oki.util.ViewUtil;

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    private List<History.DeliveryHistory> histories = new ArrayList<>();
    private String name, status, phone, bonusCode, date;
    private int bonus, sex;
    private List<String> adres = new ArrayList<>();
    private Integer visibility = View.GONE;
    private Listener listener = new Listener() {@Override public void delete(int pos) { }@Override public void settings() { }@Override public void add() { }@Override public void longClick(History.DeliveryHistory pos) { }@Override public void click(History.DeliveryHistory pos) { }};

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setAdres(List<String> adres) {
        this.adres = adres;
        if (histories.size() == 0) histories.add(null);
        notifyItemChanged(0);
    }

    public void setUser(String name, String status, int bonus, String phone, String bonusCode, int sex, String date){
        this.name = name; this.status = status; this.bonus = bonus; this.phone = phone; this.bonusCode = bonusCode; this.sex = sex; this.date = date;
        if (histories.size() == 0) histories.add(null);
        notifyItemChanged(0);
    }

    public void setHistories(List<History.DeliveryHistory> histories) {
        this.histories = histories;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? 0 : 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        if (viewType == 1) return new HistoryView(LayoutInflater.from(mContext).inflate(R.layout.rv_history,parent,false));
        return new HeaderView(LayoutInflater.from(mContext).inflate(R.layout.rv_header,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, int p) {
        if (getItemViewType(p) == 0){
            HeaderView rootView = (HeaderView) h;
            rootView.itemView.findViewById(R.id.card_adres).setVisibility(visibility);
            rootView.itemView.findViewById(R.id.card_user).setVisibility(visibility);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) rootView.itemView.findViewById(R.id.card_main).getLayoutParams();
            lp.bottomMargin = visibility == View.VISIBLE ? ViewUtil.dpToPx(1) : 0;
            rootView.itemView.findViewById(R.id.card_main).setLayoutParams(lp);
            ((ImageView)rootView.itemView.findViewById(R.id.arrow)).setImageResource( visibility == View.VISIBLE ? R.drawable.prof_arrow_top : R.drawable.prof_arrow_bottom);
            ((TextView)rootView.itemView.findViewById(R.id.name)).setText(name);
            ((TextView)rootView.itemView.findViewById(R.id.status)).setText(status);
            ((TextView)rootView.itemView.findViewById(R.id.bonus)).setText(bonus+" "+ViewUtil.R);
            ((TextView)rootView.itemView.findViewById(R.id.phone)).setText(phone);
            ((TextView)rootView.itemView.findViewById(R.id.card)).setText(bonusCode);
            ((TextView)rootView.itemView.findViewById(R.id.gender)).setText(sex == 0 ? "пол не указан" : sex == 2 ? "женский пол" : "мужской пол" );
            ((TextView)rootView.itemView.findViewById(R.id.birth)).setText(date);
            LinearLayout layout = rootView.itemView.findViewById(R.id.rv);
            layout.removeAllViews();
            for( int i = 0; i < adres.size(); i++ ){
                View v = LayoutInflater.from(mContext).inflate(R.layout.view_select,layout,false);
                v.findViewById(R.id.select).setVisibility(View.GONE);
                ((TextView) v.findViewById(R.id.text)).setText(adres.get(i));
                ((ImageView)v.findViewById(R.id.img)).setImageResource(R.drawable.order_adress);
                v.findViewById(R.id.btn).setTag(i);
                v.findViewById(R.id.btn).setOnLongClickListener((view)-> {
                    onDelete((Integer) view.getTag());
                    return true;
                });
                layout.addView(v);
            }
            rootView.itemView.findViewById(R.id.btn_arrow).setOnClickListener((v)->{
                visibility = visibility == View.GONE ? View.VISIBLE : View.GONE;
                notifyItemChanged(0);
            });
            rootView.itemView.findViewById(R.id.new_adress).setOnClickListener((v)-> listener.add());
            rootView.itemView.findViewById(R.id.btn_set).setOnClickListener((v)-> listener.settings());
            return;
        }
        HistoryView holder = (HistoryView) h;
        holder.order.setText(mContext.getString(R.string.order_num, histories.get(p).getNumber()));
        holder.date.setText(histories.get(p).getDate());
        holder.status.setText(status(histories.get(p).getStatus()));
        holder.button.setOnClickListener((v)->listener.click(histories.get(p)));
        holder.button.setOnLongClickListener((v)->{
            listener.longClick(histories.get(p));
            return true;
        });
        int sum = 0;
        for (CusOrder.Item i : histories.get(p).getItems()){
            sum += i.getSum();
        }
        holder.price.setText(mContext.getString(R.string.price, sum));
//        if (histories.get(p).getSum() < sum && histories.get(p).getSum() > 0){
//            holder.cardText.setText("- " + (sum - histories.get(p).getSum()) + " " + ViewUtil.R);
//        }else if (histories.get(p).getSum() > 0){
//            holder.card.setCardBackgroundColor(mContext.getResources().getColor(R.color.green));
//            holder.cardText.setTextColor(mContext.getResources().getColor(R.color.green));
//            double bonus = histories.get(p).getIsSelfService() ? histories.get(p).getSum() * 0.05 : histories.get(p).getSum() * 0.02;
//            holder.cardText.setText("+ " + Math.round(bonus)  + " " + ViewUtil.R);
//        }else {
//            holder.card.setVisibility(View.GONE);
//            holder.cardText.setVisibility(View.GONE);
//        }
    }

    private void onDelete(int i) {
        new MaterialDialog.Builder(mContext)
                .title("Удалить")
                .content("Вы действительно хотите удалить " + adres.get(i) + "?")
                .positiveText("Да")
                .negativeText("Нет")
                .onPositive((d,w)->{
                    listener.delete(i);
                    adres.remove(i);
                    notifyItemChanged(0);
                })
                .show();
    }

    private String status(String status){
        switch (status) {
            case "CANCELLED":
                return "Отменен";
            case "NEW":
                return "Новый";
            case "WAITING":
                return "В ожидании";
            case "ON_WAY":
                return "В пути";
            case "CLOSED":
                return "Закрыт";
            case "DELIVERED":
                return "Доставлен";
            case "UNCONFIRMED":
                return "Не подтвержден";
            default:
                return "";
        }
    }

    @Override
    public int getItemCount() { return histories.size(); }

    public class HistoryView extends RecyclerView.ViewHolder{
        @BindView(R.id.order) TextView order;
        @BindView(R.id.date) TextView date;
        @BindView(R.id.price) TextView price;
        @BindView(R.id.status) TextView status;
        @BindView(R.id.card) CardView card;
        @BindView(R.id.card_text) TextView cardText;
        @BindView(R.id.btn) Button button;

        public HistoryView(View view){
            super(view);
            ButterKnife.bind(this,view);
        }

    }

    public class HeaderView extends RecyclerView.ViewHolder{
        public HeaderView(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public interface Listener{
        void delete(int pos);
        void settings();
        void add();
        void longClick(History.DeliveryHistory item);
        void click(History.DeliveryHistory item);
    }
}
