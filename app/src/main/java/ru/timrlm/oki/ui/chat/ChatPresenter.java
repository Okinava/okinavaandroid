package ru.timrlm.oki.ui.chat;

import android.util.Log;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.timrlm.oki.data.DataManager;
import ru.timrlm.oki.data.remote.SmartApi;
import ru.timrlm.oki.di.ConfigPersistent;
import ru.timrlm.oki.ui.base.BasePresenter;
import ru.timrlm.oki.ui.chat.adapter.ChatAdapter;
import ru.timrlm.oki.util.RxUtil;

@ConfigPersistent
class ChatPresenter extends BasePresenter<ChatMvpView> {
    private DataManager mDataManager;
    private Disposable disposable;

    @Inject
    public ChatPresenter(DataManager dataManager) {
        this.mDataManager = dataManager;
    }

    public void load(){
        RxUtil.dispose(disposable);
        disposable = mDataManager.getPushs()
                .map(ps->{
                    List<ChatAdapter.ChatItem> ci = new ArrayList<>();
                    List<String> dd = new ArrayList<>();
                    for (SmartApi.SmartPush p : ps.data){
                        myTime mt = getTime(p.datetime);
                        if ( !dd.contains(mt.date) ) {
                            dd.add(mt.date);
                            ci.add(new ChatAdapter.ChatItem("",0,mt.date,mt.t,mt.time));
                        }
                        SmartApi.SmartTextPush tp = new Gson().fromJson(p.orders,SmartApi.SmartTextPush.class);
                        ci.add(new ChatAdapter.ChatItem("",1,tp.text,mt.t,mt.time));
                        if ( tp.deliveryStatus.equals("DELIVERED") || tp.deliveryStatus.equals("CLOSED") ){
                            ci.add(new ChatAdapter.ChatItem("",1,"Заказ доставлен. Пожалуйста, оцените качество нашего сервиса.",mt.t,mt.time));
                            String text = mDataManager.getMark(tp.orderId);
                            if (text == null) ci.add(new ChatAdapter.ChatItem(tp.orderId,2,"",mt.t,mt.time));
                            else ci.add(new ChatAdapter.ChatItem(tp.orderId,3, text, mt.t, mt.time));
                        }
                    }
                    return ci;
                }).subscribe(r->{
                    getMvpView().setR(r);
                }, t->{
                    getMvpView().noResult();
                });
    }

    @Override
    public void detachView() {
        RxUtil.dispose(disposable);
        super.detachView();
    }

    private myTime getTime(String datetime) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = formatter.parse(datetime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        myTime mt = new myTime();
        mt.date = calendar.get(Calendar.DAY_OF_MONTH) + "\n" + ma[calendar.get(Calendar.MONTH)];
        mt.time = String.format("%02d",  calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d",  calendar.get(Calendar.MINUTE));
        mt.t = calendar.getTimeInMillis();
        return mt;
    }

    String[] ma = {"Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"};

    public void reload() {
        RxUtil.dispose(disposable);
        disposable = mDataManager.getPushs()
                .map(ps->{
                    List<ChatAdapter.ChatItem> ci = new ArrayList<>();
                    List<String> dd = new ArrayList<>();
                    for (SmartApi.SmartPush p : ps.data){
                        myTime mt = getTime(p.datetime);
                        if ( !dd.contains(mt.date) ) {
                            dd.add(mt.date);
                            ci.add(new ChatAdapter.ChatItem("",0,mt.date,mt.t,mt.time));
                        }
                        SmartApi.SmartTextPush tp = new Gson().fromJson(p.orders,SmartApi.SmartTextPush.class);
                        ci.add(new ChatAdapter.ChatItem("",1,tp.text,mt.t,mt.time));
                        if ( tp.deliveryStatus.equals("DELIVERED") || tp.deliveryStatus.equals("CLOSED") ){
                            ci.add(new ChatAdapter.ChatItem("",1,"Заказ доставлен. Пожалуйста, оцените качество нашего сервиса.",mt.t,mt.time));
                            String text = mDataManager.getMark(tp.orderId);
                            if (text == null) ci.add(new ChatAdapter.ChatItem(tp.orderId,2,"",mt.t,mt.time));
                            else ci.add(new ChatAdapter.ChatItem(tp.orderId,3, text, mt.t, mt.time));
                        }
                    }
                    return ci;
                }).subscribe(r->{
                    getMvpView().setReloaded(r);
                }, t->{
                    getMvpView().noResult();
                });
    }

    class myTime{
        public String date;
        public String time;
        public Long t;
    }
}
