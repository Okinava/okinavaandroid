package ru.timrlm.oki.ui.prof;

import java.util.List;

import ru.timrlm.oki.data.model.iko.History;
import ru.timrlm.oki.ui.base.MvpView;

interface PforMvpView extends MvpView{
    void updateCustomer(boolean isAuto);
    void setUser(String name, String status, int bonus, String phone, String bonusCode, int sex, String date);
    void setHistory(List<History.DeliveryHistory> history);
    void setAdres(List<String> adres);
    void endR();
    void auto(boolean b);

    void ok();
    void ok(String text);
    void er(String text);
}
