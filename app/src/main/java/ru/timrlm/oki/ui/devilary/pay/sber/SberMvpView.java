package ru.timrlm.oki.ui.devilary.pay.sber;

import ru.timrlm.oki.ui.base.MvpView;

interface SberMvpView extends MvpView {
    void error();
    void success(String formUrl);

    void end();
}
