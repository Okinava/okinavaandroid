package ru.timrlm.oki.ui.splash.tutorial;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.timrlm.oki.R;

public class TutotialVPAdapter extends PagerAdapter {
    private Context mContext;

    public TutotialVPAdapter(Context context){ mContext = context; }

    public Object instantiateItem(ViewGroup collection, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_tutorial,collection,false);
        ((ViewPager) collection).addView(view);
        switch (position) {
            case 0:
                ((AppCompatImageView)view.findViewById(R.id.img)).setImageResource(R.drawable.tutorial_three);
                ((AppCompatTextView)view.findViewById(R.id.title)).setText(mContext.getResources().getString(R.string.tutorial_title_three));
                ((AppCompatTextView)view.findViewById(R.id.text)).setText(mContext.getResources().getString(R.string.tutorial_text_three));
                break;
            case 1:
                ((AppCompatImageView)view.findViewById(R.id.img)).setImageResource(R.drawable.tutorial_one);
                ((AppCompatTextView)view.findViewById(R.id.title)).setText(mContext.getResources().getString(R.string.tutorial_title_one));
                ((AppCompatTextView)view.findViewById(R.id.text)).setText(mContext.getResources().getString(R.string.tutorial_text_one));
                break;
            case 2:
                ((AppCompatImageView)view.findViewById(R.id.img)).setImageResource(R.drawable.tutorial_two);
                ((AppCompatTextView)view.findViewById(R.id.title)).setText(mContext.getResources().getString(R.string.tutorial_title_two));
                ((AppCompatTextView)view.findViewById(R.id.text)).setText(mContext.getResources().getString(R.string.tutorial_text_two));
                break;
            case 3:
                ((AppCompatImageView)view.findViewById(R.id.img)).setImageResource(R.drawable.tutorial_four);
                ((AppCompatTextView)view.findViewById(R.id.title)).setText(mContext.getResources().getString(R.string.tutorial_title_four));
                ((AppCompatTextView)view.findViewById(R.id.text)).setText(mContext.getResources().getString(R.string.tutorial_text_four));
                break;
            case 4:
                ((AppCompatImageView)view.findViewById(R.id.img)).setImageResource(R.drawable.tutorial_five);
                ((AppCompatTextView)view.findViewById(R.id.title)).setText(mContext.getResources().getString(R.string.tutorial_title_five));
                ((AppCompatTextView)view.findViewById(R.id.text)).setText(mContext.getResources().getString(R.string.tutorial_text_five));
                break;
            case 5:
                ((AppCompatImageView)view.findViewById(R.id.img)).setImageResource(R.drawable.tutorial_six);
                ((AppCompatTextView)view.findViewById(R.id.title)).setText(mContext.getResources().getString(R.string.tutorial_title_six));
                ((AppCompatTextView)view.findViewById(R.id.text)).setText(mContext.getResources().getString(R.string.tutorial_text_six));
                break;
            case 6:
                ((AppCompatImageView)view.findViewById(R.id.img)).setImageResource(R.drawable.tutorial_seven);
                ((AppCompatTextView)view.findViewById(R.id.title)).setText(mContext.getResources().getString(R.string.tutorial_title_seven));
                ((AppCompatTextView)view.findViewById(R.id.text)).setText(mContext.getResources().getString(R.string.tutorial_text_seven));
                break;
        }
        return view;
    }

    @Override
    public int getCount() { return 7; }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) { return arg0 == ((View) arg1);  }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
       ((ViewPager) container).removeView((View) object);
    }
}
